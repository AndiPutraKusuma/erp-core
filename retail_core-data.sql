-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 07, 2018 at 08:06 AM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `retail_core`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_alert_stok` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT tb1.nama_item,tb1.id_suplier,tb1.nama_item,tb1.`id_item`,tb1.tipe,tb1.satuan,SUM(tb1.`jumlah`) AS jumlah,tb1.hargaSatuan,tb1.link_photo,tb1.deskripsi
	FROM (SELECT id_suplier,nama_item,item_master.`id_item`,tipe,satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_produksi`=penerimaan_barang.`id_produksi`
	INNER JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	where item_master.`id_pemilik`=_id_pemilik
	GROUP BY item_master.`id_item`
UNION
	SELECT id_suplier,nama_item,item_master.`id_item`,tipe,satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	where item_master.`id_pemilik`=_id_pemilik
	 GROUP BY item_master.`id_item` ORDER BY nama_item ASC) AS tb1 WHERE tb1.jumlah<20 GROUP BY tb1.nama_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariCustomer` (`_keyword` VARCHAR(300))  BEGIN
	SELECT * FROM customer WHERE nama LIKE CONCAT('%',_keyword,'%') or id_customer like CONCAT('%',_keyword,'%') ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariPetugas` (`_keyword` VARCHAR(300))  BEGIN
	select * from petugas where nama like CONCAT('%',_keyword,'%') or id_petugas LIKE CONCAT('%',_keyword,'%');
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariProduk` (`cari_suplier` VARCHAR(200))  BEGIN
	SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE nama_item LIKE CONCAT('%',cari_suplier,'%')
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC;
 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariSuplier` (`_keyword` VARCHAR(300))  BEGIN
	SELECT * FROM suplier WHERE nama_suplier LIKE CONCAT('%',_keyword,'%') or id_suplier LIKE CONCAT('%',_keyword,'%');
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cari_suplierProduk` (`cari_suplier` VARCHAR(200))  BEGIN
	SELECT nama_suplier,suplier.`id_suplier`,item_master.`id_item`,nama_item,satuan,tipe, SUM(gudang.`jumlah`)AS jumlah,
	 link_photo,item_master.deskripsi,detail_penerimaan.hargaSatuan 
	FROM detail_penerimaan LEFT JOIN gudang 
	ON detail_penerimaan.`id_purchasing`=gudang.`id_purchasing` AND
	detail_penerimaan.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_penerimaan.`id_item`
	INNER JOIN penerimaan_barang ON penerimaan_barang.`id_po`=detail_penerimaan.`id_purchasing`
	INNER JOIN suplier ON suplier.`id_suplier`=penerimaan_barang.`id_suplier`
	 WHERE nama_item LIKE CONCAT('%',cari_suplier,'%') GROUP BY detail_penerimaan.`id_item`;
	 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cekStok` (`_barcode` VARCHAR(100), `_harga` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
SELECT gudang.`id_rec`,detail_suplier.`id_suplier` ,detail_suplier.`id_item`,gudang.`jumlah`,hargaSatuan 
FROM gudang 
INNER JOIN detail_suplier ON detail_suplier.`id_suplier`=gudang.`id_supplier` AND detail_suplier.`id_item`=gudang.`id_item`
WHERE gudang.`barcode_barang`=_barcode
ORDER BY gudang.`id_rec` asc LIMIT 1;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cekStok2` (`_id_item` VARCHAR(100), `_id_supplier` INT(11), `_harga` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
SELECT gudang.`id_rec`,detail_suplier.`id_suplier`,detail_suplier.`id_item`,gudang.`jumlah`,hargaSatuan FROM penerimaan_barang 
INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
WHERE detail_suplier.`id_item`=_id_item and penerimaan_barang.`id_suplier`=_id_supplier  and gudang.`hargaSatuan`=_harga and penerimaan_barang.`id_pemilik`=_id_pemilik
ORDER BY gudang.`id_rec` asc LIMIT 1;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cek_akses` (`_email` VARCHAR(200))  BEGIN
    set@idPet=(select id_petugas from petugas where petugas.`email`=_email);
	select privilege from petugas where id_petugas=@idPet;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteCustomer` (`_id_customer` VARCHAR(100))  BEGIN
	set@cek=(select id_customer from penjualan where id_customer=_id_customer limit 1);
	IF(@cek) THEN
			SELECT -1 AS cek;
		ELSE
		DELETE FROM customer WHERE id_customer=_id_customer;
			SELECT 1 AS cek;
	end if;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteSuplier` (`_id_suplier` INT(11))  BEGIN
	SET@cek=(SELECT id_suplier FROM purchasing WHERE id_suplier=_id_suplier LIMIT 1);
	IF(@cek) THEN
			SELECT -1 AS cek;
	ELSE
	delete from suplier where id_suplier=_id_suplier;
	select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_item` (`_id_item` VARCHAR(100))  BEGIN
	set@cek=(select id_item from detail_suplier where id_item=_id_item limit 1);
	if(@cek) then
		select -1 as cek;
	else
		delete from item_master where id_item=_id_item;
		select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_satuan` (`_id_satuan` INT(11))  BEGIN
	set@child=(select satuan from item_master where satuan=_id_satuan);
	if(@child) then
		select -1 as cek;
	else
		delete from satuan where id_satuan=_id_satuan;
		set@cek=(select id_satuan from satuan where id_satuan=_id_satuan);
		if(@cek) then
			select -1 as cek;
		else
			select 1 as cek;
		end if;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deletPetugas` (`_id_petugas` VARCHAR(100))  BEGIN
	SET@cek=(SELECT id_petugas FROM penjualan WHERE id_petugas=_id_petugas LIMIT 1);
	SET@cek1=(SELECT id_petugas FROM purchasing WHERE id_petugas=_id_petugas LIMIT 1);
	SET@cek2=(SELECT id_petugas FROM service WHERE id_petugas=_id_petugas LIMIT 1);
	SET@cek3=(SELECT teknisi FROM solving WHERE teknisi=_id_petugas LIMIT 1);
	IF(@cek or @cek1 or @cek2 or @cek3 ) THEN
			SELECT -1 AS cek;
	ELSE
	delete from petugas where id_petugas=_id_petugas;
	select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_detail_pembelian` (`_id_pembelian` VARCHAR(100), `_id_produk` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11))  BEGIN
	insert into detail_pembelian (id_transaksi,id_produk,jumlah,harga) values(_id_pembelian,_id_produk,_jumlah,_harga);
	UPDATE produk SET jumalah=jumlah-_jumlah WHERE id_produk=_id_produk;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_detail_pengeluaran` (`_id_issue` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` INT(11), `_harga` INT(11), `_id_suplier` INT(11), `_id_rec` VARCHAR(100), `_kode` INT(11), `_barcode` VARCHAR(100))  BEGIN
	
	insert into detail_pengeluaran(id_issue,id_item,jumlah,harga,id_suplier,id_rec,barcode_barang)
	values(_id_issue,_id_item,_jumlah,_harga,_id_suplier,_id_rec,_barcode);
		if(_kode=1) then
		SET@idSO=(SELECT id_so FROM pengeluaran_barang WHERE pengeluaran_barang.`id_issue`=_id_issue);
		SET@count1=(SELECT SUM(jumlah)  FROM detail_penjualan WHERE id_so=@idSO);
		SET@count2=(SELECT SUM(jumlah) FROM detail_pengeluaran WHERE id_issue=_id_issue);
		if(@count2>=@count1) then
			update penjualan set `status`=3 where id_so=@idSO;
			update pengeluaran_barang set `status`=3 where id_issue=_id_issue;
		end if;
	elseif(_kode=2) then
		set@idPro=(select id_produksi from pengeluaran_barang where pengeluaran_barang.`id_issue`=_id_issue);
		SET@count1=(SELECT SUM(jumlah)  FROM detail_produksi WHERE id_produksi=@idPro);
		SET@count2=(SELECT SUM(jumlah) FROM detail_pengeluaran WHERE id_issue=_id_issue);
		IF(@count2>=@count1) THEN
			UPDATE produksi SET `status`=3 WHERE id_produksi=@idPro;
			UPDATE pengeluaran_barang SET `status`=3 WHERE id_issue=_id_issue;
		END IF;
	ELSEIF(_kode=3) THEN
		SET@idPro=(SELECT pengeluaran_barang.`id_produkService` FROM pengeluaran_barang WHERE pengeluaran_barang.`id_issue`=_id_issue);
		SET@count1=(SELECT SUM(jumlah)  FROM detail_produkservice WHERE id_produkService=@idPro);
		SET@count2=(SELECT SUM(jumlah) FROM detail_pengeluaran WHERE id_issue=_id_issue);
		IF(@count2>=@count1) THEN
			UPDATE produkservice SET `status`=3 WHERE id_produkService=@idPro;
			UPDATE pengeluaran_barang SET `status`=3 WHERE id_issue=_id_issue;
		END IF;
	end if;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_filter_laporanPembelian` (`_tgl_awal` DATE, `_tgl_akhir` DATE, `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penerimaan_barang.`id_rec` ,tanggal_receive,nama_item,sat.nama_satuan,jumlah,petugas.`nama` AS nama_petugas,kurir,nama_suplier
	FROM detail_penerimaan 
	INNER JOIN penerimaan_barang ON detail_penerimaan.`id_rec`=penerimaan_barang.`id_rec`
	INNER JOIN item_master ON detail_penerimaan.`id_item`=item_master.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan` 
	INNER JOIN petugas ON penerimaan_barang.`id_petugas`=petugas.`id_petugas`
	INNER JOIN suplier ON penerimaan_barang.`id_suplier`=suplier.`id_suplier`
	where penerimaan_barang.`status`=3 and penerimaan_barang.`tanggal_receive`>=_tgl_awal 
	and penerimaan_barang.`tanggal_receive`<=_tgl_akhir and penerimaan_barang.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_filter_laporanPenjualan` (`_tgl_awal` DATE, `_tgl_akhir` DATE, `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penjualan.`id_so`,tanggal,nama_item,sat.nama_satuan,jumlah,petugas.`nama` AS nama_petugas,kurir FROM detail_penjualan 
	INNER JOIN penjualan ON detail_penjualan.`id_so`=penjualan.`id_so`
	INNER JOIN item_master ON detail_penjualan.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan` 
	INNER JOIN petugas ON penjualan.`id_petugas`=petugas.`id_petugas`
	where penjualan.`status`=3 and penjualan.`tanggal`>=_tgl_awal and penjualan.`tanggal`<=_tgl_akhir and penjualan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_userId` (`_email` VARCHAR(200))  BEGIN
	select id_petugas from petugas where email=_email;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungCustomer` (`_id_pemilik` VARCHAR(100))  BEGIN
	select count(id_customer) as jumlah from customer where customer.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPembelian` (`_id_pemilik` VARCHAR(100))  BEGIN
	select count(purchasing.`id_po`) as jumlah from purchasing where purchasing.`id_pemilik`=_id_pemilik ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPembelianSukses` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(purchasing.`id_po`) AS jumlah FROM purchasing where purchasing.`status`=3 and purchasing.`id_pemilik`=_id_pemilik ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPenerimaan` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(penerimaan_barang.`id_rec`) AS jumlah FROM penerimaan_barang where penerimaan_barang.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPengeluaran` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(pengeluaran_barang.`id_issue`)AS jumlah FROM pengeluaran_barang where pengeluaran_barang.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPenjualan` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(penjualan.`id_so`) AS jumlah FROM penjualan where penjualan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPenjualanSukses` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(penjualan.`id_so`) AS jumlah FROM penjualan where penjualan.`status`=3 and penjualan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungProduk` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(item_master.`id_item`) AS jumlah FROM item_master where item_master.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungSupplier` (`_id_pemilik` VARCHAR(100))  BEGIN
	select count(id_suplier) as jumlah from suplier where suplier.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_inputProduk_supplier` (`_idSupplier` INT(11), `_id_item` VARCHAR(100), `_id_pemilik` VARCHAR(100), `_harga` INT(11))  BEGIN
	set@cek=(select id_suplier from detail_suplier where id_suplier=_idSupplier and id_item=_id_item);
	if(@cek) then
		select -1 as cek;
	else
		insert into detail_suplier(id_suplier,id_item,id_pemilik,harga) values(_idSupplier,_id_item,_id_pemilik,_harga);
		select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_customer` (`_idCustomer` VARCHAR(100), `_id_institut` VARCHAR(100), `_nama` VARCHAR(200), `_jenkel` VARCHAR(3), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_jabatan` VARCHAR(100), `_id_pemilik` VARCHAR(100))  BEGIN
	set@cek=(select id_customer from customer where id_customer=_idCustomer);
	if(@cek) then
		select -1 as cek;
	else
		insert into `customer` (id_customer,id_institut,nama,jenkel,alamat,hp,email,tgl,jabatan,id_pemilik) 
		values(_idCustomer,_id_institut,_nama,_jenkel,_alamat,_hp,_email,now(),_jabatan,_id_pemilik);
		select 0 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailKeluarGudang` (`_id_tran` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_id_suplier` INT(11))  BEGIN
	update detail_penjualan set jumlah_keluar=_jumlah where id_transaksi=_id_tran and id_item=_id_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailPenjualan` (`_id_so` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11), `_idSuplier` INT(11), `_barcode` VARCHAR(100), `_idPemilik` VARCHAR(100))  BEGIN
	INSERT INTO detail_penjualan (id_so,id_item,jumlah,harga,barcode_barang,id_pemilik,id_suplier)
	VALUES(_id_so,_id_item,_jumlah,_harga,_barcode,_idPemilik,_idSuplier);
	    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailProduksi` (`_id_produksi` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_hargaSatuan` INT(11), `_id_suplier` INT(11))  BEGIN
	INSERT INTO detail_produksi(id_produksi,id_item,jumlah,hargaSatuan,id_suplier) VALUES(_id_produksi,_id_item,_jumlah,_hargaSatuan,_id_suplier);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailPurchasing` (`_id_purchasing` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_hargaSatuan` INT(11), `_idSupplier` INT(11))  BEGIN
	insert into detail_purchasing(id_purchasing,id_item,jumlah,hargaSatuan,id_supplier) values(_id_purchasing,_id_item,_jumlah,_hargaSatuan,_idSupplier);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_gudang` (`_id_rec` VARCHAR(200), `_id_tran` VARCHAR(200), `_id_item` VARCHAR(200), `_jumlah` DOUBLE, `_harga` INT(11), `_kode` INT(11), `_id_pemilik` VARCHAR(100), `_barcode` VARCHAR(100), `_idSupplier` INT(11))  BEGIN
	if(_kode=1) then
	set@cekPO=(select jumlah from detail_purchasing where id_purchasing=_id_tran and id_item=_id_item);
	if (_jumlah>=@cekPO) then
		insert into gudang (id_rec,id_item,jumlah,hargaSatuan,id_pemilik,barcode_barang,id_supplier) values(_id_rec,_id_item,_jumlah,_harga,_id_pemilik,_barcode,_idSupplier);
		INSERT INTO detail_penerimaan (id_rec,id_item,jumlah,hargaSatuan,id_pemilik,barcode_barang,id_supplier) VALUES(_id_rec,_id_item,_jumlah,_harga,_id_pemilik,_barcode,_idSupplier);
		set@count1=(SELECT SUM(jumlah)  FROM detail_purchasing WHERE id_purchasing=_id_tran);
		set@count2=(select sum(jumlah) from gudang where id_rec=_id_rec);
		if (@count2>=@count1) then 
			update purchasing set `status`=3 where id_po=_id_tran;
			UPDATE penerimaan_barang SET `status`=3 WHERE id_rec=_id_rec;
		end if;
	else
		INSERT INTO gudang (id_rec,id_item,jumlah,hargaSatuan,`status`,id_pemilik,barcode_barang) VALUES(_id_rec,_id_item,_jumlah,_harga,(_jumlah-@cekPO),_id_pemilik,_barcode);
		INSERT INTO detail_penerimaan (id_rec,id_item,jumlah,hargaSatuan,`status`,id_pemilik,barcode_barang) VALUES(_id_rec,_id_item,_jumlah,_harga,(_jumlah-@cekPO),_id_pemilik,_barcode);
		INSERT INTO defect (id_rec,id_item,jumlah,hargaSatuan,tanggal,keterangan,`status`,id_pemilik) 
		VALUES(_id_rec,_id_item,(@cekPO-_jumlah),_harga,now(),1,1,_id_pemilik);
		UPDATE purchasing SET `status`=2 WHERE id_po=_id_tran;
		UPDATE penerimaan_barang SET `status`=2 WHERE id_rec=_id_rec; 
	end if;
	elseif(_kode=2)then
		INSERT INTO gudang (id_rec,id_item,jumlah,hargaSatuan,id_pemilik,barcode_barang) VALUES(_id_rec,_id_item,_jumlah,_harga,_id_pemilik,_barcode);
		INSERT INTO detail_penerimaan (id_rec,id_item,jumlah,hargaSatuan,id_pemilik,barcode_barang) VALUES(_id_rec,_id_item,_jumlah,_harga,_id_pemilik,_barcode);
		SET@count1=(SELECT SUM(jumlah_item)  FROM produksi WHERE id_produksi=_id_tran);
		SET@count2=(SELECT SUM(jumlah) FROM gudang WHERE id_rec=_id_rec);
		IF (@count2>=@count1) THEN 
			UPDATE produksi SET `status`=6 WHERE id_produksi=_id_tran;
			UPDATE penerimaan_barang SET `status`=3 WHERE id_rec=_id_rec;
		END IF;
		
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_item` (`_id_item` VARCHAR(100), `_nama_item` VARCHAR(200), `_harga` INT(50), `_satuan` VARCHAR(50), `_deskripsi` TEXT, `_link_photo` VARCHAR(200), `id_pemilik` VARCHAR(200))  BEGIN
	SET@cek=(SELECT count(*) from item_master where id_item=_id_item);
	if (@cek) then
		select -1 as cek;
	else
	insert into item_master(id_item,nama_item,item_harga,satuan,deskripsi,link_photo,id_pemilik) values(_id_item,_nama_item,_harga,_satuan,_deskripsi,_link_photo,id_pemilik);
	select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_keluarGudang` (`_id_issue` VARCHAR(100), `_id` VARCHAR(100), `_email` VARCHAR(100), `_id_customer` INT(11), `_total` BIGINT(20), `_tgl` DATETIME, `_kurir` VARCHAR(100), `_kode` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	set@idPet=(select id_petugas from petugas where email=_email);
	if _kode=1 then
		insert into pengeluaran_barang(id_issue,id_so,id_petugas,id_customer,total,kurir,tanggal,id_pemilik)
		values(_id_issue,_id,@idPet,_id_customer,_total,_kurir,_tgl,_id_pemilik);
	elseif _kode=2 then
		INSERT INTO pengeluaran_barang(id_issue,id_produksi,id_petugas,id_customer,total,kurir,tanggal,id_pemilik)
		VALUES(_id_issue,_id,@idPet,_id_customer,_total,_kurir,_tgl,_id_pemilik);
	ELSEIF _kode=3 THEN
		INSERT INTO pengeluaran_barang(id_issue,id_produkService,id_petugas,id_customer,total,kurir,tanggal,id_pemilik)
		VALUES(_id_issue,_id,@idPet,_id_customer,_total,_kurir,_tgl,_id_pemilik);
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_penerimaan` (`_id_rec` VARCHAR(100), `_id_po` VARCHAR(200), `_idPenerima` VARCHAR(200), `_tgl` DATE, `_idSuplier` INT(11), `_totalHarga` BIGINT(20), `_kurir` VARCHAR(100), `_kode` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	if(_kode=1) then
		INSERT INTO penerimaan_barang (id_rec,id_po,id_petugas,tanggal_receive,id_suplier,totalHarga,kurir,id_pemilik) 
		VALUES(_id_rec,_id_po,_idPenerima,_tgl,_idSuplier,_totalHarga,_kurir,_id_pemilik);
	elseif(_kode=2) then
		INSERT INTO penerimaan_barang (id_rec,id_produksi,id_petugas,tanggal_receive,id_suplier,totalHarga,kurir) 
		VALUES(_id_rec,_id_po,_idPenerima,_tgl,_idSuplier,_totalHarga,_kurir);
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_penjualan` (`_id_so` VARCHAR(100), `_email` VARCHAR(100), `_id_customer` VARCHAR(100), `_total` BIGINT(20), `_tgl` DATETIME, `_kurir` VARCHAR(100), `_ongkir` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SET@id_petugas=(SELECT id_petugas FROM petugas WHERE email=_email);
	INSERT INTO penjualan (id_so,id_petugas,id_customer,tanggal,total,kurir,ongkir,id_pemilik) 
	VALUES(_id_so,@id_petugas,_id_customer,_tgl,_total,_kurir,_ongkir,_id_pemilik);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_petugas` (`_ktp` VARCHAR(100), `_nama` VARCHAR(200), `_email` VARCHAR(200), `_passwd` VARCHAR(200))  BEGIN
	set@cek=(select count(*) from petugas where id_petugas=_ktp);
	SET@cek2=(SELECT Count(*) email FROM petugas WHERE email=_email);
	if (@cek or @cek2) then
		select -1 as cek;
	else
		INSERT INTO `petugas` (id_petugas,nama,email,tgl,`password`,privilege,jabatan) 
		VALUES(_ktp,_nama,_email,now(),md5(_passwd),1,'user');
		select 0 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_purchasing` (`_id_po` VARCHAR(100), `_id_suplier` INT(11), `_email` VARCHAR(200), `_tanggal_po` DATE, `_totalHarga` BIGINT(20), `_id_pemilik` VARCHAR(100), `_ongkir` INT(11))  BEGIN
	set@idPetugas=(select id_petugas from petugas where email=_email);
	insert into purchasing (id_po,id_suplier,id_petugas,tanggal_po,totalHarga,id_pemilik,ongkir) 
	values(_id_po,_id_suplier,@idPetugas,_tanggal_po,_totalHarga,_id_pemilik,_ongkir);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_satuan` (`_nama_satuan` VARCHAR(50), `_kelompok` VARCHAR(50), `_deskripsi` VARCHAR(100), `_id_pemilik` VARCHAR(100))  BEGIN
	insert into satuan (nama_satuan,kelompok_satuan,deskripsi_satuan,id_pemilik) values(_nama_satuan,_kelompok,_deskripsi,_id_pemilik);
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_suplier` (`_nama` VARCHAR(200), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_deskripsi` TEXT, `_id_pemilik` VARCHAR(100))  BEGIN
	set@cek=(select id_suplier from suplier where lower(nama_suplier)=lower(_nama));
	if(@cek) then
		select -1 as cek;
	else
		INSERT INTO `suplier` (nama_suplier,alamat,hp,email,deskripsi,tgl,id_pemilik) VALUES(_nama,_alamat,_hp,_email,_deskripsi,now(),_id_pemilik);
		select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_defect` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_def,id_rec,id_issue,defect.`id_item`,nama_item,nama_satuan,nama_tipe_item,jumlah,hargaSatuan,tanggal,keterangan FROM defect 
	INNER JOIN item_master ON item_master.`id_item`=defect.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE ((`status`=1 AND defect.`tanggal`>=now() AND defect.`tanggal`<=now())
	 OR (`status`=2 AND defect.`tanggal`>=now() AND defect.`tanggal`<=now())) and defect.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_pembelian` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penerimaan_barang.`id_rec` ,tanggal_receive,nama_item,sat.nama_satuan,jumlah,petugas.`nama` AS nama_petugas,kurir,nama_suplier
	FROM detail_penerimaan 
	INNER JOIN penerimaan_barang ON detail_penerimaan.`id_rec`=penerimaan_barang.`id_rec`
	INNER JOIN item_master ON detail_penerimaan.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan` 
	INNER JOIN petugas ON penerimaan_barang.`id_petugas`=petugas.`id_petugas`
	INNER JOIN suplier ON penerimaan_barang.`id_suplier`=suplier.`id_suplier`
	WHERE penerimaan_barang.`status`=3 and penerimaan_barang.`tanggal_receive`>=now() AND penerimaan_barang.`tanggal_receive`<=now() and penerimaan_barang.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_penjualan` (`_id_pemilik` VARCHAR(100))  BEGIN
	select penjualan.`id_so`,tanggal,nama_item,sat.nama_satuan,jumlah,petugas.`nama` as nama_petugas,kurir from detail_penjualan 
	inner join penjualan on detail_penjualan.`id_so`=penjualan.`id_so`
	inner join item_master on detail_penjualan.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan` 
	inner join petugas on penjualan.`id_petugas`=petugas.`id_petugas`
	WHERE penjualan.`status`=3 and penjualan.`tanggal`>=now() AND penjualan.`tanggal`<=now() and penjualan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listProduk_perSuplier` (`_id_suplier` INT(11))  BEGIN
	SELECT detail_suplier.`harga`,nama_item,detail_suplier.`id_item`,sat.nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan` 
	WHERE detail_suplier.`id_suplier`=_id_suplier GROUP BY detail_suplier.`id_item`,hargaSatuan ORDER BY nama_item ASC;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listProduk_perSuplier_pagination` (`_id_suplier` INT(11), `_offset` INT(11), `_rows` INT(11))  BEGIN
	SELECT detail_suplier.`harga`,nama_item,detail_suplier.`id_item`,sat.nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan` 
	WHERE detail_suplier.`id_suplier`=_id_suplier GROUP BY detail_suplier.`id_item`,hargaSatuan ORDER BY nama_item ASC
	LIMIT _offset, _rows;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_customer` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT institusi.`nama_institusi`,id_customer,id_institusi,nama,jenkel,alamat,hp,customer.email,tgl,photo_link,jabatan
	 FROM customer INNER JOIN institusi ON customer.`id_institut`=institusi.`id_institusi` 
	 where customer.`id_pemilik`=_id_pemilik order by nama asc;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_item` (IN `_id_pemilik` VARCHAR(100), `game` INT(10))  BEGIN
	if(game > 0) then
		select item_master.id_item ,nama_item, item_harga , link_photo , sat.nama_satuan as satuan_nama ,item_master.`satuan` as satuan ,item_master.deskripsi,sub_kategori.`subKategori_name` as tipe_name,item_master.`tipe`,item_master.`item_harga` as harga from item_master
		inner join satuan sat on sat.id_satuan=item_master.`satuan`
		inner join sub_kategori on sub_kategori.`subKategori_kode`=item_master.`tipe` where item_master.`id_pemilik`= 0 AND sub_kategori.`id_pemilik` = 0 order by nama_item asc;
	else
		SELECT item_master.id_item ,nama_item, item_harga , link_photo , sat.nama_satuan AS satuan_nama , item_master.`satuan` AS satuan,item_master.deskripsi,sub_kategori.`subKategori_name` AS tipe_name,item_master.`tipe`,item_master.`item_harga` AS harga FROM item_master
		INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
		INNER JOIN sub_kategori ON sub_kategori.`subKategori_kode`=item_master.`tipe` WHERE item_master.`id_pemilik`= _id_pemilik AND sub_kategori.`id_pemilik` = _id_pemilik  ORDER BY nama_item ASC;
		
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_penerimaan` ()  BEGIN
	SELECT purchasing.`id_petugas`,id_po,petugas.`nama` AS nama_petugas,nama_suplier,tanggal_po,totalHarga,`status`,1 as kode FROM purchasing 
	INNER JOIN petugas ON purchasing.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON purchasing.`id_suplier`=suplier.`id_suplier` where `status`=1 or `status`=2 or `status`=3
union
	SELECT produksi.`id_petugas`,id_produksi,petugas.`nama` AS nama_petugas,"PT.Miconos" AS nama_suplier,tanggal_po,totalHarga,`status`,2 AS kode 
	FROM produksi 
	INNER JOIN petugas ON produksi.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON suplier.`id_suplier`=3 WHERE `status`=5 or `status`=6;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_pengeluaran` ()  BEGIN
	select penjualan.`id_so`,petugas.nama as nama_petugas,customer.`nama` as nama_customer,tanggal,total,kurir,`status`, 1 as kode
	from penjualan 
	inner join petugas on petugas.`id_petugas`=penjualan.`id_petugas`
	inner join customer on customer.`id_customer`=penjualan.`id_customer`
	where penjualan.`status`=1 or penjualan.`status`=2 or `status`=3
	union
	SELECT produksi.`id_produksi`,petugas.nama AS nama_petugas,"Produksi" as nama_customer,tanggal_po,totalHarga,NULL AS kurir,`status`,2 as kode
	FROM produksi
	INNER JOIN petugas ON petugas.`id_petugas`=produksi.`id_petugas`
	WHERE produksi.`status`=1 OR produksi.`status`=2 or `status`=3
	union 
	SELECT produkservice.`id_produkService`,petugas.nama AS nama_petugas,customer.`nama` as nama_customer,tanggal,total,"Service" AS kurir,produkservice.`status`,3 AS kode
	FROM produkservice
	INNER JOIN service ON service.`id_service`=produkservice.`id_service`
	INNER JOIN customer ON customer.`id_customer`=service.`id_customer`
	INNER JOIN petugas ON petugas.`id_petugas`=produkservice.`teknisi`
	WHERE produkservice.`status`=1 or produkservice.`status`=2 or produkservice.`status`=3 ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_penjualan` ()  BEGIN
	
	select id_so,petugas.`nama` as nama_petugas,customer.`nama` as nama_customer,tanggal,total,kurir,`status` 
	from penjualan 
	inner join customer on penjualan.`id_customer`=customer.`id_customer` 
	inner join petugas on penjualan.`id_petugas`=petugas.`id_petugas` order by tanggal desc;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_petugas` ()  BEGIN
	SELECT * FROM petugas;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_produk` (`_id_pemilik` VARCHAR(100))  BEGIN
	
	SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	where item_master.`id_pemilik`=_id_pemilik
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC;
 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_purchasing` (`nama` VARCHAR(200))  BEGIN
	select purchasing.`id_petugas`,id_po,petugas.`nama` as nama_petugas,nama_suplier,tanggal_po,totalHarga,`status` from purchasing 
	inner join petugas on purchasing.`id_petugas`=petugas.`id_petugas` 
	inner join suplier on purchasing.`id_suplier`=suplier.`id_suplier` where id_petugas=nama order by tanggal_po desc;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_satuan` (`_id_pemilik` VARCHAR(100))  BEGIN
		select * from satuan;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_service` ()  BEGIN
	select id_service,`subject`,keluhan,tgl_open,`status`,customer.`nama` as nama_customer,customer.`id_customer`,petugas.`nama` as nama_petugas,service.`id_petugas`,service.`id_customer` 
	from service inner join customer on service.`id_customer`=customer.`id_customer`
	inner join petugas on service.`id_petugas`=petugas.`id_petugas`;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_suplier` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT * FROM suplier where id_pemilik=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_tipeItem2` (`_idPemilik` VARCHAR(200))  BEGIN
	select * from item_SubGroup where id_pemilik = _idPemilik ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_transaksiPerCustomer` (`_id_customer` VARCHAR(100))  BEGIN
	select penjualan.`id_so`,tanggal,nama_item,jumlah,harga
	from detail_penjualan 
	inner join item_master on detail_penjualan.`id_item`=item_master.`id_item`
	INNER JOIN penjualan on detail_penjualan.`id_so`=penjualan.`id_so`
	  where id_customer=_id_customer;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_login` (IN `_email` VARCHAR(200), IN `_passwd` VARCHAR(50))  BEGIN
	set@cek=(select 1 from petugas where email=_email and `password`=md5(_passwd));
	if @cek=1 then
		select 1 as A;
	else select 0 as A;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_customer` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	
	SELECT institusi.`nama_institusi`,id_customer,id_institusi,nama,jenkel,alamat,hp,customer.email,tgl,photo_link,jabatan
	 FROM customer INNER JOIN institusi ON customer.`id_institut`=institusi.`id_institusi`
	 where customer.`id_pemilik`=_id_pemilik
	 ORDER BY nama ASC LIMIT _limit offset _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_pembelian` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT purchasing.`id_petugas`,id_po,petugas.`nama` AS nama_petugas,nama_suplier,tanggal_po,totalHarga,`status`,ongkir FROM purchasing 
	INNER JOIN petugas ON purchasing.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON purchasing.`id_suplier`=suplier.`id_suplier`
	where purchasing.`id_pemilik`=_id_pemilik
	ORDER BY tanggal_po DESC LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_penerimaan` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN 
	SELECT purchasing.`id_petugas`,id_po,petugas.`nama` AS nama_petugas,nama_suplier,tanggal_po,totalHarga,`status`,1 AS kode,ongkir FROM purchasing 
	INNER JOIN petugas ON purchasing.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON purchasing.`id_suplier`=suplier.`id_suplier` WHERE (`status`=1 OR `status`=2 OR `status`=3) and purchasing.id_pemilik=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_pengeluaran` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penjualan.`id_so`,petugas.nama AS nama_petugas,customer.`nama` AS nama_customer,tanggal,total,kurir,`status`, 1 AS kode, ongkir
	FROM penjualan 
	INNER JOIN petugas ON petugas.`id_petugas`=penjualan.`id_petugas`
	INNER JOIN customer ON customer.`id_customer`=penjualan.`id_customer`
	WHERE penjualan.`status` > 0 and penjualan.`id_pemilik`=_id_pemilik
	order by penjualan.`tanggal` desc LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_penjualan` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_so,petugas.`nama` AS nama_petugas,customer.`nama` AS nama_customer,tanggal,total,kurir,`status` 
	FROM penjualan 
	INNER JOIN customer ON penjualan.`id_customer`=customer.`id_customer` 
	INNER JOIN petugas ON penjualan.`id_petugas`=petugas.`id_petugas`
	where penjualan.`id_pemilik`=_id_pemilik
	ORDER BY tanggal DESC LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_penjualan2` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	
	SELECT id_so,petugas.`nama` AS nama_petugas,tanggal,total,kurir,`status` 
	FROM penjualan 
	INNER JOIN petugas ON penjualan.`id_petugas`=petugas.`id_petugas`
	WHERE penjualan.`id_pemilik`=_id_pemilik
	ORDER BY tanggal DESC LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_produk` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	where penerimaan_barang.`id_pemilik`= `_id_pemilik`
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_supplier` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	select * from suplier where id_pemilik=_id_pemilik order by nama_suplier asc ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pembelian` (`_id_transaksi` VARCHAR(100), `_email` VARCHAR(100), `_id_customer` INT(11), `_total` BIGINT(20), `_tgl` DATETIME)  BEGIN
	set@id_petugas=(select id_petugas from petugas where email=_email);
	insert into pembelian (id_transaksi,id_petugas,id_customer,tanggal,total) values(_id_transaksi,@id_petugas,_id_customer,_tgl,_total);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pengurangan_stok` (`_id_rec` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE)  BEGIN
	UPDATE gudang SET jumlah=(jumlah-_jumlah) WHERE id_rec=_id_rec AND id_item=_id_item;
	set@cek=(select jumlah from gudang WHERE id_rec=_id_rec AND id_item=_id_item);
	set@set=(SELECT `status` FROM gudang WHERE id_rec=_id_rec AND id_item=_id_item);
	if(@cek<1 and @set=0)then
		delete from gudang WHERE id_rec=_id_rec AND id_item=_id_item;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pengurangan_stok2` (`_id_rec` VARCHAR(100), `_barcode_barang` VARCHAR(100), `_jumlah` DOUBLE)  BEGIN
	UPDATE gudang SET jumlah=(jumlah-_jumlah) WHERE id_rec=_id_rec and barcode_barang = _barcode_barang ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianListProduk` (`_id_item` VARCHAR(100))  BEGIN
	SELECT nama_suplier,detail_suplier.`id_suplier`,nama_item,detail_suplier.`id_item`,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	inner join suplier on suplier.`id_suplier`=detail_suplier.`id_suplier`
	WHERE detail_suplier.`id_item`=_id_item GROUP BY detail_suplier.`id_item`,hargaSatuan ORDER BY nama_item ASC;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianPembelian` (IN `_id_pur` VARCHAR(100))  BEGIN
	select DISTINCT  detail_purchasing.`id_item`,nama_item,nama_satuan,sub_kategori.`subKategori_name` as  nama_tipe_item,detail_purchasing.jumlah,tab.`jumlah`as jml_keluar,detail_purchasing.hargaSatuan 
	from detail_purchasing inner join item_master on item_master.`id_item`=detail_purchasing.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	INNER JOIN sub_kategori ON sub_kategori.`subKategori_kode`=item_master.`tipe` AND sub_kategori.`id_pemilik` = item_master.`id_pemilik`
		left join (select penerimaan_barang.`id_po`,jumlah,id_item from detail_penerimaan inner join penerimaan_barang on penerimaan_barang.`id_rec`=detail_penerimaan.`id_rec`
	where penerimaan_barang.`id_po`=_id_pur) as tab on tab.id_po=detail_purchasing.`id_purchasing` AND tab.id_item=detail_purchasing.`id_item`
	where detail_purchasing.`id_purchasing`=_id_pur ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianPenjualan` (IN `_id_tran` VARCHAR(100))  BEGIN
	SELECT detail_penjualan.`id_item`,nama_item,nama_satuan,sub_kategori.`subKategori_name` AS nama_tipe_item,SUM(detail_penjualan.jumlah) AS jumlah,SUM(tab.`jumlah`) AS keluar,harga FROM detail_penjualan
	INNER JOIN item_master ON item_master.`id_item`=detail_penjualan.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	#inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	INNER JOIN sub_kategori ON sub_kategori.`subKategori_kode`=item_master.`tipe` AND sub_kategori.`id_pemilik` = item_master.`id_pemilik`
	LEFT JOIN (SELECT pengeluaran_barang.`id_so`,jumlah,id_item,barcode_barang FROM detail_pengeluaran 
	INNER JOIN pengeluaran_barang ON pengeluaran_barang.`id_issue`=detail_pengeluaran.`id_issue`
	WHERE pengeluaran_barang.`id_so`=_id_tran) AS tab ON tab.id_so=detail_penjualan.`id_so` AND tab.barcode_barang=detail_penjualan.`barcode_barang`
	WHERE detail_penjualan.`id_so`=_id_tran
	GROUP BY detail_penjualan.`id_item`;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincian_viewSO` (`_id_so` VARCHAR(100))  BEGIN
	select customer.`id_customer`,customer.`nama`as nama_customer,petugas.`nama`as nama_petugas,penjualan.`tanggal`,kurir,penjualan.`total`,penjualan.`ongkir` from penjualan
	inner join petugas on petugas.`id_petugas`=penjualan.`id_petugas`
	inner join customer on customer.`id_customer`=penjualan.`id_customer`
	where penjualan.`id_so`=_id_so;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_stok_gudang` (`_id_item` VARCHAR(100))  BEGIN
	SELECT "Masuk" AS asal,SUM(detail_penerimaan.`jumlah`)AS jumlah,nama_item FROM detail_penerimaan 
	INNER JOIN penerimaan_barang ON penerimaan_barang.`id_rec`=detail_penerimaan.`id_rec`
	inner join item_master on item_master.`id_item`=detail_penerimaan.`id_item`
	WHERE (penerimaan_barang.`status`=3 AND detail_penerimaan.`id_item`=_id_item) 
	or (penerimaan_barang.`status`=2 AND detail_penerimaan.`id_item`=_id_item)
	UNION 
	SELECT "Gudang" AS asal, SUM(gudang.`jumlah`) AS jumlah,nama_item FROM gudang
	inner join item_master on item_master.`id_item`=gudang.`id_item`
	WHERE gudang.`id_item`=_id_item
	UNION
	SELECT "Keluar" AS asal,SUM(detail_pengeluaran.`jumlah`) AS jumlah,nama_item FROM detail_pengeluaran 
	INNER JOIN pengeluaran_barang ON pengeluaran_barang.`id_issue`=detail_pengeluaran.`id_issue`
	inner join item_master on item_master.`id_item`=detail_pengeluaran.`id_item`
	WHERE pengeluaran_barang.`status`=3 AND detail_pengeluaran.`id_item`=_id_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_ubahPassword` (`_id_petugas` VARCHAR(100), `_pwdLama` VARCHAR(200), `_pwdBaru` VARCHAR(200))  BEGIN
	set@cek=(select id_petugas from petugas where id_petugas=_id_petugas and `password`=md5(_pwdLama));
	if (@cek) then
		update petugas set `password`=md5(_pwdBaru) where id_petugas=_id_petugas;
		select 1 as cek;
	else 
		select -1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateCustomer` (`_institusi` VARCHAR(100), `_ktp` VARCHAR(100), `_nama` VARCHAR(200), `_jenkel` CHAR(3), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_jabatan` VARCHAR(200))  BEGIN
    
	 
	UPDATE customer SET id_institut=_institusi, nama=_nama, jenkel=_jenkel,alamat=_alamat, hp=_hp, email=_email,jabatan=_jabatan WHERE id_customer=_ktp;
	SELECT 1 AS cek;
	 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateItem` (`_id_item` VARCHAR(100), `_nama_item` VARCHAR(200), `_tipe` VARCHAR(50), `_satuan` VARCHAR(50), `_deskripsi` TEXT)  BEGIN
	update item_master set nama_item=_nama_item,tipe=_tipe,satuan=_satuan,deskripsi=_deskripsi where id_item=_id_item;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updatePetugas` (`_ktp` VARCHAR(100), `_nama` VARCHAR(200), `_jenkel` CHAR(3), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_jabatan` VARCHAR(200), `_passwd` VARCHAR(200))  BEGIN
    set@passwd=(select `password` from petugas where id_petugas=_ktp);
	if@passwd=(md5(_passwd)) then 
	 
	UPDATE petugas SET nama=_nama, jenkel=_jenkel,alamat=_alamat, hp=_hp, email=_email,jabatan=_jabatan WHERE id_petugas=_ktp;
	select 1 as cek;
	 else select -1 as cek; end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateSatuan` (`_id` INT(11), `_nama_satuan` VARCHAR(50), `_kelompok` VARCHAR(50), `_deskripsi` VARCHAR(100))  BEGIN
	update satuan set nama_satuan=_nama_satuan,kelompok_satuan=_kelompok,deskripsi_satuan=_deskripsi where id_satuan=_id;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateStatus_pembelian` (`_id_pur` VARCHAR(100), `_status` INT(11))  BEGIN
	update purchasing set `status`=_status where id_po=_id_pur;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateStatus_penjualan` (`_id_so` VARCHAR(100), `_status` INT(11))  BEGIN
	update penjualan set `status`=_status where id_so=_id_so;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateSuplier` (`_id_suplier` INT(11), `_nama` VARCHAR(200), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_deskripsi` TEXT)  BEGIN
	update suplier set nama_suplier=_nama,alamat=_alamat,hp=_hp,email=_email,deskripsi=_deskripsi where id_suplier=_id_suplier;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewPO` (`_id_po` VARCHAR(200))  BEGIN
	select id_purchasing,nama_item,item_master.`id_item`,jumlah,hargaSatuan,sub_kategori.`subKategori_name` as nama_tipe_item,nama_satuan,id_suplier,purchasing.id_petugas,totalHarga,1 as kode
	FROM detail_purchasing INNER JOIN purchasing ON purchasing.`id_po`=detail_purchasing.`id_purchasing`
	INNER JOIN item_master ON detail_purchasing.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN sub_kategori ON sub_kategori.`subKategori_kode`=item_master.`tipe` AND sub_kategori.`id_pemilik` = item_master.`id_pemilik`
	where id_purchasing=_id_po;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewSO` (`_id_so` VARCHAR(100))  BEGIN
	select detail_suplier.`harga` as harga_beli, nama_suplier,penjualan.`id_so`,nama_item,item_master.`id_item`,jumlah,nama_satuan,sub_kategori.`subKategori_name` as nama_tipe_item,detail_penjualan.harga,jumlah,detail_penjualan.`id_suplier`,1 as kode, barcode_barang
	from detail_penjualan 
	inner join penjualan on penjualan.`id_so`=detail_penjualan.`id_so`
	inner join suplier on suplier.`id_suplier`=detail_penjualan.`id_suplier`
	inner join item_master on item_master.`id_item`=detail_penjualan.`id_item`
	INNER JOIN detail_suplier ON suplier.`id_suplier`=detail_suplier.`id_suplier` and detail_suplier.`id_item`=item_master.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	INNER JOIN sub_kategori ON sub_kategori.`subKategori_kode`=item_master.`tipe` AND sub_kategori.`id_pemilik` = item_master.`id_pemilik`
		where penjualan.`id_so`=_id_so;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perCustomer` (`_id_customer` VARCHAR(100))  BEGIN
	SELECT institusi.`nama_institusi`,id_customer,id_institusi,nama,jenkel,alamat,hp,customer.email,tgl,photo_link,jabatan
	 FROM customer inner join institusi on customer.`id_institut`=institusi.`id_institusi` WHERE id_customer=_id_customer;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perItem` (`_id_item` VARCHAR(100), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT item_master.`id_item`,nama_item,item_master.deskripsi,link_photo,
	sat.nama_satuan, sub_kategori.`subKategori_name` as nama_tipe_item,item_master.`satuan`,item_master.`tipe`,item_master.`item_harga` as harga
	FROM item_master INNER JOIN satuan ON satuan.`id_satuan`=item_master.`satuan`
	inner join satuan sat on sat.`id_satuan`=item_master.`satuan`
	inner join sub_kategori on sub_kategori.`subKategori_kode`=item_master.`tipe` and sub_kategori.`id_pemilik` =_id_pemilik
		WHERE id_item=_id_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perPetugas` (`_id_pet` VARCHAR(200))  BEGIN
	select * from petugas where id_petugas=_id_pet;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perProduk` (`_id_item` VARCHAR(100))  BEGIN
	select * from item_master inner join satuan on satuan.`id_satuan`=item_master.`satuan` where id_item=_id_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perSuplier` (`_id_suplier` INT(11))  BEGIN
	SELECT * FROM suplier WHERE id_suplier=_id_suplier;
    END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `fc_tambah` (`_id_po` INT) RETURNS INT(11) BEGIN
	declare hasil int;
	set hasil = ( SELECT SUM(jumlah) FROM gudang WHERE id_purchasing=_id_po);
	return hasil;
    END$$

DELIMITER ;

--
-- Dumping data for table `core_company`
--

INSERT INTO `core_company` (`id`, `parent_id`, `company_code`, `company_name`, `company_address`, `creation_time`, `creation_user`, `last_mod_time`, `last_mod_user`) VALUES
(1, 0, 'SYSTEM', 'System Administrator', 'Surabaya', '2018-01-15 13:01:56', 5, '0000-00-00 00:00:00', NULL),
(2, 1, 'TESTER', 'Company Tester', 'Surabaya', '2018-02-18 11:15:24', 51, '0000-00-00 00:00:00', NULL),
(3, 1, 'TESTER3', 'Company Tester2', 'Surabaya', '2018-01-20 09:50:45', 51, '0000-00-00 00:00:00', NULL);

--
-- Dumping data for table `core_menu`
--

INSERT INTO `core_menu` (`id`, `parent_id`, `name`, `controller`, `icon_name`, `sort`, `company_id`, `is_delete`, `creation_user`, `creation_time`, `last_mod_user`, `last_mod_time`, `deletable`) VALUES
(1, 0, 'Add User', 'Pegawai', 'heheheea', 1, 1, 1, 5, '2018-04-03 14:51:22', 1120, '0000-00-00 00:00:00', 1),
(2, 12, 'Add Menu', 'Menu/inputMenu', 'fa fa-plus', NULL, 1, 1, 51, '2018-02-12 09:28:43', 51, '0000-00-00 00:00:00', 1),
(5, 40, 'Roles', 'Role', 'fa fa-tasks', 3, 1, 0, 51, '2018-03-05 01:25:39', NULL, '0000-00-00 00:00:00', 0),
(6, 12, 'Test EasyUI2', 'Test/test2', 'fa fa-envelope2', NULL, 1, 1, 51, '2018-02-19 06:09:21', NULL, '0000-00-00 00:00:00', 1),
(11, 2, 'sdasfafaf', 'dadsdasd', 'asdasdasda', NULL, 1, 1, 51, '2018-02-12 09:28:43', 51, '0000-00-00 00:00:00', 1),
(12, 40, 'Menu', 'menu', 'fa fa-bars', 4, 1, 0, 51, '2018-03-05 01:25:42', 51, '0000-00-00 00:00:00', 0),
(13, 0, 'Cek', 'cek', 'fa fa-list', NULL, 1, 1, 51, '2018-02-12 09:56:59', 51, '0000-00-00 00:00:00', 0),
(14, 0, 'Cek2', 'cek', 'fa fa-list', NULL, 1, 1, 51, '2018-02-12 09:57:04', 51, '0000-00-00 00:00:00', 0),
(15, 0, 'Cek3', 'ce', 'fa fa-list', NULL, 1, 1, 51, '2018-02-19 06:09:26', NULL, '0000-00-00 00:00:00', 0),
(16, 0, 'Cek4', 'cea', 'sasfasf', NULL, 1, 1, 51, '2018-02-19 06:09:28', NULL, '0000-00-00 00:00:00', 0),
(17, 0, 'Laporan', 'Gl', 'fa fa-file', 5, 1, 0, 51, '2018-03-05 01:25:07', NULL, '0000-00-00 00:00:00', 0),
(18, 17, 'Laporan Harian', 'Gl', 'fa fa-file-text', NULL, 1, 0, 51, '2018-02-12 09:58:20', NULL, '0000-00-00 00:00:00', 0),
(19, 41, 'Supplier', 'supplier/listSupplier', 'fa fa-users', 1, 1, 0, 51, '2018-03-05 01:25:51', NULL, '0000-00-00 00:00:00', 0),
(20, 41, 'Produk', 'produk/listItem', 'fa fa-cubes', 2, 1, 0, 51, '2018-03-05 01:25:52', NULL, '0000-00-00 00:00:00', 0),
(22, 42, 'Pembelian', 'pembelian/listPemesanan', 'fa fa-cart-plus', 2, 1, 0, 51, '2018-03-05 01:26:06', NULL, '0000-00-00 00:00:00', 0),
(23, 42, 'Penjualan', 'penjualan/listPenjualan', 'fa fa-money', 1, 1, 0, 51, '2018-03-05 01:26:04', NULL, '0000-00-00 00:00:00', 0),
(25, 0, 'Gudang', 'gudang', 'fa fa-bank', 4, 1, 0, 51, '2018-03-05 01:24:29', 51, '0000-00-00 00:00:00', 0),
(26, 25, 'List Penerimaan', 'gudang/listPenerimaan', 'fa fa-reply-all', 1, 1, 0, 51, '2018-03-05 01:45:49', NULL, '0000-00-00 00:00:00', 0),
(27, 25, 'List Pengeluaran', 'gudang/listPengeluaran', 'fa fa-sign-out', 2, 1, 0, 51, '2018-03-05 01:26:22', NULL, '0000-00-00 00:00:00', 0),
(28, 40, 'Produk', 'produk/listItem', 'fa fa-cube', NULL, 1, 0, 51, '2018-03-05 01:14:03', NULL, '0000-00-00 00:00:00', 0),
(29, 17, 'Balance Sheet', 'gl/laporanbsheet', 'fa fa-file-excel-o', NULL, 1, 0, 51, '2018-03-02 11:21:49', NULL, '0000-00-00 00:00:00', 0),
(30, 17, 'Cash Flow', 'gl/laporancflow', 'fa fa-file-excel-o', NULL, 1, 0, 51, '2018-03-02 11:22:22', NULL, '0000-00-00 00:00:00', 0),
(31, 17, 'Income Statement', 'gl/laporanistate', 'fa fa-file-excel-o', NULL, 1, 0, 51, '2018-03-02 11:22:51', NULL, '0000-00-00 00:00:00', 0),
(32, 0, 'General Ledger', 'gl', 'fa fa-book', NULL, 1, 1, 51, '2018-03-05 01:05:14', 51, '0000-00-00 00:00:00', 0),
(33, 41, 'Kode Akun', 'gl/account', 'fa fa-book', 3, 1, 0, 51, '2018-03-05 01:25:57', NULL, '0000-00-00 00:00:00', 0),
(34, 41, 'Schema Jurnal', 'gl/schema', 'fa fa-booka', 4, 1, 0, 51, '2018-03-05 01:45:03', NULL, '0000-00-00 00:00:00', 0),
(36, 0, 'Master Data', 'master', 'fa fa nabrak tiang listrik', 2, 1, 0, 51, '2018-03-05 01:24:16', NULL, '0000-00-00 00:00:00', 0),
(37, 0, 'Transaksi', 'transaksi', 'fa fa ga pulang beibeh', 3, 1, 0, 51, '2018-03-05 01:24:21', NULL, '0000-00-00 00:00:00', 0),
(40, 0, 'Core Data', 'core', 'fa fa bukuku faaaa', 1, 1, 0, 51, '2018-03-05 01:49:36', NULL, '0000-00-00 00:00:00', 0),
(41, 0, 'Master Data', 'master', 'fa fa mama baru fa', 2, 1, 0, 51, '2018-03-05 03:19:22', NULL, '0000-00-00 00:00:00', 0),
(42, 0, 'Transaksi', 'transaksi', 'fa fa budek ya', 3, 1, 0, 51, '2018-03-05 03:19:24', NULL, '0000-00-00 00:00:00', 0),
(43, 40, 'Company', 'company', 'fa fa company fa', 1, 1, 0, 51, '2018-03-05 01:25:29', NULL, '0000-00-00 00:00:00', 0),
(44, 40, 'User', 'user', 'fa fa-users', 2, 1, 0, 51, '2018-03-05 01:46:50', NULL, '0000-00-00 00:00:00', 0),
(45, 0, 'test', 'afasf', 'assfasf', NULL, 1, 1, 51, '2018-03-05 01:51:12', 51, '0000-00-00 00:00:00', 0),
(46, 42, 'TEST', 'SAAA', 'SFASF', NULL, 1, 0, 51, '2018-03-05 04:49:17', NULL, '0000-00-00 00:00:00', 0),
(47, 41, 'Modal', 'modal/listModal', 'fa fa-money', NULL, 1, 0, 51, '2018-04-18 05:40:18', NULL, '0000-00-00 00:00:00', 0),
(48, 41, 'Aset', 'aset', 'fa fa-book', NULL, 1, 0, 51, '2018-03-05 12:37:46', NULL, '0000-00-00 00:00:00', 0),
(49, 25, 'Stock Item', 'produk/stock', 'fa fa-cubes', NULL, 1, 0, 51, '2018-04-18 02:52:56', NULL, '0000-00-00 00:00:00', 0),
(50, 0, 'Backup', 'backup', '	fa fa-caret-square-o-down', 99, 1, 0, 51, '2018-04-25 01:11:51', NULL, '0000-00-00 00:00:00', 0),
(51, 50, 'Export SQL', 'backup/exportSQL', 'fa fa-book', 99, 1, 0, 51, '2018-04-25 01:11:22', NULL, '0000-00-00 00:00:00', 0),
(52, 50, 'Export CSV', 'backup/exportCSV', 'fa fa-book', 99, 1, 0, 51, '2018-04-25 01:11:16', NULL, '0000-00-00 00:00:00', 0);

--
-- Dumping data for table `core_role`
--

INSERT INTO `core_role` (`id`, `menu_id`, `group_id`, `access`, `company_id`, `creation_user`, `creation_time`, `last_mod_user`, `last_mod_time`) VALUES
(1, 1, 1, 0, 1, 5, '2018-04-25 01:10:57', NULL, '0000-00-00 00:00:00'),
(2, 2, 1, 0, 1, 51, '2018-02-12 09:25:54', NULL, '0000-00-00 00:00:00'),
(6, 5, 1, 1, 1, 51, '2018-02-12 08:56:41', NULL, '0000-00-00 00:00:00'),
(7, 6, 1, 0, 1, 51, '2018-02-04 17:11:29', NULL, '0000-00-00 00:00:00'),
(8, 7, 1, 0, 1, 51, '2018-01-30 08:47:16', NULL, '0000-00-00 00:00:00'),
(9, 8, 1, 0, 1, 51, '2018-01-30 08:47:18', NULL, '0000-00-00 00:00:00'),
(10, 9, 1, 0, 1, 51, '2018-01-30 08:47:23', NULL, '0000-00-00 00:00:00'),
(11, 10, 1, 0, 1, 51, '2018-01-30 08:47:29', NULL, '0000-00-00 00:00:00'),
(12, 11, 1, 0, 1, 51, '2018-02-03 15:19:36', NULL, '0000-00-00 00:00:00'),
(13, 12, 1, 1, 1, 51, '2018-02-12 10:09:53', NULL, '0000-00-00 00:00:00'),
(14, 1, NULL, 0, 1, 51, '2018-02-03 17:13:27', NULL, '0000-00-00 00:00:00'),
(15, 2, NULL, 0, 1, 51, '2018-02-03 17:13:28', NULL, '0000-00-00 00:00:00'),
(16, 5, NULL, 1, 1, 51, '2018-02-03 17:13:29', NULL, '0000-00-00 00:00:00'),
(17, 6, NULL, 0, 1, 51, '2018-02-03 17:13:29', NULL, '0000-00-00 00:00:00'),
(18, 11, NULL, 0, 1, 51, '2018-02-03 17:13:29', NULL, '0000-00-00 00:00:00'),
(19, 12, NULL, 1, 1, 51, '2018-02-03 17:13:29', NULL, '0000-00-00 00:00:00'),
(62, 1, 17, 0, 1, 51, '2018-02-19 06:54:46', NULL, '0000-00-00 00:00:00'),
(63, 2, 17, 0, 1, 51, '2018-02-19 06:54:30', NULL, '0000-00-00 00:00:00'),
(64, 6, 17, 0, 1, 51, '2018-02-19 06:54:31', NULL, '0000-00-00 00:00:00'),
(65, 11, 17, 0, 1, 51, '2018-02-19 06:54:32', NULL, '0000-00-00 00:00:00'),
(66, 5, 17, 0, 1, 51, '2018-03-05 11:16:01', NULL, '0000-00-00 00:00:00'),
(67, 12, 17, 0, 1, 51, '2018-04-25 01:21:38', NULL, '0000-00-00 00:00:00'),
(68, 13, 1, 0, 1, 51, '2018-02-04 15:32:50', NULL, '0000-00-00 00:00:00'),
(69, 14, 1, 0, 1, 51, '2018-02-04 15:33:02', NULL, '0000-00-00 00:00:00'),
(70, 15, 1, 0, 1, 51, '2018-02-04 15:33:14', NULL, '0000-00-00 00:00:00'),
(71, 16, 1, 0, 1, 51, '2018-02-04 15:33:32', NULL, '0000-00-00 00:00:00'),
(72, 17, 1, 1, 1, 51, '2018-02-12 08:41:48', NULL, '0000-00-00 00:00:00'),
(73, 18, 1, 1, 1, 51, '2018-03-02 11:23:12', NULL, '0000-00-00 00:00:00'),
(74, 19, 1, 1, 1, 51, '2018-03-01 12:04:08', NULL, '0000-00-00 00:00:00'),
(75, 20, 1, 1, 1, 51, '2018-03-02 10:57:20', NULL, '0000-00-00 00:00:00'),
(76, 21, 1, 0, 1, 51, '2018-02-19 06:12:39', NULL, '0000-00-00 00:00:00'),
(77, 22, 1, 1, 1, 51, '2018-02-19 06:26:15', NULL, '0000-00-00 00:00:00'),
(78, 23, 1, 1, 1, 51, '2018-03-01 12:04:08', NULL, '0000-00-00 00:00:00'),
(79, 24, 1, 0, 1, 51, '2018-02-19 06:15:13', NULL, '0000-00-00 00:00:00'),
(80, 17, 17, 1, 1, 51, '2018-02-23 12:16:00', NULL, '0000-00-00 00:00:00'),
(81, 18, 17, 1, 1, 51, '2018-03-05 11:13:20', NULL, '0000-00-00 00:00:00'),
(82, 22, 17, 1, 1, 51, '2018-02-19 06:54:43', NULL, '0000-00-00 00:00:00'),
(83, 23, 17, 1, 1, 51, '2018-02-19 06:54:38', NULL, '0000-00-00 00:00:00'),
(84, 20, 17, 1, 1, 51, '2018-02-19 06:54:45', NULL, '0000-00-00 00:00:00'),
(85, 19, 17, 1, 1, 51, '2018-02-19 06:54:29', NULL, '0000-00-00 00:00:00'),
(87, 25, 1, 1, 1, 51, '2018-03-02 01:32:38', NULL, '0000-00-00 00:00:00'),
(88, 26, 1, 1, 1, 51, '2018-03-02 01:32:38', NULL, '0000-00-00 00:00:00'),
(89, 27, 1, 1, 1, 51, '2018-03-02 01:32:38', NULL, '0000-00-00 00:00:00'),
(90, 28, 1, 1, 1, 51, '2018-03-28 07:00:47', NULL, '0000-00-00 00:00:00'),
(91, 29, 1, 1, 1, 51, '2018-03-02 11:23:13', NULL, '0000-00-00 00:00:00'),
(92, 30, 1, 1, 1, 51, '2018-03-02 11:23:13', NULL, '0000-00-00 00:00:00'),
(93, 31, 1, 1, 1, 51, '2018-03-02 11:23:13', NULL, '0000-00-00 00:00:00'),
(94, 32, 1, 0, 1, 51, '2018-03-05 01:14:42', NULL, '0000-00-00 00:00:00'),
(95, 33, 1, 1, 1, 51, '2018-03-04 14:27:22', NULL, '0000-00-00 00:00:00'),
(96, 34, 1, 1, 1, 51, '2018-03-04 14:27:22', NULL, '0000-00-00 00:00:00'),
(99, 40, 1, 1, 1, 51, '2018-03-05 01:14:42', NULL, '0000-00-00 00:00:00'),
(100, 41, 1, 1, 1, 51, '2018-03-05 01:14:42', NULL, '0000-00-00 00:00:00'),
(101, 42, 1, 1, 1, 51, '2018-03-05 01:14:42', NULL, '0000-00-00 00:00:00'),
(102, 43, 1, 1, 1, 51, '2018-03-05 01:14:42', NULL, '0000-00-00 00:00:00'),
(103, 44, 1, 1, 1, 51, '2018-03-05 01:14:42', NULL, '0000-00-00 00:00:00'),
(104, 45, 1, 0, 1, 51, '2018-03-05 01:51:06', NULL, '0000-00-00 00:00:00'),
(105, 46, 1, 0, 1, 51, '2018-04-25 01:10:57', NULL, '0000-00-00 00:00:00'),
(106, 25, 17, 1, 1, 51, '2018-03-05 00:00:00', NULL, '0000-00-00 00:00:00'),
(107, 26, 17, 1, 1, 51, '2018-03-05 00:00:00', NULL, '0000-00-00 00:00:00'),
(108, 27, 17, 1, 1, 51, '2018-03-05 00:00:00', NULL, '0000-00-00 00:00:00'),
(109, 29, 17, 1, 1, 51, '2018-03-05 00:00:00', NULL, '0000-00-00 00:00:00'),
(110, 30, 17, 1, 1, 51, '2018-03-05 00:00:00', NULL, '0000-00-00 00:00:00'),
(111, 31, 17, 1, 1, 51, '2018-03-05 00:00:00', NULL, '0000-00-00 00:00:00'),
(112, 33, 17, 1, 1, 51, '2018-03-05 00:00:00', NULL, '0000-00-00 00:00:00'),
(113, 34, 17, 1, 1, 51, '2018-03-05 00:00:00', NULL, '0000-00-00 00:00:00'),
(114, 40, 17, 0, 1, 51, '2018-03-05 11:16:02', NULL, '0000-00-00 00:00:00'),
(115, 41, 17, 1, 1, 51, '2018-03-05 00:00:00', NULL, '0000-00-00 00:00:00'),
(116, 42, 17, 1, 1, 51, '2018-03-05 00:00:00', NULL, '0000-00-00 00:00:00'),
(117, 43, 17, 0, 1, 51, '2018-03-05 11:16:02', NULL, '0000-00-00 00:00:00'),
(118, 44, 17, 0, 1, 51, '2018-03-05 11:16:02', NULL, '0000-00-00 00:00:00'),
(119, 47, 1, 1, 1, 51, '2018-03-28 06:58:48', NULL, '0000-00-00 00:00:00'),
(120, 47, 17, 1, 1, 51, '2018-04-18 04:57:18', NULL, '0000-00-00 00:00:00'),
(121, 48, 1, 1, 1, 51, '2018-03-05 13:21:54', NULL, '0000-00-00 00:00:00'),
(122, 48, 17, 1, 1, 51, '2018-03-05 00:00:00', NULL, '0000-00-00 00:00:00'),
(123, 5, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(124, 12, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(125, 17, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(126, 18, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(127, 19, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(128, 20, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(129, 22, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(130, 23, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(131, 25, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(132, 26, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(133, 27, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(134, 29, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(135, 30, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(136, 31, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(137, 33, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(138, 34, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(139, 40, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(140, 41, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(141, 42, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(142, 43, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(143, 44, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(144, 48, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(145, 1, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(146, 28, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(147, 46, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(148, 47, 25, 1, 1, 123, '2018-03-27 00:00:00', NULL, '0000-00-00 00:00:00'),
(149, 49, 1, 1, 1, 51, '2018-04-25 01:10:57', NULL, '0000-00-00 00:00:00'),
(150, 28, 17, 1, 1, 51, '2018-04-18 00:00:00', NULL, '0000-00-00 00:00:00'),
(151, 49, 17, 1, 1, 51, '2018-04-18 04:56:48', NULL, '0000-00-00 00:00:00'),
(152, 50, 1, 1, 1, 51, '2018-04-25 01:10:57', NULL, '0000-00-00 00:00:00'),
(153, 51, 1, 1, 1, 51, '2018-04-25 01:10:57', NULL, '0000-00-00 00:00:00'),
(154, 52, 1, 1, 1, 51, '2018-04-25 01:10:57', NULL, '0000-00-00 00:00:00'),
(155, 51, 17, 1, 1, 51, '2018-04-25 00:00:00', NULL, '0000-00-00 00:00:00'),
(156, 52, 17, 1, 1, 51, '2018-04-25 00:00:00', NULL, '0000-00-00 00:00:00'),
(157, 50, 17, 1, 1, 51, '2018-04-25 00:00:00', NULL, '0000-00-00 00:00:00');

--
-- Dumping data for table `gl_account`
--

INSERT INTO `gl_account` (`id`, `coa_id`, `acc_code`, `acc_name`, `deletable`, `other`, `uid`) VALUES
(1, 1, 1000, 'Kas', 0, 0, NULL),
(2, 1, 1001, 'Rekening Bank', 0, 0, NULL),
(3, 1, 1200, 'Piutang Usaha', 0, 0, NULL),
(4, 1, 1210, 'Piutang Lainnya', 0, 0, NULL),
(5, 1, 1300, 'Dana Belum Disetor', 0, 0, NULL),
(6, 1, 1400, 'Persediaan Barang', 0, 0, NULL),
(7, 1, 1500, 'Uang Muka Pembelian', 0, 0, NULL),
(8, 1, 1600, 'Pinjaman Karyawan', 0, 0, NULL),
(9, 1, 1700, 'Pinjaman Lainnya', 0, 0, NULL),
(10, 1, 1800, 'Aset Tetap', 0, 0, NULL),
(11, 1, 1801, 'Penyusutan Aset Tetap', 0, 0, NULL),
(12, 1, 1810, 'Aset Tak Berwujud', 0, 0, NULL),
(13, 1, 1820, 'Investasi', 0, 0, NULL),
(14, 1, 1900, 'PPN Masukan', 0, 0, NULL),
(15, 1, 1910, 'Pajak Masukan lainnya', 0, 0, NULL),
(16, 2, 2000, 'Hutang Usaha', 0, 0, NULL),
(17, 2, 2010, 'Hutang Gaji Karyawan', 0, 0, NULL),
(18, 2, 2020, 'Hutang Dividen', 0, 0, NULL),
(19, 2, 2030, 'Hutang Lainnya', 0, 0, NULL),
(20, 2, 2090, 'Uang Muka Penjualan', 0, 0, NULL),
(21, 2, 2100, 'Hutang Bank', 0, 0, NULL),
(22, 2, 2200, 'PPN Pengeluaran', 0, 0, NULL),
(23, 2, 2210, 'Pengeluaran Pajak Payroll', 0, 0, NULL),
(24, 2, 2230, 'Pengeluaran Pajak Penghasilan Usaha', 0, 0, NULL),
(25, 2, 2299, 'PPH Pengeluaran Lainnya', 0, 0, NULL),
(26, 2, 2910, 'Hutang Dari Pemegang Saham', 0, 0, NULL),
(27, 3, 3000, 'Modal Awal', 0, 0, NULL),
(28, 3, 3100, 'Laba Ditahan', 0, 0, NULL),
(29, 3, 3200, 'Dividen', 0, 0, NULL),
(30, 3, 3900, 'Ekuitas Saldo Awal', 0, 0, NULL),
(31, 4, 4000, 'Penjualan', 0, 0, NULL),
(32, 4, 4100, 'Diskon Penjualan', 0, 0, NULL),
(33, 4, 4200, 'Retur Penjualan', 0, 0, NULL),
(34, 5, 5000, 'Harga Pokok Penjualan', 0, 0, NULL),
(35, 5, 5100, 'Diskon Penjualan', 0, 0, NULL),
(36, 5, 5200, 'Retur Pembelian', 0, 0, NULL),
(37, 5, 5300, 'Pengiriman dan Pengangkutan', 0, 0, NULL),
(38, 5, 5900, 'Biaya Produksi', 0, 0, NULL),
(40, 6, 6000, 'Iklan & Promosi', 0, 0, NULL),
(41, 6, 6001, 'Piutang Tak Tertagih', 0, 0, NULL),
(42, 6, 6002, 'Bank', 0, 0, NULL),
(43, 6, 6003, 'Kontribusi Sosial', 0, 0, NULL),
(44, 6, 6004, 'Biaya Tenaga Kerja', 0, 0, NULL),
(45, 6, 6005, 'Komisi & Upah', 0, 0, NULL),
(46, 6, 6006, 'Biaya Pembuangan', 0, 0, NULL),
(47, 6, 6007, 'Iuran & Langganan', 0, 0, NULL),
(48, 6, 6008, 'Hiburan', 0, 0, NULL),
(49, 6, 6009, 'Makanan Hiburan', 0, 0, NULL),
(50, 6, 6010, 'Penyewaan Alat', 0, 0, NULL),
(51, 6, 6011, 'Asuransi', 0, 0, NULL),
(52, 6, 6012, 'Bunga Hutang', 0, 0, NULL),
(53, 6, 6013, 'Bahan Pekerjaan', 0, 0, NULL),
(54, 6, 6014, 'Legal & Profesional', 0, 0, NULL),
(55, 6, 6015, 'Pengobatan', 0, 0, NULL),
(56, 6, 6016, 'Biaya Kantor', 0, 0, NULL),
(57, 6, 6017, 'Biaya Administrasi & Umum Lainnya', 0, 0, NULL),
(58, 6, 6018, 'Sewa Tempat', 0, 0, NULL),
(59, 6, 6019, 'Pemeliharaan & Perbaikan Gedung', 0, 0, NULL),
(60, 6, 6020, 'Alat Tulis Kantor & Printing', 0, 0, NULL),
(61, 6, 6021, 'Bea Materai', 0, 0, NULL),
(62, 6, 6022, 'Pemborong', 0, 0, NULL),
(63, 6, 6023, 'Persediaan & Bahan', 0, 0, NULL),
(64, 6, 6024, 'Pajak & Lisensi', 0, 0, NULL),
(65, 6, 6025, 'Alat-alat', 0, 0, NULL),
(66, 6, 6026, 'Perjalanan & Transportasi', 0, 0, NULL),
(67, 6, 6027, 'Makanan Perjalanan', 0, 0, NULL),
(68, 6, 6028, 'Fasilitas/Utilitas', 0, 0, NULL),
(70, 7, 7000, 'Pendapatan Pengiriman & Pengangkutan', 0, 0, NULL),
(71, 7, 7100, 'Pendapatan Bunga & Jasa Giro', 0, 0, NULL),
(72, 7, 7900, 'Pendapatan Lainnya', 0, 0, NULL),
(73, 8, 8000, 'Pengeluaran Lainnya', 0, 0, NULL),
(74, 8, 8001, 'Biaya Amortisasi', 0, 0, NULL),
(75, 8, 8002, 'Biaya Penyusutan', 0, 0, NULL),
(77, 8, 8900, 'Penyesuaian Persediaan Barang', 0, 0, NULL),
(78, 6, 6029, 'Kendaraan & Mesin', 0, 0, NULL),
(79, 6, 6030, 'Denda & Hukuman', 0, 0, NULL),
(80, 6, 6031, 'Upah & Gaji', 0, 0, NULL),
(81, 6, 6032, 'Bonus Karyawan', 0, 0, NULL),
(82, 6, 6900, 'Pengeluaran Barang Rusak', 0, 0, NULL),
(83, 8, 8003, 'Untung/Rugi Pertukaran Kurs', 0, 0, NULL),
(101, 1, 1, 'Assets', 0, 0, NULL),
(102, 2, 2, 'Liabilities', 0, 0, NULL),
(103, 3, 3, 'Equity', 0, 0, NULL),
(104, 4, 4, 'Income', 0, 0, NULL),
(105, 5, 5, 'COGS', 0, 0, NULL),
(106, 6, 6, 'Expense', 0, 0, NULL),
(107, 7, 7, 'Other Income', 0, 0, NULL),
(108, 8, 8, 'Other Expense', 0, 0, NULL),
(109, 2, 20301, 'Hutang Temen', 0, 0, '9999');

--
-- Dumping data for table `gl_group`
--

INSERT INTO `gl_group` (`id`, `coa_number`, `coa_name`) VALUES
(1, '1', 'Assets'),
(2, '2', 'Liabilities'),
(3, '3', 'Equity'),
(4, '4', 'Income'),
(5, '5', 'COGS'),
(6, '6', 'Expenses'),
(7, '7', 'Other Income'),
(8, '8', 'Other Expense');

--
-- Dumping data for table `gl_has`
--

INSERT INTO `gl_has` (`id`, `parent`, `child`, `uid`) VALUES
(1, NULL, 1, NULL),
(2, NULL, 2, NULL),
(3, NULL, 3, NULL),
(4, NULL, 4, NULL),
(5, NULL, 5, NULL),
(6, NULL, 6, NULL),
(7, NULL, 7, NULL),
(8, NULL, 8, NULL),
(9, 1, 1000, NULL),
(10, 1000, 1001, NULL),
(11, 1, 1200, NULL),
(12, 1200, 1210, NULL),
(13, 1, 1300, NULL),
(14, 1, 1400, NULL),
(15, 1, 1500, NULL),
(16, 1, 1600, NULL),
(17, 1, 1700, NULL),
(18, 1, 1800, NULL),
(19, 1800, 1801, NULL),
(20, 1800, 1810, NULL),
(21, 1800, 1820, NULL),
(22, 1, 1900, NULL),
(23, 1900, 1910, NULL),
(24, 2, 2000, NULL),
(25, 2000, 2010, NULL),
(26, 2000, 2020, NULL),
(27, 2000, 2030, NULL),
(28, 2000, 2090, NULL),
(29, 2, 2100, NULL),
(30, 2, 2200, NULL),
(31, 2200, 2210, NULL),
(32, 2200, 2230, NULL),
(33, 2200, 2299, NULL),
(34, 2, 2910, NULL),
(35, 3, 3000, NULL),
(36, 3, 3100, NULL),
(37, 3, 3200, NULL),
(38, 3, 3900, NULL),
(39, 4, 4000, NULL),
(40, 4, 4100, NULL),
(41, 4, 4200, NULL),
(42, 5, 5000, NULL),
(43, 5, 5100, NULL),
(44, 5, 5200, NULL),
(45, 5, 5300, NULL),
(46, 5, 5900, NULL),
(47, 6, 6000, NULL),
(48, 6000, 6001, NULL),
(49, 6000, 6002, NULL),
(50, 6000, 6003, NULL),
(51, 6000, 6004, NULL),
(52, 6000, 6005, NULL),
(53, 6000, 6006, NULL),
(54, 6000, 6007, NULL),
(55, 6000, 6008, NULL),
(56, 6000, 6009, NULL),
(57, 6000, 6010, NULL),
(58, 6000, 6011, NULL),
(59, 6000, 6012, NULL),
(60, 6000, 6013, NULL),
(61, 6000, 6014, NULL),
(62, 6000, 6015, NULL),
(63, 6000, 6016, NULL),
(64, 6000, 6017, NULL),
(65, 6000, 6018, NULL),
(66, 6000, 6019, NULL),
(67, 6000, 6020, NULL),
(68, 6000, 6021, NULL),
(69, 6000, 6022, NULL),
(70, 6000, 6023, NULL),
(71, 6000, 6024, NULL),
(72, 6000, 6025, NULL),
(73, 6000, 6026, NULL),
(74, 6000, 6027, NULL),
(75, 6000, 6028, NULL),
(76, 6000, 6029, NULL),
(77, 6000, 6030, NULL),
(78, 6000, 6031, NULL),
(79, 6000, 6032, NULL),
(80, 6, 6900, NULL),
(81, 7, 7000, NULL),
(82, 7, 7100, NULL),
(83, 7, 7900, NULL),
(84, 8, 8000, NULL),
(85, 8000, 8001, NULL),
(86, 8000, 8002, NULL),
(87, 8000, 8003, NULL),
(88, 8, 8900, NULL),
(89, 2030, 20301, '9999');

--
-- Dumping data for table `gl_schema_h`
--

INSERT INTO `gl_schema_h` (`id`, `journal_name`, `uid`) VALUES
(1, 'Pembelian', NULL),
(2, 'Penjualan', NULL),
(3, 'Pembelian Aset', NULL);

--
-- Dumping data for table `gl_schema_l`
--

INSERT INTO `gl_schema_l` (`id`, `journal_id`, `acc_id`, `line_debit`, `line_credit`, `uid`) VALUES
(1, 1, 1000, '0', 'HB', '51'),
(3, 1, 1400, 'HB', '0', '51'),
(4, 2, 1000, 'HJ', '0', '51'),
(5, 2, 1400, '0', 'HB', '51'),
(6, 2, 4000, '0', 'HJ', '51'),
(7, 2, 5000, 'HB', '0', '51'),
(8, 3, 1000, '0', 'HB', '51'),
(9, 3, 1800, 'HB', '0', '51');

--
-- Dumping data for table `gl_variable`
--

INSERT INTO `gl_variable` (`id`, `v_code`, `v_desc`, `uid`) VALUES
(1, 'HB', 'Harga Beli', NULL),
(2, 'HJ', 'Harga Jual', NULL),
(3, '0', 'Kosong', NULL),
(4, 'HJ-HB', 'Profit', NULL);

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `nama`, `jenkel`, `alamat`, `hp`, `email`, `jabatan`, `photo_link`, `password`, `company_id`, `privilege`, `is_delete`, `created_by`, `created_time`, `last_mod_by`, `last_mod_time`) VALUES
('1', 'Renanda Agustiantoro', 'L', 'Surabaya', '087878878878', 'recta@user.com', '', '', '319f4d26e3c536b5dd871bb2c52e3178', 1, 4, 0, 0, '2018-01-15 17:14:02', NULL, NULL),
('1111', 'A-Mart', 'L', 'Surabaya', '123456', 'team1@mbd.com', 'Player', '', '2d5b2b3d2454b24e20ed42c6b6557465', 1, 17, 0, 51, '2018-03-21 18:39:27', NULL, NULL),
('1112', 'B-Mart', 'L', 'Surabaya', '123456', 'team2@mbd.com', 'Player', '', '60962ab1d5d11d479644b34c182ea283', 1, 17, 0, 51, '2018-03-21 18:40:13', NULL, NULL),
('1113', 'C-Mart', 'L', 'Surabaya', '123456', 'team3@mbd.com', 'Player', '', '865ec7e2959da256a2a97a8b39a3ba2a', 1, 17, 0, 51, '2018-03-21 18:41:20', NULL, NULL),
('1114', 'D-Mart', 'L', 'Surabaya', '123456', 'team4@mbd.com', 'Player', '', '8512eed920a9b68c84fbb7cc7454378c', 1, 17, 0, 51, '2018-03-21 18:42:00', NULL, NULL),
('1115', 'E-Mart', 'L', 'Surabaya', '123456', 'team5@mbd.com', 'Player', '', '3e2b15fb0e41329efc1f62378ca5186b', 1, 17, 0, 51, '2018-03-21 18:42:43', NULL, NULL),
('1116', 'F-Mart', 'L', 'Surabaya', '123456', 'team6@mbd.com', 'Player', '', '73de29021fd0d8d2cfd204d2d955a46d', 1, 17, 0, 51, '2018-03-21 18:45:22', NULL, NULL),
('1117', 'G-Mart', 'L', 'Surabaya', '123456', 'team7@mbd.com', 'Player', '', 'a74161293d644d8f1ccca5be3f034df8', 1, 17, 0, 51, '2018-03-21 18:47:54', NULL, NULL),
('1118', 'H-Mart', 'L', 'Surabaya', '123456', 'team8@mbd.com', 'Player', '', '9bd414328e8c2658731938a05963c1db', 1, 17, 0, 51, '2018-03-21 18:48:32', NULL, NULL),
('1119', 'I-Mart', 'L', 'Surabaya', '123456', 'team9@mbd.com', 'Player', '', '2e6ebca3b60eeec1da4b9e9be7ab52fb', 1, 17, 0, 51, '2018-03-21 18:49:58', NULL, NULL),
('1120', 'Toko Berkah', 'L', 'Surabaya', '123456', 'team10@mbd.com', 'Administrator', '', '305e9e107e681eace3276d661a690d74', 1, 17, 0, 51, '2018-03-21 18:51:59', NULL, NULL),
('1121', 'K-Mart', 'L', 'Surabaya', '123456', 'team11@mbd.com', 'Player', '', '9cbd94ff695e137c62ee63ef920d4665', 1, 17, 0, 51, '2018-03-21 18:51:59', NULL, NULL),
('1122', 'L-Mart', 'L', 'Surabaya', '123456', 'team12@mbd.com', 'Player', '', '104c3b90fcb6da8e653c05eb1137f8ef', 1, 17, 0, 51, '2018-03-21 18:51:59', NULL, NULL),
('1123', 'SUSU MAHAL', 'L', 'Surabaya', '123456', 'team13@mbd.com', 'Player', '', 'aa8d051910780a557dc4a37f183f1a68', 1, 17, 0, 51, '2018-03-21 18:51:59', NULL, NULL),
('1124', 'M-Mart', 'L', 'Surabaya', '123456', 'team14@mbd.com', 'Player', '', 'cf3bd921c50ff37026e8a011fcaefafa', 1, 17, 0, 51, '2018-03-21 18:51:59', NULL, NULL),
('1125', 'N-Mart', 'L', 'Surabaya', '123456', 'team15@mbd.com', 'Player', '', '89b9547cbe73c8f0d63e42b4245870a1', 1, 17, 0, 51, '2018-03-21 18:51:59', NULL, NULL),
('1126', 'O-Mart', 'L', 'Surabaya', '123456', 'team16@mbd.com', 'Player', '', 'c2d690252d9b1e8f036497fb969c72c0', 1, 17, 0, 51, '2018-03-21 18:51:59', NULL, NULL),
('1127', 'P-Mart', 'L', 'Surabaya', '123456', 'team17@mbd.com', 'Player', '', 'd782924e733c817e5af717fdda8914a7', 1, 17, 0, 51, '2018-03-21 18:51:59', NULL, NULL),
('1235557', 'Indomerk', 'L', 'dsfafsaf', '1223231111', 'player1@game.com', 'Player', '', '5d2bbc279b5ce75815849d5e3f0533ec', 1, 17, 0, 51, '2018-02-18 11:12:40', NULL, NULL),
('3115080508950041', 'Renanda Agustiantoro', 'L', 'Surabaya', '087878878878', 'homina@homina.com', '', '', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 1, 0, 0, '2018-01-16 15:06:48', NULL, NULL),
('41253151', 'Alfamerk', 'L', 'safasfasf', '12547564', 'player2@game.com', 'Player', '', '88e77ff74930f37010370c296d14737b', 1, 17, 0, 51, '2018-02-18 11:16:05', NULL, NULL),
('51', 'Administrator', 'L', 'Surabaya', '0', 'admin@admin.com', 'System Developer', '', '21232f297a57a5a743894a0e4a801fc3', 1, 1, 0, 1, '2018-01-15 12:15:05', 0, '2018-01-15 12:15:05'),
('9999', 'User Tester', 'P', 'Surabaya', '12345555', 'user@tester.com', 'Player', '', 'f5d1278e8109edd94e1e4197e04873b9', 1, 17, 0, 51, '2018-03-22 13:38:31', NULL, NULL);

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`roleId`, `parent_roleId`, `role`, `company_id`) VALUES
(1, 1, 'System Administrator', 1),
(2, 1, 'Admin2', 1),
(3, 1, 'Admin Company 1', 1),
(24, 1, 'asdasdf', NULL),
(23, 1, 'sdasdsadasf', NULL),
(22, 1, 'sfasfasasd', NULL),
(21, 1, 'dafasasf', NULL),
(20, 1, 'sazfsgsdbxdb', NULL),
(18, 1, 'testsss', NULL),
(17, 1, 'Player', NULL),
(19, 1, 'nyoba', NULL),
(25, 1, 'asdtasd', NULL),
(26, 1, 'Player2', NULL),
(27, 17, 'Teest1', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
