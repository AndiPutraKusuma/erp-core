-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 07, 2018 at 08:03 AM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `retail_core`
--

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `sp_alert_stok`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_alert_stok` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT tb1.nama_item,tb1.id_suplier,tb1.nama_item,tb1.`id_item`,tb1.tipe,tb1.satuan,SUM(tb1.`jumlah`) AS jumlah,tb1.hargaSatuan,tb1.link_photo,tb1.deskripsi
	FROM (SELECT id_suplier,nama_item,item_master.`id_item`,tipe,satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_produksi`=penerimaan_barang.`id_produksi`
	INNER JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	where item_master.`id_pemilik`=_id_pemilik
	GROUP BY item_master.`id_item`
UNION
	SELECT id_suplier,nama_item,item_master.`id_item`,tipe,satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	where item_master.`id_pemilik`=_id_pemilik
	 GROUP BY item_master.`id_item` ORDER BY nama_item ASC) AS tb1 WHERE tb1.jumlah<20 GROUP BY tb1.nama_item;
    END$$

DROP PROCEDURE IF EXISTS `sp_cariCustomer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariCustomer` (`_keyword` VARCHAR(300))  BEGIN
	SELECT * FROM customer WHERE nama LIKE CONCAT('%',_keyword,'%') or id_customer like CONCAT('%',_keyword,'%') ;
    END$$

DROP PROCEDURE IF EXISTS `sp_cariPetugas`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariPetugas` (`_keyword` VARCHAR(300))  BEGIN
	select * from petugas where nama like CONCAT('%',_keyword,'%') or id_petugas LIKE CONCAT('%',_keyword,'%');
    END$$

DROP PROCEDURE IF EXISTS `sp_cariProduk`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariProduk` (`cari_suplier` VARCHAR(200))  BEGIN
	SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE nama_item LIKE CONCAT('%',cari_suplier,'%')
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC;
 
    END$$

DROP PROCEDURE IF EXISTS `sp_cariSuplier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariSuplier` (`_keyword` VARCHAR(300))  BEGIN
	SELECT * FROM suplier WHERE nama_suplier LIKE CONCAT('%',_keyword,'%') or id_suplier LIKE CONCAT('%',_keyword,'%');
    END$$

DROP PROCEDURE IF EXISTS `sp_cari_suplierProduk`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cari_suplierProduk` (`cari_suplier` VARCHAR(200))  BEGIN
	SELECT nama_suplier,suplier.`id_suplier`,item_master.`id_item`,nama_item,satuan,tipe, SUM(gudang.`jumlah`)AS jumlah,
	 link_photo,item_master.deskripsi,detail_penerimaan.hargaSatuan 
	FROM detail_penerimaan LEFT JOIN gudang 
	ON detail_penerimaan.`id_purchasing`=gudang.`id_purchasing` AND
	detail_penerimaan.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_penerimaan.`id_item`
	INNER JOIN penerimaan_barang ON penerimaan_barang.`id_po`=detail_penerimaan.`id_purchasing`
	INNER JOIN suplier ON suplier.`id_suplier`=penerimaan_barang.`id_suplier`
	 WHERE nama_item LIKE CONCAT('%',cari_suplier,'%') GROUP BY detail_penerimaan.`id_item`;
	 
    END$$

DROP PROCEDURE IF EXISTS `sp_cekStok`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cekStok` (`_barcode` VARCHAR(100), `_harga` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
SELECT gudang.`id_rec`,detail_suplier.`id_suplier` ,detail_suplier.`id_item`,gudang.`jumlah`,hargaSatuan 
FROM gudang 
INNER JOIN detail_suplier ON detail_suplier.`id_suplier`=gudang.`id_supplier` AND detail_suplier.`id_item`=gudang.`id_item`
WHERE gudang.`barcode_barang`=_barcode
ORDER BY gudang.`id_rec` asc LIMIT 1;
    END$$

DROP PROCEDURE IF EXISTS `sp_cekStok2`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cekStok2` (`_id_item` VARCHAR(100), `_id_supplier` INT(11), `_harga` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
SELECT gudang.`id_rec`,detail_suplier.`id_suplier`,detail_suplier.`id_item`,gudang.`jumlah`,hargaSatuan FROM penerimaan_barang 
INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
WHERE detail_suplier.`id_item`=_id_item and penerimaan_barang.`id_suplier`=_id_supplier  and gudang.`hargaSatuan`=_harga and penerimaan_barang.`id_pemilik`=_id_pemilik
ORDER BY gudang.`id_rec` asc LIMIT 1;
    END$$

DROP PROCEDURE IF EXISTS `sp_cek_akses`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cek_akses` (`_email` VARCHAR(200))  BEGIN
    set@idPet=(select id_petugas from petugas where petugas.`email`=_email);
	select privilege from petugas where id_petugas=@idPet;
    END$$

DROP PROCEDURE IF EXISTS `sp_deleteCustomer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteCustomer` (`_id_customer` VARCHAR(100))  BEGIN
	set@cek=(select id_customer from penjualan where id_customer=_id_customer limit 1);
	IF(@cek) THEN
			SELECT -1 AS cek;
		ELSE
		DELETE FROM customer WHERE id_customer=_id_customer;
			SELECT 1 AS cek;
	end if;
	
    END$$

DROP PROCEDURE IF EXISTS `sp_deleteSuplier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteSuplier` (`_id_suplier` INT(11))  BEGIN
	SET@cek=(SELECT id_suplier FROM purchasing WHERE id_suplier=_id_suplier LIMIT 1);
	IF(@cek) THEN
			SELECT -1 AS cek;
	ELSE
	delete from suplier where id_suplier=_id_suplier;
	select 1 as cek;
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_delete_item`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_item` (`_id_item` VARCHAR(100))  BEGIN
	set@cek=(select id_item from detail_suplier where id_item=_id_item limit 1);
	if(@cek) then
		select -1 as cek;
	else
		delete from item_master where id_item=_id_item;
		select 1 as cek;
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_delete_satuan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_satuan` (`_id_satuan` INT(11))  BEGIN
	set@child=(select satuan from item_master where satuan=_id_satuan);
	if(@child) then
		select -1 as cek;
	else
		delete from satuan where id_satuan=_id_satuan;
		set@cek=(select id_satuan from satuan where id_satuan=_id_satuan);
		if(@cek) then
			select -1 as cek;
		else
			select 1 as cek;
		end if;
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_deletPetugas`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deletPetugas` (`_id_petugas` VARCHAR(100))  BEGIN
	SET@cek=(SELECT id_petugas FROM penjualan WHERE id_petugas=_id_petugas LIMIT 1);
	SET@cek1=(SELECT id_petugas FROM purchasing WHERE id_petugas=_id_petugas LIMIT 1);
	SET@cek2=(SELECT id_petugas FROM service WHERE id_petugas=_id_petugas LIMIT 1);
	SET@cek3=(SELECT teknisi FROM solving WHERE teknisi=_id_petugas LIMIT 1);
	IF(@cek or @cek1 or @cek2 or @cek3 ) THEN
			SELECT -1 AS cek;
	ELSE
	delete from petugas where id_petugas=_id_petugas;
	select 1 as cek;
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_detail_pembelian`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_detail_pembelian` (`_id_pembelian` VARCHAR(100), `_id_produk` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11))  BEGIN
	insert into detail_pembelian (id_transaksi,id_produk,jumlah,harga) values(_id_pembelian,_id_produk,_jumlah,_harga);
	UPDATE produk SET jumalah=jumlah-_jumlah WHERE id_produk=_id_produk;
    END$$

DROP PROCEDURE IF EXISTS `sp_detail_pengeluaran`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_detail_pengeluaran` (`_id_issue` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` INT(11), `_harga` INT(11), `_id_suplier` INT(11), `_id_rec` VARCHAR(100), `_kode` INT(11), `_barcode` VARCHAR(100))  BEGIN
	
	insert into detail_pengeluaran(id_issue,id_item,jumlah,harga,id_suplier,id_rec,barcode_barang)
	values(_id_issue,_id_item,_jumlah,_harga,_id_suplier,_id_rec,_barcode);
		if(_kode=1) then
		SET@idSO=(SELECT id_so FROM pengeluaran_barang WHERE pengeluaran_barang.`id_issue`=_id_issue);
		SET@count1=(SELECT SUM(jumlah)  FROM detail_penjualan WHERE id_so=@idSO);
		SET@count2=(SELECT SUM(jumlah) FROM detail_pengeluaran WHERE id_issue=_id_issue);
		if(@count2>=@count1) then
			update penjualan set `status`=3 where id_so=@idSO;
			update pengeluaran_barang set `status`=3 where id_issue=_id_issue;
		end if;
	elseif(_kode=2) then
		set@idPro=(select id_produksi from pengeluaran_barang where pengeluaran_barang.`id_issue`=_id_issue);
		SET@count1=(SELECT SUM(jumlah)  FROM detail_produksi WHERE id_produksi=@idPro);
		SET@count2=(SELECT SUM(jumlah) FROM detail_pengeluaran WHERE id_issue=_id_issue);
		IF(@count2>=@count1) THEN
			UPDATE produksi SET `status`=3 WHERE id_produksi=@idPro;
			UPDATE pengeluaran_barang SET `status`=3 WHERE id_issue=_id_issue;
		END IF;
	ELSEIF(_kode=3) THEN
		SET@idPro=(SELECT pengeluaran_barang.`id_produkService` FROM pengeluaran_barang WHERE pengeluaran_barang.`id_issue`=_id_issue);
		SET@count1=(SELECT SUM(jumlah)  FROM detail_produkservice WHERE id_produkService=@idPro);
		SET@count2=(SELECT SUM(jumlah) FROM detail_pengeluaran WHERE id_issue=_id_issue);
		IF(@count2>=@count1) THEN
			UPDATE produkservice SET `status`=3 WHERE id_produkService=@idPro;
			UPDATE pengeluaran_barang SET `status`=3 WHERE id_issue=_id_issue;
		END IF;
	end if;
	
    END$$

DROP PROCEDURE IF EXISTS `sp_filter_laporanPembelian`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_filter_laporanPembelian` (`_tgl_awal` DATE, `_tgl_akhir` DATE, `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penerimaan_barang.`id_rec` ,tanggal_receive,nama_item,sat.nama_satuan,jumlah,petugas.`nama` AS nama_petugas,kurir,nama_suplier
	FROM detail_penerimaan 
	INNER JOIN penerimaan_barang ON detail_penerimaan.`id_rec`=penerimaan_barang.`id_rec`
	INNER JOIN item_master ON detail_penerimaan.`id_item`=item_master.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan` 
	INNER JOIN petugas ON penerimaan_barang.`id_petugas`=petugas.`id_petugas`
	INNER JOIN suplier ON penerimaan_barang.`id_suplier`=suplier.`id_suplier`
	where penerimaan_barang.`status`=3 and penerimaan_barang.`tanggal_receive`>=_tgl_awal 
	and penerimaan_barang.`tanggal_receive`<=_tgl_akhir and penerimaan_barang.`id_pemilik`=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_filter_laporanPenjualan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_filter_laporanPenjualan` (`_tgl_awal` DATE, `_tgl_akhir` DATE, `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penjualan.`id_so`,tanggal,nama_item,sat.nama_satuan,jumlah,petugas.`nama` AS nama_petugas,kurir FROM detail_penjualan 
	INNER JOIN penjualan ON detail_penjualan.`id_so`=penjualan.`id_so`
	INNER JOIN item_master ON detail_penjualan.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan` 
	INNER JOIN petugas ON penjualan.`id_petugas`=petugas.`id_petugas`
	where penjualan.`status`=3 and penjualan.`tanggal`>=_tgl_awal and penjualan.`tanggal`<=_tgl_akhir and penjualan.`id_pemilik`=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_get_userId`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_userId` (`_email` VARCHAR(200))  BEGIN
	select id_petugas from petugas where email=_email;
    END$$

DROP PROCEDURE IF EXISTS `sp_hitungCustomer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungCustomer` (`_id_pemilik` VARCHAR(100))  BEGIN
	select count(id_customer) as jumlah from customer where customer.`id_pemilik`=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_hitungPembelian`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPembelian` (`_id_pemilik` VARCHAR(100))  BEGIN
	select count(purchasing.`id_po`) as jumlah from purchasing where purchasing.`id_pemilik`=_id_pemilik ;
    END$$

DROP PROCEDURE IF EXISTS `sp_hitungPembelianSukses`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPembelianSukses` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(purchasing.`id_po`) AS jumlah FROM purchasing where purchasing.`status`=3 and purchasing.`id_pemilik`=_id_pemilik ;
    END$$

DROP PROCEDURE IF EXISTS `sp_hitungPenerimaan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPenerimaan` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(penerimaan_barang.`id_rec`) AS jumlah FROM penerimaan_barang where penerimaan_barang.`id_pemilik`=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_hitungPengeluaran`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPengeluaran` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(pengeluaran_barang.`id_issue`)AS jumlah FROM pengeluaran_barang where pengeluaran_barang.`id_pemilik`=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_hitungPenjualan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPenjualan` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(penjualan.`id_so`) AS jumlah FROM penjualan where penjualan.`id_pemilik`=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_hitungPenjualanSukses`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPenjualanSukses` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(penjualan.`id_so`) AS jumlah FROM penjualan where penjualan.`status`=3 and penjualan.`id_pemilik`=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_hitungProduk`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungProduk` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(item_master.`id_item`) AS jumlah FROM item_master where item_master.`id_pemilik`=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_hitungSupplier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungSupplier` (`_id_pemilik` VARCHAR(100))  BEGIN
	select count(id_suplier) as jumlah from suplier where suplier.`id_pemilik`=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_inputProduk_supplier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_inputProduk_supplier` (`_idSupplier` INT(11), `_id_item` VARCHAR(100), `_id_pemilik` VARCHAR(100), `_harga` INT(11))  BEGIN
	set@cek=(select id_suplier from detail_suplier where id_suplier=_idSupplier and id_item=_id_item);
	if(@cek) then
		select -1 as cek;
	else
		insert into detail_suplier(id_suplier,id_item,id_pemilik,harga) values(_idSupplier,_id_item,_id_pemilik,_harga);
		select 1 as cek;
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_input_customer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_customer` (`_idCustomer` VARCHAR(100), `_id_institut` VARCHAR(100), `_nama` VARCHAR(200), `_jenkel` VARCHAR(3), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_jabatan` VARCHAR(100), `_id_pemilik` VARCHAR(100))  BEGIN
	set@cek=(select id_customer from customer where id_customer=_idCustomer);
	if(@cek) then
		select -1 as cek;
	else
		insert into `customer` (id_customer,id_institut,nama,jenkel,alamat,hp,email,tgl,jabatan,id_pemilik) 
		values(_idCustomer,_id_institut,_nama,_jenkel,_alamat,_hp,_email,now(),_jabatan,_id_pemilik);
		select 0 as cek;
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_input_detailKeluarGudang`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailKeluarGudang` (`_id_tran` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_id_suplier` INT(11))  BEGIN
	update detail_penjualan set jumlah_keluar=_jumlah where id_transaksi=_id_tran and id_item=_id_item;
    END$$

DROP PROCEDURE IF EXISTS `sp_input_detailPenjualan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailPenjualan` (`_id_so` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11), `_idSuplier` INT(11), `_barcode` VARCHAR(100), `_idPemilik` VARCHAR(100))  BEGIN
	INSERT INTO detail_penjualan (id_so,id_item,jumlah,harga,barcode_barang,id_pemilik,id_suplier)
	VALUES(_id_so,_id_item,_jumlah,_harga,_barcode,_idPemilik,_idSuplier);
	    END$$

DROP PROCEDURE IF EXISTS `sp_input_detailProduksi`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailProduksi` (`_id_produksi` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_hargaSatuan` INT(11), `_id_suplier` INT(11))  BEGIN
	INSERT INTO detail_produksi(id_produksi,id_item,jumlah,hargaSatuan,id_suplier) VALUES(_id_produksi,_id_item,_jumlah,_hargaSatuan,_id_suplier);
    END$$

DROP PROCEDURE IF EXISTS `sp_input_detailPurchasing`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailPurchasing` (`_id_purchasing` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_hargaSatuan` INT(11), `_idSupplier` INT(11))  BEGIN
	insert into detail_purchasing(id_purchasing,id_item,jumlah,hargaSatuan,id_supplier) values(_id_purchasing,_id_item,_jumlah,_hargaSatuan,_idSupplier);
    END$$

DROP PROCEDURE IF EXISTS `sp_input_gudang`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_gudang` (`_id_rec` VARCHAR(200), `_id_tran` VARCHAR(200), `_id_item` VARCHAR(200), `_jumlah` DOUBLE, `_harga` INT(11), `_kode` INT(11), `_id_pemilik` VARCHAR(100), `_barcode` VARCHAR(100), `_idSupplier` INT(11))  BEGIN
	if(_kode=1) then
	set@cekPO=(select jumlah from detail_purchasing where id_purchasing=_id_tran and id_item=_id_item);
	if (_jumlah>=@cekPO) then
		insert into gudang (id_rec,id_item,jumlah,hargaSatuan,id_pemilik,barcode_barang,id_supplier) values(_id_rec,_id_item,_jumlah,_harga,_id_pemilik,_barcode,_idSupplier);
		INSERT INTO detail_penerimaan (id_rec,id_item,jumlah,hargaSatuan,id_pemilik,barcode_barang,id_supplier) VALUES(_id_rec,_id_item,_jumlah,_harga,_id_pemilik,_barcode,_idSupplier);
		set@count1=(SELECT SUM(jumlah)  FROM detail_purchasing WHERE id_purchasing=_id_tran);
		set@count2=(select sum(jumlah) from gudang where id_rec=_id_rec);
		if (@count2>=@count1) then 
			update purchasing set `status`=3 where id_po=_id_tran;
			UPDATE penerimaan_barang SET `status`=3 WHERE id_rec=_id_rec;
		end if;
	else
		INSERT INTO gudang (id_rec,id_item,jumlah,hargaSatuan,`status`,id_pemilik,barcode_barang) VALUES(_id_rec,_id_item,_jumlah,_harga,(_jumlah-@cekPO),_id_pemilik,_barcode);
		INSERT INTO detail_penerimaan (id_rec,id_item,jumlah,hargaSatuan,`status`,id_pemilik,barcode_barang) VALUES(_id_rec,_id_item,_jumlah,_harga,(_jumlah-@cekPO),_id_pemilik,_barcode);
		INSERT INTO defect (id_rec,id_item,jumlah,hargaSatuan,tanggal,keterangan,`status`,id_pemilik) 
		VALUES(_id_rec,_id_item,(@cekPO-_jumlah),_harga,now(),1,1,_id_pemilik);
		UPDATE purchasing SET `status`=2 WHERE id_po=_id_tran;
		UPDATE penerimaan_barang SET `status`=2 WHERE id_rec=_id_rec; 
	end if;
	elseif(_kode=2)then
		INSERT INTO gudang (id_rec,id_item,jumlah,hargaSatuan,id_pemilik,barcode_barang) VALUES(_id_rec,_id_item,_jumlah,_harga,_id_pemilik,_barcode);
		INSERT INTO detail_penerimaan (id_rec,id_item,jumlah,hargaSatuan,id_pemilik,barcode_barang) VALUES(_id_rec,_id_item,_jumlah,_harga,_id_pemilik,_barcode);
		SET@count1=(SELECT SUM(jumlah_item)  FROM produksi WHERE id_produksi=_id_tran);
		SET@count2=(SELECT SUM(jumlah) FROM gudang WHERE id_rec=_id_rec);
		IF (@count2>=@count1) THEN 
			UPDATE produksi SET `status`=6 WHERE id_produksi=_id_tran;
			UPDATE penerimaan_barang SET `status`=3 WHERE id_rec=_id_rec;
		END IF;
		
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_input_item`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_item` (`_id_item` VARCHAR(100), `_nama_item` VARCHAR(200), `_harga` INT(50), `_satuan` VARCHAR(50), `_deskripsi` TEXT, `_link_photo` VARCHAR(200), `id_pemilik` VARCHAR(200))  BEGIN
	SET@cek=(SELECT count(*) from item_master where id_item=_id_item);
	if (@cek) then
		select -1 as cek;
	else
	insert into item_master(id_item,nama_item,item_harga,satuan,deskripsi,link_photo,id_pemilik) values(_id_item,_nama_item,_harga,_satuan,_deskripsi,_link_photo,id_pemilik);
	select 1 as cek;
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_input_keluarGudang`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_keluarGudang` (`_id_issue` VARCHAR(100), `_id` VARCHAR(100), `_email` VARCHAR(100), `_id_customer` INT(11), `_total` BIGINT(20), `_tgl` DATETIME, `_kurir` VARCHAR(100), `_kode` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	set@idPet=(select id_petugas from petugas where email=_email);
	if _kode=1 then
		insert into pengeluaran_barang(id_issue,id_so,id_petugas,id_customer,total,kurir,tanggal,id_pemilik)
		values(_id_issue,_id,@idPet,_id_customer,_total,_kurir,_tgl,_id_pemilik);
	elseif _kode=2 then
		INSERT INTO pengeluaran_barang(id_issue,id_produksi,id_petugas,id_customer,total,kurir,tanggal,id_pemilik)
		VALUES(_id_issue,_id,@idPet,_id_customer,_total,_kurir,_tgl,_id_pemilik);
	ELSEIF _kode=3 THEN
		INSERT INTO pengeluaran_barang(id_issue,id_produkService,id_petugas,id_customer,total,kurir,tanggal,id_pemilik)
		VALUES(_id_issue,_id,@idPet,_id_customer,_total,_kurir,_tgl,_id_pemilik);
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_input_penerimaan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_penerimaan` (`_id_rec` VARCHAR(100), `_id_po` VARCHAR(200), `_idPenerima` VARCHAR(200), `_tgl` DATE, `_idSuplier` INT(11), `_totalHarga` BIGINT(20), `_kurir` VARCHAR(100), `_kode` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	if(_kode=1) then
		INSERT INTO penerimaan_barang (id_rec,id_po,id_petugas,tanggal_receive,id_suplier,totalHarga,kurir,id_pemilik) 
		VALUES(_id_rec,_id_po,_idPenerima,_tgl,_idSuplier,_totalHarga,_kurir,_id_pemilik);
	elseif(_kode=2) then
		INSERT INTO penerimaan_barang (id_rec,id_produksi,id_petugas,tanggal_receive,id_suplier,totalHarga,kurir) 
		VALUES(_id_rec,_id_po,_idPenerima,_tgl,_idSuplier,_totalHarga,_kurir);
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_input_penjualan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_penjualan` (`_id_so` VARCHAR(100), `_email` VARCHAR(100), `_id_customer` VARCHAR(100), `_total` BIGINT(20), `_tgl` DATETIME, `_kurir` VARCHAR(100), `_ongkir` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SET@id_petugas=(SELECT id_petugas FROM petugas WHERE email=_email);
	INSERT INTO penjualan (id_so,id_petugas,id_customer,tanggal,total,kurir,ongkir,id_pemilik) 
	VALUES(_id_so,@id_petugas,_id_customer,_tgl,_total,_kurir,_ongkir,_id_pemilik);
    END$$

DROP PROCEDURE IF EXISTS `sp_input_petugas`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_petugas` (`_ktp` VARCHAR(100), `_nama` VARCHAR(200), `_email` VARCHAR(200), `_passwd` VARCHAR(200))  BEGIN
	set@cek=(select count(*) from petugas where id_petugas=_ktp);
	SET@cek2=(SELECT Count(*) email FROM petugas WHERE email=_email);
	if (@cek or @cek2) then
		select -1 as cek;
	else
		INSERT INTO `petugas` (id_petugas,nama,email,tgl,`password`,privilege,jabatan) 
		VALUES(_ktp,_nama,_email,now(),md5(_passwd),1,'user');
		select 0 as cek;
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_input_purchasing`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_purchasing` (`_id_po` VARCHAR(100), `_id_suplier` INT(11), `_email` VARCHAR(200), `_tanggal_po` DATE, `_totalHarga` BIGINT(20), `_id_pemilik` VARCHAR(100), `_ongkir` INT(11))  BEGIN
	set@idPetugas=(select id_petugas from petugas where email=_email);
	insert into purchasing (id_po,id_suplier,id_petugas,tanggal_po,totalHarga,id_pemilik,ongkir) 
	values(_id_po,_id_suplier,@idPetugas,_tanggal_po,_totalHarga,_id_pemilik,_ongkir);
    END$$

DROP PROCEDURE IF EXISTS `sp_input_satuan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_satuan` (`_nama_satuan` VARCHAR(50), `_kelompok` VARCHAR(50), `_deskripsi` VARCHAR(100), `_id_pemilik` VARCHAR(100))  BEGIN
	insert into satuan (nama_satuan,kelompok_satuan,deskripsi_satuan,id_pemilik) values(_nama_satuan,_kelompok,_deskripsi,_id_pemilik);
	select 1 as cek;
    END$$

DROP PROCEDURE IF EXISTS `sp_input_suplier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_suplier` (`_nama` VARCHAR(200), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_deskripsi` TEXT, `_id_pemilik` VARCHAR(100))  BEGIN
	set@cek=(select id_suplier from suplier where lower(nama_suplier)=lower(_nama));
	if(@cek) then
		select -1 as cek;
	else
		INSERT INTO `suplier` (nama_suplier,alamat,hp,email,deskripsi,tgl,id_pemilik) VALUES(_nama,_alamat,_hp,_email,_deskripsi,now(),_id_pemilik);
		select 1 as cek;
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_laporan_defect`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_defect` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_def,id_rec,id_issue,defect.`id_item`,nama_item,nama_satuan,nama_tipe_item,jumlah,hargaSatuan,tanggal,keterangan FROM defect 
	INNER JOIN item_master ON item_master.`id_item`=defect.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE ((`status`=1 AND defect.`tanggal`>=now() AND defect.`tanggal`<=now())
	 OR (`status`=2 AND defect.`tanggal`>=now() AND defect.`tanggal`<=now())) and defect.`id_pemilik`=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_laporan_pembelian`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_pembelian` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penerimaan_barang.`id_rec` ,tanggal_receive,nama_item,sat.nama_satuan,jumlah,petugas.`nama` AS nama_petugas,kurir,nama_suplier
	FROM detail_penerimaan 
	INNER JOIN penerimaan_barang ON detail_penerimaan.`id_rec`=penerimaan_barang.`id_rec`
	INNER JOIN item_master ON detail_penerimaan.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan` 
	INNER JOIN petugas ON penerimaan_barang.`id_petugas`=petugas.`id_petugas`
	INNER JOIN suplier ON penerimaan_barang.`id_suplier`=suplier.`id_suplier`
	WHERE penerimaan_barang.`status`=3 and penerimaan_barang.`tanggal_receive`>=now() AND penerimaan_barang.`tanggal_receive`<=now() and penerimaan_barang.`id_pemilik`=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_laporan_penjualan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_penjualan` (`_id_pemilik` VARCHAR(100))  BEGIN
	select penjualan.`id_so`,tanggal,nama_item,sat.nama_satuan,jumlah,petugas.`nama` as nama_petugas,kurir from detail_penjualan 
	inner join penjualan on detail_penjualan.`id_so`=penjualan.`id_so`
	inner join item_master on detail_penjualan.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan` 
	inner join petugas on penjualan.`id_petugas`=petugas.`id_petugas`
	WHERE penjualan.`status`=3 and penjualan.`tanggal`>=now() AND penjualan.`tanggal`<=now() and penjualan.`id_pemilik`=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_listProduk_perSuplier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listProduk_perSuplier` (`_id_suplier` INT(11))  BEGIN
	SELECT detail_suplier.`harga`,nama_item,detail_suplier.`id_item`,sat.nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan` 
	WHERE detail_suplier.`id_suplier`=_id_suplier GROUP BY detail_suplier.`id_item`,hargaSatuan ORDER BY nama_item ASC;
    END$$

DROP PROCEDURE IF EXISTS `sp_listProduk_perSuplier_pagination`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listProduk_perSuplier_pagination` (`_id_suplier` INT(11), `_offset` INT(11), `_rows` INT(11))  BEGIN
	SELECT detail_suplier.`harga`,nama_item,detail_suplier.`id_item`,sat.nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan` 
	WHERE detail_suplier.`id_suplier`=_id_suplier GROUP BY detail_suplier.`id_item`,hargaSatuan ORDER BY nama_item ASC
	LIMIT _offset, _rows;
    END$$

DROP PROCEDURE IF EXISTS `sp_list_customer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_customer` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT institusi.`nama_institusi`,id_customer,id_institusi,nama,jenkel,alamat,hp,customer.email,tgl,photo_link,jabatan
	 FROM customer INNER JOIN institusi ON customer.`id_institut`=institusi.`id_institusi` 
	 where customer.`id_pemilik`=_id_pemilik order by nama asc;
    END$$

DROP PROCEDURE IF EXISTS `sp_list_item`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_item` (IN `_id_pemilik` VARCHAR(100), `game` INT(10))  BEGIN
	if(game > 0) then
		select item_master.id_item ,nama_item, item_harga , link_photo , sat.nama_satuan as satuan_nama ,item_master.`satuan` as satuan ,item_master.deskripsi,sub_kategori.`subKategori_name` as tipe_name,item_master.`tipe`,item_master.`item_harga` as harga from item_master
		inner join satuan sat on sat.id_satuan=item_master.`satuan`
		inner join sub_kategori on sub_kategori.`subKategori_kode`=item_master.`tipe` where item_master.`id_pemilik`= 0 AND sub_kategori.`id_pemilik` = 0 order by nama_item asc;
	else
		SELECT item_master.id_item ,nama_item, item_harga , link_photo , sat.nama_satuan AS satuan_nama , item_master.`satuan` AS satuan,item_master.deskripsi,sub_kategori.`subKategori_name` AS tipe_name,item_master.`tipe`,item_master.`item_harga` AS harga FROM item_master
		INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
		INNER JOIN sub_kategori ON sub_kategori.`subKategori_kode`=item_master.`tipe` WHERE item_master.`id_pemilik`= _id_pemilik AND sub_kategori.`id_pemilik` = _id_pemilik  ORDER BY nama_item ASC;
		
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_list_penerimaan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_penerimaan` ()  BEGIN
	SELECT purchasing.`id_petugas`,id_po,petugas.`nama` AS nama_petugas,nama_suplier,tanggal_po,totalHarga,`status`,1 as kode FROM purchasing 
	INNER JOIN petugas ON purchasing.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON purchasing.`id_suplier`=suplier.`id_suplier` where `status`=1 or `status`=2 or `status`=3
union
	SELECT produksi.`id_petugas`,id_produksi,petugas.`nama` AS nama_petugas,"PT.Miconos" AS nama_suplier,tanggal_po,totalHarga,`status`,2 AS kode 
	FROM produksi 
	INNER JOIN petugas ON produksi.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON suplier.`id_suplier`=3 WHERE `status`=5 or `status`=6;
    END$$

DROP PROCEDURE IF EXISTS `sp_list_pengeluaran`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_pengeluaran` ()  BEGIN
	select penjualan.`id_so`,petugas.nama as nama_petugas,customer.`nama` as nama_customer,tanggal,total,kurir,`status`, 1 as kode
	from penjualan 
	inner join petugas on petugas.`id_petugas`=penjualan.`id_petugas`
	inner join customer on customer.`id_customer`=penjualan.`id_customer`
	where penjualan.`status`=1 or penjualan.`status`=2 or `status`=3
	union
	SELECT produksi.`id_produksi`,petugas.nama AS nama_petugas,"Produksi" as nama_customer,tanggal_po,totalHarga,NULL AS kurir,`status`,2 as kode
	FROM produksi
	INNER JOIN petugas ON petugas.`id_petugas`=produksi.`id_petugas`
	WHERE produksi.`status`=1 OR produksi.`status`=2 or `status`=3
	union 
	SELECT produkservice.`id_produkService`,petugas.nama AS nama_petugas,customer.`nama` as nama_customer,tanggal,total,"Service" AS kurir,produkservice.`status`,3 AS kode
	FROM produkservice
	INNER JOIN service ON service.`id_service`=produkservice.`id_service`
	INNER JOIN customer ON customer.`id_customer`=service.`id_customer`
	INNER JOIN petugas ON petugas.`id_petugas`=produkservice.`teknisi`
	WHERE produkservice.`status`=1 or produkservice.`status`=2 or produkservice.`status`=3 ;
    END$$

DROP PROCEDURE IF EXISTS `sp_list_penjualan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_penjualan` ()  BEGIN
	
	select id_so,petugas.`nama` as nama_petugas,customer.`nama` as nama_customer,tanggal,total,kurir,`status` 
	from penjualan 
	inner join customer on penjualan.`id_customer`=customer.`id_customer` 
	inner join petugas on penjualan.`id_petugas`=petugas.`id_petugas` order by tanggal desc;
	
    END$$

DROP PROCEDURE IF EXISTS `sp_list_petugas`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_petugas` ()  BEGIN
	SELECT * FROM petugas;
    END$$

DROP PROCEDURE IF EXISTS `sp_list_produk`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_produk` (`_id_pemilik` VARCHAR(100))  BEGIN
	
	SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	where item_master.`id_pemilik`=_id_pemilik
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC;
 
    END$$

DROP PROCEDURE IF EXISTS `sp_list_purchasing`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_purchasing` (`nama` VARCHAR(200))  BEGIN
	select purchasing.`id_petugas`,id_po,petugas.`nama` as nama_petugas,nama_suplier,tanggal_po,totalHarga,`status` from purchasing 
	inner join petugas on purchasing.`id_petugas`=petugas.`id_petugas` 
	inner join suplier on purchasing.`id_suplier`=suplier.`id_suplier` where id_petugas=nama order by tanggal_po desc;
    END$$

DROP PROCEDURE IF EXISTS `sp_list_satuan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_satuan` (`_id_pemilik` VARCHAR(100))  BEGIN
		select * from satuan;
    END$$

DROP PROCEDURE IF EXISTS `sp_list_service`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_service` ()  BEGIN
	select id_service,`subject`,keluhan,tgl_open,`status`,customer.`nama` as nama_customer,customer.`id_customer`,petugas.`nama` as nama_petugas,service.`id_petugas`,service.`id_customer` 
	from service inner join customer on service.`id_customer`=customer.`id_customer`
	inner join petugas on service.`id_petugas`=petugas.`id_petugas`;
    END$$

DROP PROCEDURE IF EXISTS `sp_list_suplier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_suplier` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT * FROM suplier where id_pemilik=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_list_tipeItem2`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_tipeItem2` (`_idPemilik` VARCHAR(200))  BEGIN
	select * from item_SubGroup where id_pemilik = _idPemilik ;
    END$$

DROP PROCEDURE IF EXISTS `sp_list_transaksiPerCustomer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_transaksiPerCustomer` (`_id_customer` VARCHAR(100))  BEGIN
	select penjualan.`id_so`,tanggal,nama_item,jumlah,harga
	from detail_penjualan 
	inner join item_master on detail_penjualan.`id_item`=item_master.`id_item`
	INNER JOIN penjualan on detail_penjualan.`id_so`=penjualan.`id_so`
	  where id_customer=_id_customer;
    END$$

DROP PROCEDURE IF EXISTS `sp_login`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_login` (IN `_email` VARCHAR(200), IN `_passwd` VARCHAR(50))  BEGIN
	set@cek=(select 1 from petugas where email=_email and `password`=md5(_passwd));
	if @cek=1 then
		select 1 as A;
	else select 0 as A;
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_pageList_customer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_customer` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	
	SELECT institusi.`nama_institusi`,id_customer,id_institusi,nama,jenkel,alamat,hp,customer.email,tgl,photo_link,jabatan
	 FROM customer INNER JOIN institusi ON customer.`id_institut`=institusi.`id_institusi`
	 where customer.`id_pemilik`=_id_pemilik
	 ORDER BY nama ASC LIMIT _limit offset _start;
    END$$

DROP PROCEDURE IF EXISTS `sp_pageList_pembelian`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_pembelian` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT purchasing.`id_petugas`,id_po,petugas.`nama` AS nama_petugas,nama_suplier,tanggal_po,totalHarga,`status`,ongkir FROM purchasing 
	INNER JOIN petugas ON purchasing.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON purchasing.`id_suplier`=suplier.`id_suplier`
	where purchasing.`id_pemilik`=_id_pemilik
	ORDER BY tanggal_po DESC LIMIT _limit OFFSET _start;
    END$$

DROP PROCEDURE IF EXISTS `sp_pageList_penerimaan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_penerimaan` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN 
	SELECT purchasing.`id_petugas`,id_po,petugas.`nama` AS nama_petugas,nama_suplier,tanggal_po,totalHarga,`status`,1 AS kode,ongkir FROM purchasing 
	INNER JOIN petugas ON purchasing.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON purchasing.`id_suplier`=suplier.`id_suplier` WHERE (`status`=1 OR `status`=2 OR `status`=3) and purchasing.id_pemilik=_id_pemilik;
    END$$

DROP PROCEDURE IF EXISTS `sp_pageList_pengeluaran`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_pengeluaran` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penjualan.`id_so`,petugas.nama AS nama_petugas,customer.`nama` AS nama_customer,tanggal,total,kurir,`status`, 1 AS kode, ongkir
	FROM penjualan 
	INNER JOIN petugas ON petugas.`id_petugas`=penjualan.`id_petugas`
	INNER JOIN customer ON customer.`id_customer`=penjualan.`id_customer`
	WHERE penjualan.`status` > 0 and penjualan.`id_pemilik`=_id_pemilik
	order by penjualan.`tanggal` desc LIMIT _limit OFFSET _start;
    END$$

DROP PROCEDURE IF EXISTS `sp_pageList_penjualan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_penjualan` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_so,petugas.`nama` AS nama_petugas,customer.`nama` AS nama_customer,tanggal,total,kurir,`status` 
	FROM penjualan 
	INNER JOIN customer ON penjualan.`id_customer`=customer.`id_customer` 
	INNER JOIN petugas ON penjualan.`id_petugas`=petugas.`id_petugas`
	where penjualan.`id_pemilik`=_id_pemilik
	ORDER BY tanggal DESC LIMIT _limit OFFSET _start;
    END$$

DROP PROCEDURE IF EXISTS `sp_pageList_penjualan2`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_penjualan2` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	
	SELECT id_so,petugas.`nama` AS nama_petugas,tanggal,total,kurir,`status` 
	FROM penjualan 
	INNER JOIN petugas ON penjualan.`id_petugas`=petugas.`id_petugas`
	WHERE penjualan.`id_pemilik`=_id_pemilik
	ORDER BY tanggal DESC LIMIT _limit OFFSET _start;
    END$$

DROP PROCEDURE IF EXISTS `sp_pageList_produk`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_produk` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	where penerimaan_barang.`id_pemilik`= `_id_pemilik`
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC LIMIT _limit OFFSET _start;
    END$$

DROP PROCEDURE IF EXISTS `sp_pageList_supplier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_supplier` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	select * from suplier where id_pemilik=_id_pemilik order by nama_suplier asc ;
    END$$

DROP PROCEDURE IF EXISTS `sp_pembelian`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pembelian` (`_id_transaksi` VARCHAR(100), `_email` VARCHAR(100), `_id_customer` INT(11), `_total` BIGINT(20), `_tgl` DATETIME)  BEGIN
	set@id_petugas=(select id_petugas from petugas where email=_email);
	insert into pembelian (id_transaksi,id_petugas,id_customer,tanggal,total) values(_id_transaksi,@id_petugas,_id_customer,_tgl,_total);
    END$$

DROP PROCEDURE IF EXISTS `sp_pengurangan_stok`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pengurangan_stok` (`_id_rec` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE)  BEGIN
	UPDATE gudang SET jumlah=(jumlah-_jumlah) WHERE id_rec=_id_rec AND id_item=_id_item;
	set@cek=(select jumlah from gudang WHERE id_rec=_id_rec AND id_item=_id_item);
	set@set=(SELECT `status` FROM gudang WHERE id_rec=_id_rec AND id_item=_id_item);
	if(@cek<1 and @set=0)then
		delete from gudang WHERE id_rec=_id_rec AND id_item=_id_item;
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_pengurangan_stok2`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pengurangan_stok2` (`_id_rec` VARCHAR(100), `_barcode_barang` VARCHAR(100), `_jumlah` DOUBLE)  BEGIN
	UPDATE gudang SET jumlah=(jumlah-_jumlah) WHERE id_rec=_id_rec and barcode_barang = _barcode_barang ;
    END$$

DROP PROCEDURE IF EXISTS `sp_rincianListProduk`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianListProduk` (`_id_item` VARCHAR(100))  BEGIN
	SELECT nama_suplier,detail_suplier.`id_suplier`,nama_item,detail_suplier.`id_item`,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	inner join suplier on suplier.`id_suplier`=detail_suplier.`id_suplier`
	WHERE detail_suplier.`id_item`=_id_item GROUP BY detail_suplier.`id_item`,hargaSatuan ORDER BY nama_item ASC;
    END$$

DROP PROCEDURE IF EXISTS `sp_rincianPembelian`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianPembelian` (IN `_id_pur` VARCHAR(100))  BEGIN
	select DISTINCT  detail_purchasing.`id_item`,nama_item,nama_satuan,sub_kategori.`subKategori_name` as  nama_tipe_item,detail_purchasing.jumlah,tab.`jumlah`as jml_keluar,detail_purchasing.hargaSatuan 
	from detail_purchasing inner join item_master on item_master.`id_item`=detail_purchasing.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	INNER JOIN sub_kategori ON sub_kategori.`subKategori_kode`=item_master.`tipe` AND sub_kategori.`id_pemilik` = item_master.`id_pemilik`
		left join (select penerimaan_barang.`id_po`,jumlah,id_item from detail_penerimaan inner join penerimaan_barang on penerimaan_barang.`id_rec`=detail_penerimaan.`id_rec`
	where penerimaan_barang.`id_po`=_id_pur) as tab on tab.id_po=detail_purchasing.`id_purchasing` AND tab.id_item=detail_purchasing.`id_item`
	where detail_purchasing.`id_purchasing`=_id_pur ;
    END$$

DROP PROCEDURE IF EXISTS `sp_rincianPenjualan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianPenjualan` (IN `_id_tran` VARCHAR(100))  BEGIN
	SELECT detail_penjualan.`id_item`,nama_item,nama_satuan,sub_kategori.`subKategori_name` AS nama_tipe_item,SUM(detail_penjualan.jumlah) AS jumlah,SUM(tab.`jumlah`) AS keluar,harga FROM detail_penjualan
	INNER JOIN item_master ON item_master.`id_item`=detail_penjualan.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	#inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	INNER JOIN sub_kategori ON sub_kategori.`subKategori_kode`=item_master.`tipe` AND sub_kategori.`id_pemilik` = item_master.`id_pemilik`
	LEFT JOIN (SELECT pengeluaran_barang.`id_so`,jumlah,id_item,barcode_barang FROM detail_pengeluaran 
	INNER JOIN pengeluaran_barang ON pengeluaran_barang.`id_issue`=detail_pengeluaran.`id_issue`
	WHERE pengeluaran_barang.`id_so`=_id_tran) AS tab ON tab.id_so=detail_penjualan.`id_so` AND tab.barcode_barang=detail_penjualan.`barcode_barang`
	WHERE detail_penjualan.`id_so`=_id_tran
	GROUP BY detail_penjualan.`id_item`;
    END$$

DROP PROCEDURE IF EXISTS `sp_rincian_viewSO`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincian_viewSO` (`_id_so` VARCHAR(100))  BEGIN
	select customer.`id_customer`,customer.`nama`as nama_customer,petugas.`nama`as nama_petugas,penjualan.`tanggal`,kurir,penjualan.`total`,penjualan.`ongkir` from penjualan
	inner join petugas on petugas.`id_petugas`=penjualan.`id_petugas`
	inner join customer on customer.`id_customer`=penjualan.`id_customer`
	where penjualan.`id_so`=_id_so;
    END$$

DROP PROCEDURE IF EXISTS `sp_stok_gudang`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_stok_gudang` (`_id_item` VARCHAR(100))  BEGIN
	SELECT "Masuk" AS asal,SUM(detail_penerimaan.`jumlah`)AS jumlah,nama_item FROM detail_penerimaan 
	INNER JOIN penerimaan_barang ON penerimaan_barang.`id_rec`=detail_penerimaan.`id_rec`
	inner join item_master on item_master.`id_item`=detail_penerimaan.`id_item`
	WHERE (penerimaan_barang.`status`=3 AND detail_penerimaan.`id_item`=_id_item) 
	or (penerimaan_barang.`status`=2 AND detail_penerimaan.`id_item`=_id_item)
	UNION 
	SELECT "Gudang" AS asal, SUM(gudang.`jumlah`) AS jumlah,nama_item FROM gudang
	inner join item_master on item_master.`id_item`=gudang.`id_item`
	WHERE gudang.`id_item`=_id_item
	UNION
	SELECT "Keluar" AS asal,SUM(detail_pengeluaran.`jumlah`) AS jumlah,nama_item FROM detail_pengeluaran 
	INNER JOIN pengeluaran_barang ON pengeluaran_barang.`id_issue`=detail_pengeluaran.`id_issue`
	inner join item_master on item_master.`id_item`=detail_pengeluaran.`id_item`
	WHERE pengeluaran_barang.`status`=3 AND detail_pengeluaran.`id_item`=_id_item;
    END$$

DROP PROCEDURE IF EXISTS `sp_ubahPassword`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_ubahPassword` (`_id_petugas` VARCHAR(100), `_pwdLama` VARCHAR(200), `_pwdBaru` VARCHAR(200))  BEGIN
	set@cek=(select id_petugas from petugas where id_petugas=_id_petugas and `password`=md5(_pwdLama));
	if (@cek) then
		update petugas set `password`=md5(_pwdBaru) where id_petugas=_id_petugas;
		select 1 as cek;
	else 
		select -1 as cek;
	end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_updateCustomer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateCustomer` (`_institusi` VARCHAR(100), `_ktp` VARCHAR(100), `_nama` VARCHAR(200), `_jenkel` CHAR(3), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_jabatan` VARCHAR(200))  BEGIN
    
	 
	UPDATE customer SET id_institut=_institusi, nama=_nama, jenkel=_jenkel,alamat=_alamat, hp=_hp, email=_email,jabatan=_jabatan WHERE id_customer=_ktp;
	SELECT 1 AS cek;
	 
    END$$

DROP PROCEDURE IF EXISTS `sp_updateItem`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateItem` (`_id_item` VARCHAR(100), `_nama_item` VARCHAR(200), `_tipe` VARCHAR(50), `_satuan` VARCHAR(50), `_deskripsi` TEXT)  BEGIN
	update item_master set nama_item=_nama_item,tipe=_tipe,satuan=_satuan,deskripsi=_deskripsi where id_item=_id_item;
	select 1 as cek;
    END$$

DROP PROCEDURE IF EXISTS `sp_updatePetugas`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updatePetugas` (`_ktp` VARCHAR(100), `_nama` VARCHAR(200), `_jenkel` CHAR(3), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_jabatan` VARCHAR(200), `_passwd` VARCHAR(200))  BEGIN
    set@passwd=(select `password` from petugas where id_petugas=_ktp);
	if@passwd=(md5(_passwd)) then 
	 
	UPDATE petugas SET nama=_nama, jenkel=_jenkel,alamat=_alamat, hp=_hp, email=_email,jabatan=_jabatan WHERE id_petugas=_ktp;
	select 1 as cek;
	 else select -1 as cek; end if;
    END$$

DROP PROCEDURE IF EXISTS `sp_updateSatuan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateSatuan` (`_id` INT(11), `_nama_satuan` VARCHAR(50), `_kelompok` VARCHAR(50), `_deskripsi` VARCHAR(100))  BEGIN
	update satuan set nama_satuan=_nama_satuan,kelompok_satuan=_kelompok,deskripsi_satuan=_deskripsi where id_satuan=_id;
	select 1 as cek;
    END$$

DROP PROCEDURE IF EXISTS `sp_updateStatus_pembelian`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateStatus_pembelian` (`_id_pur` VARCHAR(100), `_status` INT(11))  BEGIN
	update purchasing set `status`=_status where id_po=_id_pur;
    END$$

DROP PROCEDURE IF EXISTS `sp_updateStatus_penjualan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateStatus_penjualan` (`_id_so` VARCHAR(100), `_status` INT(11))  BEGIN
	update penjualan set `status`=_status where id_so=_id_so;
    END$$

DROP PROCEDURE IF EXISTS `sp_updateSuplier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateSuplier` (`_id_suplier` INT(11), `_nama` VARCHAR(200), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_deskripsi` TEXT)  BEGIN
	update suplier set nama_suplier=_nama,alamat=_alamat,hp=_hp,email=_email,deskripsi=_deskripsi where id_suplier=_id_suplier;
	select 1 as cek;
    END$$

DROP PROCEDURE IF EXISTS `sp_viewPO`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewPO` (`_id_po` VARCHAR(200))  BEGIN
	select id_purchasing,nama_item,item_master.`id_item`,jumlah,hargaSatuan,sub_kategori.`subKategori_name` as nama_tipe_item,nama_satuan,id_suplier,purchasing.id_petugas,totalHarga,1 as kode
	FROM detail_purchasing INNER JOIN purchasing ON purchasing.`id_po`=detail_purchasing.`id_purchasing`
	INNER JOIN item_master ON detail_purchasing.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN sub_kategori ON sub_kategori.`subKategori_kode`=item_master.`tipe` AND sub_kategori.`id_pemilik` = item_master.`id_pemilik`
	where id_purchasing=_id_po;
    END$$

DROP PROCEDURE IF EXISTS `sp_viewSO`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewSO` (`_id_so` VARCHAR(100))  BEGIN
	select detail_suplier.`harga` as harga_beli, nama_suplier,penjualan.`id_so`,nama_item,item_master.`id_item`,jumlah,nama_satuan,sub_kategori.`subKategori_name` as nama_tipe_item,detail_penjualan.harga,jumlah,detail_penjualan.`id_suplier`,1 as kode, barcode_barang
	from detail_penjualan 
	inner join penjualan on penjualan.`id_so`=detail_penjualan.`id_so`
	inner join suplier on suplier.`id_suplier`=detail_penjualan.`id_suplier`
	inner join item_master on item_master.`id_item`=detail_penjualan.`id_item`
	INNER JOIN detail_suplier ON suplier.`id_suplier`=detail_suplier.`id_suplier` and detail_suplier.`id_item`=item_master.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	INNER JOIN sub_kategori ON sub_kategori.`subKategori_kode`=item_master.`tipe` AND sub_kategori.`id_pemilik` = item_master.`id_pemilik`
		where penjualan.`id_so`=_id_so;
    END$$

DROP PROCEDURE IF EXISTS `sp_view_perCustomer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perCustomer` (`_id_customer` VARCHAR(100))  BEGIN
	SELECT institusi.`nama_institusi`,id_customer,id_institusi,nama,jenkel,alamat,hp,customer.email,tgl,photo_link,jabatan
	 FROM customer inner join institusi on customer.`id_institut`=institusi.`id_institusi` WHERE id_customer=_id_customer;
    END$$

DROP PROCEDURE IF EXISTS `sp_view_perItem`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perItem` (`_id_item` VARCHAR(100), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT item_master.`id_item`,nama_item,item_master.deskripsi,link_photo,
	sat.nama_satuan, sub_kategori.`subKategori_name` as nama_tipe_item,item_master.`satuan`,item_master.`tipe`,item_master.`item_harga` as harga
	FROM item_master INNER JOIN satuan ON satuan.`id_satuan`=item_master.`satuan`
	inner join satuan sat on sat.`id_satuan`=item_master.`satuan`
	inner join sub_kategori on sub_kategori.`subKategori_kode`=item_master.`tipe` and sub_kategori.`id_pemilik` =_id_pemilik
		WHERE id_item=_id_item;
    END$$

DROP PROCEDURE IF EXISTS `sp_view_perPetugas`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perPetugas` (`_id_pet` VARCHAR(200))  BEGIN
	select * from petugas where id_petugas=_id_pet;
    END$$

DROP PROCEDURE IF EXISTS `sp_view_perProduk`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perProduk` (`_id_item` VARCHAR(100))  BEGIN
	select * from item_master inner join satuan on satuan.`id_satuan`=item_master.`satuan` where id_item=_id_item;
    END$$

DROP PROCEDURE IF EXISTS `sp_view_perSuplier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perSuplier` (`_id_suplier` INT(11))  BEGIN
	SELECT * FROM suplier WHERE id_suplier=_id_suplier;
    END$$

--
-- Functions
--
DROP FUNCTION IF EXISTS `fc_tambah`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `fc_tambah` (`_id_po` INT) RETURNS INT(11) BEGIN
	declare hasil int;
	set hasil = ( SELECT SUM(jumlah) FROM gudang WHERE id_purchasing=_id_po);
	return hasil;
    END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
CREATE TABLE `asset` (
  `asset_id` int(11) NOT NULL,
  `asset_name` varchar(100) DEFAULT NULL,
  `asset_duration` int(11) DEFAULT NULL,
  `asset_duration_type` char(2) DEFAULT NULL,
  `asset_capacity` int(11) DEFAULT '0',
  `id_retail` varchar(200) DEFAULT NULL,
  `asset_value` int(100) DEFAULT NULL,
  `is_delete` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `core_company`
--

DROP TABLE IF EXISTS `core_company`;
CREATE TABLE `core_company` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `company_code` text,
  `company_name` text,
  `company_address` text,
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creation_user` int(20) DEFAULT NULL,
  `last_mod_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_mod_user` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `core_icon`
--

DROP TABLE IF EXISTS `core_icon`;
CREATE TABLE `core_icon` (
  `id` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `clsname` varchar(50) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `creation_user` int(20) DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_mod_user` int(20) DEFAULT NULL,
  `last_mod_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `core_menu`
--

DROP TABLE IF EXISTS `core_menu`;
CREATE TABLE `core_menu` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` text,
  `controller` varchar(100) DEFAULT NULL,
  `icon_name` varchar(100) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `creation_user` int(20) DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_mod_user` int(20) DEFAULT NULL,
  `last_mod_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletable` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `core_role`
--

DROP TABLE IF EXISTS `core_role`;
CREATE TABLE `core_role` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `access` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `creation_user` int(20) DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_mod_user` int(20) DEFAULT NULL,
  `last_mod_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id_customer` varchar(100) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `jenkel` varchar(3) NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `hp` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `tgl` date NOT NULL,
  `photo_link` varchar(500) NOT NULL DEFAULT 'default/users.png',
  `jabatan` varchar(100) NOT NULL,
  `id_pemilik` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_penerimaan`
--

DROP TABLE IF EXISTS `detail_penerimaan`;
CREATE TABLE `detail_penerimaan` (
  `id_rec` varchar(100) DEFAULT NULL,
  `id_item` varchar(100) NOT NULL,
  `jumlah` double NOT NULL,
  `hargaSatuan` int(11) NOT NULL,
  `status` int(11) DEFAULT '0',
  `id_pemilik` varchar(200) DEFAULT NULL,
  `barcode_barang` varchar(100) DEFAULT NULL,
  `id_supplier` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_pengeluaran`
--

DROP TABLE IF EXISTS `detail_pengeluaran`;
CREATE TABLE `detail_pengeluaran` (
  `id_issue` varchar(100) NOT NULL,
  `id_item` varchar(100) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` int(11) NOT NULL,
  `id_suplier` int(11) DEFAULT NULL,
  `id_rec` varchar(100) DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL,
  `barcode_barang` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

DROP TABLE IF EXISTS `detail_penjualan`;
CREATE TABLE `detail_penjualan` (
  `id_so` varchar(100) NOT NULL,
  `id_item` varchar(100) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` int(11) NOT NULL,
  `id_suplier` int(11) DEFAULT NULL,
  `jumlah_keluar` double DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL,
  `barcode_barang` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_purchasing`
--

DROP TABLE IF EXISTS `detail_purchasing`;
CREATE TABLE `detail_purchasing` (
  `id_purchasing` varchar(100) NOT NULL,
  `id_item` varchar(100) NOT NULL,
  `jumlah` double NOT NULL,
  `hargaSatuan` int(11) NOT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL,
  `barcode_barang` varchar(100) DEFAULT NULL,
  `id_supplier` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_suplier`
--

DROP TABLE IF EXISTS `detail_suplier`;
CREATE TABLE `detail_suplier` (
  `id_suplier` int(11) NOT NULL,
  `id_item` varchar(100) NOT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL,
  `harga` int(11) NOT NULL DEFAULT '0',
  `supplier_stock` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
CREATE TABLE `game` (
  `game_id` int(11) NOT NULL,
  `game_name` varchar(100) DEFAULT NULL,
  `game_date` datetime DEFAULT NULL,
  `game_creator` varchar(100) DEFAULT NULL,
  `game_round` int(11) DEFAULT '1',
  `game_status` int(11) DEFAULT '0',
  `game_start_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `game_detail`
--

DROP TABLE IF EXISTS `game_detail`;
CREATE TABLE `game_detail` (
  `game_id` int(11) DEFAULT NULL,
  `game_player` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gl_account`
--

DROP TABLE IF EXISTS `gl_account`;
CREATE TABLE `gl_account` (
  `id` int(11) NOT NULL,
  `coa_id` int(11) NOT NULL,
  `acc_code` int(20) NOT NULL,
  `acc_name` varchar(100) NOT NULL,
  `deletable` tinyint(1) NOT NULL DEFAULT '0',
  `other` tinyint(1) DEFAULT '0',
  `uid` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gl_group`
--

DROP TABLE IF EXISTS `gl_group`;
CREATE TABLE `gl_group` (
  `id` int(11) NOT NULL,
  `coa_number` varchar(10) NOT NULL,
  `coa_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gl_has`
--

DROP TABLE IF EXISTS `gl_has`;
CREATE TABLE `gl_has` (
  `id` int(11) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `child` int(11) NOT NULL,
  `uid` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gl_journal_h`
--

DROP TABLE IF EXISTS `gl_journal_h`;
CREATE TABLE `gl_journal_h` (
  `id` int(11) NOT NULL,
  `period_id` date NOT NULL,
  `journal_name` varchar(80) DEFAULT NULL,
  `notes` longtext,
  `Post` tinyint(1) DEFAULT '0',
  `uid` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gl_journal_l`
--

DROP TABLE IF EXISTS `gl_journal_l`;
CREATE TABLE `gl_journal_l` (
  `id` int(20) NOT NULL,
  `journal_id` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `line_debit` bigint(20) DEFAULT '0',
  `line_credit` bigint(20) DEFAULT '0',
  `uid` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gl_schema_h`
--

DROP TABLE IF EXISTS `gl_schema_h`;
CREATE TABLE `gl_schema_h` (
  `id` int(11) NOT NULL,
  `journal_name` varchar(80) NOT NULL,
  `uid` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gl_schema_l`
--

DROP TABLE IF EXISTS `gl_schema_l`;
CREATE TABLE `gl_schema_l` (
  `id` int(11) NOT NULL,
  `journal_id` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `line_debit` varchar(100) NOT NULL,
  `line_credit` varchar(100) NOT NULL,
  `uid` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gl_variable`
--

DROP TABLE IF EXISTS `gl_variable`;
CREATE TABLE `gl_variable` (
  `id` int(11) NOT NULL,
  `v_code` varchar(100) NOT NULL,
  `v_desc` varchar(100) NOT NULL,
  `uid` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

DROP TABLE IF EXISTS `gudang`;
CREATE TABLE `gudang` (
  `id_rec` varchar(100) DEFAULT NULL,
  `id_item` varchar(100) NOT NULL,
  `jumlah` double NOT NULL,
  `hargaSatuan` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `id_produksi` varchar(100) DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL,
  `barcode_barang` varchar(100) DEFAULT NULL,
  `id_supplier` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_master`
--

DROP TABLE IF EXISTS `item_master`;
CREATE TABLE `item_master` (
  `id_item` varchar(100) NOT NULL,
  `nama_item` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `tipe` int(11) NOT NULL DEFAULT '3',
  `satuan` int(11) NOT NULL,
  `link_photo` varchar(200) DEFAULT NULL,
  `item_harga` int(50) DEFAULT NULL,
  `item_marketing` int(50) DEFAULT '0',
  `id_pemilik` varchar(200) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_stock`
--

DROP TABLE IF EXISTS `item_stock`;
CREATE TABLE `item_stock` (
  `stock_id` int(11) NOT NULL,
  `id_barang` varchar(100) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `id_retail` int(11) DEFAULT NULL,
  `stock` int(20) DEFAULT NULL,
  `creation_user` int(11) DEFAULT NULL,
  `creation_time` datetime DEFAULT NULL,
  `last_mod_user` int(11) DEFAULT NULL,
  `last_mod_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_kode` int(11) DEFAULT NULL,
  `kategori_name` varchar(100) DEFAULT NULL,
  `is_delete` int(11) DEFAULT '0',
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `modal`
--

DROP TABLE IF EXISTS `modal`;
CREATE TABLE `modal` (
  `id_modal` int(11) NOT NULL,
  `jenis_modal` int(11) NOT NULL,
  `jumlah` bigint(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `id_pemilik` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan_barang`
--

DROP TABLE IF EXISTS `penerimaan_barang`;
CREATE TABLE `penerimaan_barang` (
  `id_rec` varchar(100) NOT NULL,
  `id_po` varchar(100) DEFAULT NULL,
  `id_produksi` varchar(100) DEFAULT NULL,
  `id_petugas` varchar(100) NOT NULL,
  `tanggal_receive` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `id_suplier` int(11) NOT NULL,
  `totalHarga` bigint(20) NOT NULL,
  `kurir` varchar(100) NOT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_barang`
--

DROP TABLE IF EXISTS `pengeluaran_barang`;
CREATE TABLE `pengeluaran_barang` (
  `id_issue` varchar(100) NOT NULL,
  `id_so` varchar(100) DEFAULT NULL,
  `id_produksi` varchar(100) DEFAULT NULL,
  `id_produkService` varchar(100) DEFAULT NULL,
  `id_petugas` varchar(100) NOT NULL,
  `id_customer` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `total` bigint(20) NOT NULL,
  `kurir` varchar(100) NOT NULL,
  `status` int(11) DEFAULT '0',
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

DROP TABLE IF EXISTS `penjualan`;
CREATE TABLE `penjualan` (
  `id_so` varchar(100) NOT NULL,
  `id_petugas` varchar(100) NOT NULL,
  `id_customer` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `total` bigint(20) NOT NULL,
  `kurir` varchar(100) NOT NULL,
  `status` int(11) DEFAULT '0',
  `tanggal_keluar` date DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL,
  `ongkir` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

DROP TABLE IF EXISTS `petugas`;
CREATE TABLE `petugas` (
  `id_petugas` varchar(100) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `jenkel` varchar(3) NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `hp` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `jabatan` varchar(200) NOT NULL,
  `photo_link` varchar(500) NOT NULL,
  `password` varchar(200) NOT NULL,
  `company_id` int(11) NOT NULL,
  `privilege` int(11) DEFAULT '1',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `created_by` int(20) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_mod_by` int(20) DEFAULT NULL,
  `last_mod_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `player_detail`
--

DROP TABLE IF EXISTS `player_detail`;
CREATE TABLE `player_detail` (
  `game_player` varchar(100) DEFAULT NULL,
  `id_item` varchar(100) DEFAULT NULL,
  `item_price` int(100) DEFAULT NULL,
  `creation_user` varchar(100) DEFAULT NULL,
  `creaton_time` datetime DEFAULT NULL,
  `last_mod_user` varchar(100) DEFAULT NULL,
  `last_mod_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchasing`
--

DROP TABLE IF EXISTS `purchasing`;
CREATE TABLE `purchasing` (
  `id_po` varchar(100) NOT NULL,
  `id_suplier` int(11) NOT NULL,
  `id_petugas` varchar(100) NOT NULL,
  `tanggal_po` date NOT NULL,
  `totalHarga` bigint(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `id_pemilik` varchar(200) DEFAULT NULL,
  `ongkir` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

DROP TABLE IF EXISTS `satuan`;
CREATE TABLE `satuan` (
  `id_satuan` int(11) NOT NULL,
  `nama_satuan` varchar(50) NOT NULL,
  `kelompok_satuan` varchar(50) NOT NULL,
  `deskripsi_satuan` varchar(100) DEFAULT NULL,
  `id_petugas` varchar(100) DEFAULT NULL,
  `id_pemilik` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_kategori`
--

DROP TABLE IF EXISTS `sub_kategori`;
CREATE TABLE `sub_kategori` (
  `subKategori_id` int(11) NOT NULL,
  `subKategori_kode` int(3) DEFAULT NULL,
  `subKategori_name` varchar(100) DEFAULT NULL,
  `is_delete` int(11) DEFAULT '0',
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

DROP TABLE IF EXISTS `suplier`;
CREATE TABLE `suplier` (
  `id_suplier` int(11) NOT NULL,
  `nama_suplier` varchar(200) NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `hp` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `tgl` datetime NOT NULL,
  `photo_link` varchar(200) NOT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

DROP TABLE IF EXISTS `tbl_roles`;
CREATE TABLE `tbl_roles` (
  `roleId` tinyint(4) NOT NULL COMMENT 'role id',
  `parent_roleId` int(11) NOT NULL,
  `role` varchar(50) NOT NULL COMMENT 'role text',
  `company_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset`
--
ALTER TABLE `asset`
  ADD PRIMARY KEY (`asset_id`);

--
-- Indexes for table `core_company`
--
ALTER TABLE `core_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_icon`
--
ALTER TABLE `core_icon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_menu`
--
ALTER TABLE `core_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_role`
--
ALTER TABLE `core_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `detail_penerimaan`
--
ALTER TABLE `detail_penerimaan`
  ADD KEY `fk_id_po` (`id_rec`),
  ADD KEY `fk_idItemgudang` (`id_item`);

--
-- Indexes for table `detail_pengeluaran`
--
ALTER TABLE `detail_pengeluaran`
  ADD KEY `FK_id_transaksi` (`id_issue`),
  ADD KEY `fk_id_produk` (`id_item`),
  ADD KEY `fk_idSupDetPengeluaran` (`id_suplier`),
  ADD KEY `fk_idrecIssue` (`id_rec`);

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD KEY `FK_id_transaksi` (`id_so`),
  ADD KEY `fk_id_produk` (`id_item`),
  ADD KEY `fk_idSupDetPenjualan` (`id_suplier`);

--
-- Indexes for table `detail_purchasing`
--
ALTER TABLE `detail_purchasing`
  ADD KEY `fk_idpo` (`id_purchasing`),
  ADD KEY `fkitempo` (`id_item`);

--
-- Indexes for table `detail_suplier`
--
ALTER TABLE `detail_suplier`
  ADD KEY `fk_id_suplier` (`id_suplier`),
  ADD KEY `fk_id_itemdetailSuplier` (`id_item`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`game_id`);

--
-- Indexes for table `gl_account`
--
ALTER TABLE `gl_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coa_g_id` (`coa_id`),
  ADD KEY `coa_id` (`coa_id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `acc_code` (`id`);

--
-- Indexes for table `gl_group`
--
ALTER TABLE `gl_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gl_has`
--
ALTER TABLE `gl_has`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `gl_journal_h`
--
ALTER TABLE `gl_journal_h`
  ADD PRIMARY KEY (`id`),
  ADD KEY `period_code` (`period_id`),
  ADD KEY `journal_name` (`journal_name`),
  ADD KEY `uid` (`uid`) USING BTREE;

--
-- Indexes for table `gl_journal_l`
--
ALTER TABLE `gl_journal_l`
  ADD PRIMARY KEY (`id`),
  ADD KEY `journal_code` (`journal_id`),
  ADD KEY `coa_number` (`acc_id`),
  ADD KEY `cd_id` (`line_credit`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `gl_schema_h`
--
ALTER TABLE `gl_schema_h`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gl_schema_l`
--
ALTER TABLE `gl_schema_l`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gl_variable`
--
ALTER TABLE `gl_variable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gudang`
--
ALTER TABLE `gudang`
  ADD KEY `fk_idItemGudang` (`id_item`),
  ADD KEY `fk_idProGudang` (`id_produksi`),
  ADD KEY `fk_idPurGudang` (`id_rec`);

--
-- Indexes for table `item_master`
--
ALTER TABLE `item_master`
  ADD PRIMARY KEY (`id_item`),
  ADD KEY `fk_tipe` (`tipe`),
  ADD KEY `fk_satuan` (`satuan`);

--
-- Indexes for table `item_stock`
--
ALTER TABLE `item_stock`
  ADD PRIMARY KEY (`stock_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `modal`
--
ALTER TABLE `modal`
  ADD PRIMARY KEY (`id_modal`);

--
-- Indexes for table `penerimaan_barang`
--
ALTER TABLE `penerimaan_barang`
  ADD PRIMARY KEY (`id_rec`),
  ADD KEY `fk_idporeceive` (`id_po`),
  ADD KEY `fk_idproreceive` (`id_produksi`);

--
-- Indexes for table `pengeluaran_barang`
--
ALTER TABLE `pengeluaran_barang`
  ADD PRIMARY KEY (`id_issue`),
  ADD KEY `FK_idPetugas` (`id_petugas`),
  ADD KEY `FK_id_custom` (`id_customer`),
  ADD KEY `fk_isoIssue` (`id_so`),
  ADD KEY `fk_proIssue` (`id_produksi`),
  ADD KEY `fk_proServIssue` (`id_produkService`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id_so`),
  ADD KEY `FK_idPetugas` (`id_petugas`),
  ADD KEY `FK_id_custom` (`id_customer`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `purchasing`
--
ALTER TABLE `purchasing`
  ADD PRIMARY KEY (`id_po`),
  ADD KEY `fk_Idsuplier` (`id_suplier`),
  ADD KEY `fk_idPet` (`id_petugas`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id_satuan`);

--
-- Indexes for table `sub_kategori`
--
ALTER TABLE `sub_kategori`
  ADD PRIMARY KEY (`subKategori_id`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`id_suplier`);

--
-- Indexes for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`roleId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset`
--
ALTER TABLE `asset`
  MODIFY `asset_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `core_company`
--
ALTER TABLE `core_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `core_icon`
--
ALTER TABLE `core_icon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_menu`
--
ALTER TABLE `core_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `core_role`
--
ALTER TABLE `core_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;
--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `game_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gl_account`
--
ALTER TABLE `gl_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `gl_group`
--
ALTER TABLE `gl_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `gl_has`
--
ALTER TABLE `gl_has`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `gl_journal_h`
--
ALTER TABLE `gl_journal_h`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10528;
--
-- AUTO_INCREMENT for table `gl_journal_l`
--
ALTER TABLE `gl_journal_l`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41483;
--
-- AUTO_INCREMENT for table `gl_schema_h`
--
ALTER TABLE `gl_schema_h`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gl_schema_l`
--
ALTER TABLE `gl_schema_l`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `gl_variable`
--
ALTER TABLE `gl_variable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `item_stock`
--
ALTER TABLE `item_stock`
  MODIFY `stock_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `kategori_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `modal`
--
ALTER TABLE `modal`
  MODIFY `id_modal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id_satuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sub_kategori`
--
ALTER TABLE `sub_kategori`
  MODIFY `subKategori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=810;
--
-- AUTO_INCREMENT for table `suplier`
--
ALTER TABLE `suplier`
  MODIFY `id_suplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id', AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
