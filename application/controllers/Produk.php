<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {
	function index(){
		$this->load->model('m_menu');
		$flag = $this->m_menu->checkAccess($this->session->userdata('roles'));
		if($flag)
		{
			redirect('produk/listItem');
		}
		else
		{
			redirect('home');
		}
	}

	//add data item
	public function addItem(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik= $this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			$idPemilik=$this->session->userdata('id_retail');
			$this->load->model('mproduk');
			$idBarang = $this->mproduk->makeId($idPemilik,$this->input->post('subKategori'));
			$data['satuan']=$this->mproduk->list_satuan($idPemilik);
			$dataitem = array('id_item' => $idBarang,
								'nama_item' => $this->input->post('namaItem'),
								'deskripsi' => $this->input->post('deskripsiItem'),
								'tipe' => $this->input->post('tipe'),
								'satuan' => $this->input->post('satuan'),
								'item_harga' => $this->input->post('hargajual'),
								'id_pemilik' => $idPemilik,
								'item_harga' => $this->input->post('harga')
							 );
			$this->mproduk->additem($dataitem);
			Redirect('Produk');
			// echo 'tipe : '.$this->input->post('hokya').' $group: '.$this->input->post('group').' '.$this->input->post('hokya2');
			// $this->load->view('dasboard/head');
			// $this->load->view('dasboard/header',$user);
			// $this->load->view('dasboard/sidebar');
			// $this->load->view('dasboard/inputItem',$data);
			// $this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}

	public function addSatuan(){
		$cek=$this->session->userdata('username');
		if($cek){

			$data = array(
					'nama' => $this->input->post('nama'),
					'kelas' => $this->input->post('kelas'),
					'deskripsi' => $this->input->post('deskripsi'),
					'id_pemilik' => $this->session->userdata('id_retail')

				);

			$this->load->model('mproduk');
			$this->mproduk->addSatuan($data);
				$this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Input data berhasil !!</div></div>");
			redirect("produk/listSatuan");
		}else{

			redirect('home');
		}
	}
	public function addTipeItem(){
		$cek=$this->session->userdata('username');
		if($cek){
			$data = array(
					'nama' => $this->input->post('nama'),
					'deskripsi' => $this->input->post('deskripsi')

				);

			$this->load->model('mproduk');
			$this->mproduk->addTipeItem($data);
			$this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Input data berhasil !!</div></div>");
			redirect("produk/listTipeItem");
		}else{

			redirect('home');
		}
	}
	//list data
	public function listItem(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			// $email=$this->session->userdata('username');
			// $this->load->model('mpetugas');
			// $idPet=$this->mpetugas->getId($email);
			// $user['user']=$this->mpetugas->view_petugas($idPet);
			// $this->load->model('mgudang');
			// $idPemilik=$this->session->userdata('id_retail');
			// $user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			// $user['alert']=$this->mgudang->alertStok($idPemilik);
			$idPe= $this->session->userdata('id_retail');
			if($this->session->userdata('game'))
			{
				$idgame = $this->session->userdata('game');
			}
			else
			{
				$idgame = 0;
			}
			$this->load->model('mproduk');
			$data['isi']=$this->mproduk->list_item($idPe,$idgame);
			// print_r($data['isi']);
			$data['satuan']=$this->mproduk->list_satuan($idPe);
			// $data['tipe']=$this->mproduk->list_tipeItem();
			// $data['tipe']=$this->mproduk->list_tipeItem($idPe);
			$data['tipe']=$this->mproduk->listGroup($idPe);

			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			$this->load->view('dasboard/listItem',$data);

			$this->load->view('dasboard/footer');
		}else{
			redirect('home');
		}
	}

	public function listSatuan(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$idPemilik=$this->session->userdata('id_retail');
			$this->load->model('mproduk');
			$data['isi']=$this->mproduk->list_satuan($idPemilik);
			$this->load->view('dasboard/head',$user);
			$this->load->view('dasboard/header');
			$this->load->view('dasboard/sidebar');
			$this->load->view('dasboard/listSatuan',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	public function listTipeItem(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			// $email=$this->session->userdata('username');
			// $this->load->model('mpetugas');
			// $idPet=$this->mpetugas->getId($email);
			// $user['user']=$this->mpetugas->view_petugas($idPet);
			// $this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			// $user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			// $user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$this->load->model('mproduk');
			$data['isi']=$this->mproduk->list_tipeItem($idPemilik);
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			$this->load->view('dasboard/listTipeItem',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	//view data
	public function viewItem($id){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			// $email=$this->session->userdata('username');
			// $this->load->model('mpetugas');
			// $idPet=$this->mpetugas->getId($email);
			// $user['user']=$this->mpetugas->view_petugas($idPet);
			// $this->load->model('mgudang');
			// $user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			// $user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$idPemilik=$this->session->userdata('id_retail');
			$this->load->model('mproduk');
			$data['isi']=$this->mproduk->viewItem($id);
			$data['satuan']=$this->mproduk->list_satuan($idPemilik);
			$data['tipe']=$this->mproduk->listGroup($idPemilik);
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			$this->load->view('dasboard/viewItem',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	public function viewProduk($id){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			// $email=$this->session->userdata('username');
			// $this->load->model('mpetugas');
			// $idPet=$this->mpetugas->getId($email);
			// $user['user']=$this->mpetugas->view_petugas($idPet);
			// $this->load->model('mgudang');
			// $user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			// $user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$idPemilik=$this->session->userdata('id_retail');
			$this->load->model('mproduk');
			$data['isi']=$this->mproduk->viewItem($id);
			// print_r($data['isi']);
			$data['rincian']=$this->mproduk->rincianProduk($id);
			// print_r($data['rincian']);
			$data['satuan']=$this->mproduk->list_satuan($idPemilik);
			$data['tipe']=$this->mproduk->listGroup($idPemilik);
			// $data['tipe']=$this->mproduk->list_tipeItem($idPemilik);
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			$this->load->view('dasboard/viewProduk',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	//action function
	public function addItem_act(){
		$cek=$this->session->userdata('username');
		if($cek){
			$this->load->library('upload');
			$nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
			$config['upload_path'] = './assets/images/produk/'; //path folder
			$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
			$config['max_size'] = '2048'; //maksimum besar file 2M
			$config['max_width']  = '1288'; //lebar maksimum 1288 px
			$config['max_height']  = '768'; //tinggi maksimu 768 px
			$config['file_name'] = $nmfile; //nama yang terupload nantinya

			$this->upload->initialize($config);
			 if($_FILES['filefoto']['name'])
			{
				//return print_r($gbr);

				//return print_r($query);
				if ($this->upload->do_upload('filefoto'))
				{
					$gbr = $this->upload->data();
					$data = array(
					  'nm_gbr' =>$gbr['file_name'],
					  'idItem' => $this->input->post('idItem'),
					  'nama' => $this->input->post('nama'),
					  'harga' => $this->input->post('harga'),
					  'satuan' => $this->input->post('satuan'),
					  'deskripsi' => $this->input->post('deskripsi'),
					  'id'=> '2017' //$this->session->userdata('username')
					);
					//return print_r($data['nm_gbr']);
					$this->load->model('mproduk');
					$query=$this->mproduk->addItem($data);
					//echo $data['harga'];
					//echo "ahahaha";

					if($query==1){
						$this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Input data berhasil !!</div></div>");
						redirect('produk/addItem');
					}else if($query==-1){
						$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<p>	<i class="icon fa fa-ban"></i>Input data gagal,Id item sudah digunakan </p></div>');
						redirect('produk/addItem');
					}else{
						$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<p>	<i class="icon fa fa-ban"></i>Input data gagal,Silahkan coba kembali </p></div>');
							redirect('produk/addItem');
					}

				}else{
					$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<p>	<i class="icon fa fa-ban"></i>Input data gagal,dimensi/ukuran terlalu besar/tipe gambar tidak didukung </p></div>');
						redirect('produk/addItem');
				}
			}else{
				 $data = array(
					  'nm_gbr' => '',
					  'idItem' => $this->input->post('idItem'),
					  'nama' => $this->input->post('nama'),
					  'harga' => $this->input->post('harga'),
					  'satuan' => $this->input->post('satuan'),
					  'deskripsi' => $this->input->post('deskripsi'),
					  'id_pemilik'=> '2017' //$this->session->userdata('id_retail')
					);
					$this->load->model('mproduk');
					$query=$this->mproduk->addItem($data);
					if($query==1){
						$this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Input data berhasil !!</div></div>");
						redirect('produk/addItem');
					}else if($query==-1){
						$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<p>	<i class="icon fa fa-ban"></i>Input data gagal,Id item sudah digunakan </p></div>');
						redirect('produk/addItem');
					}else{
							$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<p>	<i class="icon fa fa-ban"></i>Input data gagal,Silahkan coba kembali </p></div>');
							redirect('produk/addItem');
					}

			}
			echo $query;
		}else{

			redirect('home');
		}
	}
	//update item
	public function updateItem(){

		$cek=$this->session->userdata('username');
		if($cek){
			 $data = array(

					  'id_item' => $this->input->post('idItem'),
					  'nama_item' => $this->input->post('nama'),
					  'tipe' => $this->input->post('tipe'),
					  'satuan' => $this->input->post('satuan'),
					  'deskripsi' => $this->input->post('deskripsi'),
					  'item_harga' =>$this->input->post("harga"),
					);
					$this->load->model('mproduk');
					$query=$this->mproduk->updateItem($data);
					if($query==1){
						$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<p>	<i class="icon fa fa-check"></i> Update data berhasil</p></div>');
						redirect("produk/listItem");
					}else{
						$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<p>	<i class="icon fa fa-ban"></i>Update data gagal </p></div>');
						redirect("produk/listItem");
					}
		}else{
			redirect('home');
		}
	}
	public function updateSatuan(){
		$cek=$this->session->userdata('username');
		if($cek){
			$data = array(
					'id' => $this->input->post('id'),
					'nama' => $this->input->post('nama'),
					'kelas' => $this->input->post('kelas'),
					'deskripsi' => $this->input->post('deskripsi')

				);

			$this->load->model('mproduk');
			$query=$this->mproduk->updateSatuan($data);
			if($query==1){
				$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-check"></i> Update data berhasil</p></div>');
				redirect("produk/listSatuan");
			}else{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-ban"></i>Update data gagal </p></div>');
				redirect("produk/listSatuan");
			}
		}else{

			redirect('home');
		}

	}
	public function updateTipeItem(){
		$cek=$this->session->userdata('username');
		if($cek){
			$data = array(
					'id' => $this->input->post('id'),
					'nama' => $this->input->post('nama'),
					'deskripsi' => $this->input->post('deskripsi')

				);

			$this->load->model('mproduk');
			$query=$this->mproduk->updateTipeItem($data);

			if($query==1){
				$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-check"></i> Update data berhasil</p></div>');
				redirect("produk/listTipeItem");
			}else{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-ban"></i>Update data gagal </p></div>');
				redirect("produk/listTipeItem");
			}
		}else{

			redirect('home');
		}
	}
	//delete item
	public function delete($id){
		$cek=$this->session->userdata('username');
		if($cek){
			$this->load->model('mproduk');
			$query=$this->mproduk->delete($id);
			if($query==1){
				$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-check"></i> Hapus data berhasil</p></div>');
				redirect("produk/listItem");
			}else{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-ban"></i>Hapus data gagal </p></div>');
				redirect("produk/listItem");
			}
		}else{

			redirect('home');
		}
	}
	public function deleteSatuan($id){
		$cek=$this->session->userdata('username');
		if($cek){
			$this->load->model('mproduk');
			$query=$this->mproduk->deleteSatuan($id);
			if($query==1){
				$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-check"></i> Hapus data berhasil</p></div>');
				redirect("produk/listSatuan");
			}else{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-ban"></i>Hapus data gagal </p></div>');
				redirect("produk/listSatuan");
			}
		}else{

			redirect('home');
		}
	}
	public function deleteTipe($id){
		$cek=$this->session->userdata('username');
		if($cek){
			$this->load->model('mproduk');
			$query=$this->mproduk->deleteTipe($id);
			if($query==1){
				$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-check"></i> Hapus data berhasil</p></div>');
				redirect("produk/listTipeItem");
			}else{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-ban"></i>Hapus data gagal </p></div>');
				redirect("produk/listTipeItem");
			}
		}else{

			redirect('home');
		}
	}

	public function list_SubGroup()
	{
		$id = $_GET['id'];
		$this->load->model('mproduk');
		echo json_encode($this->mproduk->listSub($id));
	}

	 public function barcode(){
	 	$id = $this->session->userdata('id_retail');
	 	$this->load->model('mproduk');
		$data['row'] = $this->mproduk->list_item($id,0);
		// $data['lala'] = 123456;
        $this->load->view('dasboard/barcode_view',$data);
        // $this->load->view('dasboard/barcode_view',$lala);
        // print_r($data);        
    }

    public function stock()
    {
    	$this->load->model('m_menu');
		// $flag = $this->m_menu->checkAccess($this->session->userdata('roles'));
		$flag = 1;

		if($flag)
		{
	    	$cek=$this->session->userdata('username');
			if($cek){
				//data header
				// $email=$this->session->userdata('username');
				// $this->load->model('mpetugas');
				// $idPet=$this->mpetugas->getId($email);
				// $user['user']=$this->mpetugas->view_petugas($idPet);
				// $this->load->model('mgudang');
				// $user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
				// $user['alert']=$this->mgudang->alertStok($idPemilik);
				//
				$idPemilik=$this->session->userdata('id_retail');
				$this->load->model('mproduk');

				//pagination
				$this->load->library('pagination');
				$config['base_url']=base_url().'produk/stock';
				$idPemilik=$this->session->userdata('id_retail');
				$config['total_rows']=$this->mproduk->countstock($idPemilik);
				$config["per_page"]=$per_page=10;
				$config["uri_segment"] = 3;
				//echo $config['total_rows'];
				//config for bootstrap pagination class integration
				$config['full_tag_open'] = '<ul class="pagination">';
				$config['full_tag_close'] = '</ul>';
				$config['first_link'] = 'First';
				$config['last_link'] = 'Last';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';
				$config['prev_link'] = '&laquo';
				$config['prev_tag_open'] = '<li class="prev">';
				$config['prev_tag_close'] = '</li>';
				$config['next_link'] = '&raquo';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<li class="active"><a href="#">';
				$config['cur_tag_close'] = '</a></li>';
				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';
				$this->pagination->initialize($config);

				$data['paging']=$this->pagination->create_links();
				$page=($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$idPemilik= $this->session->userdata('id_retail');
				$data['isi']=$this->mproduk->stockItem($page,$per_page,$idPemilik);
				//

				$this->load->model('DefaultMenu');
				$this->DefaultMenu->defaultLayout();
				$this->load->view('dasboard/stockItem',$data);
				$this->load->view('dasboard/footer');
			}
			else
			{
				redirect('home');
			}
		}
		else
		{
			redirect('home');
		}
    }
}
?>
