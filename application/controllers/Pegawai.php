<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pegawai extends CI_Controller{
	//add data
	public function addPegawai()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);

			$this->load->model('mcustomer');
			$data['isi']=$this->mcustomer->list_institusi();
			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('pegawai/inputPegawai',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}

	//list data
	public function listPegawai()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			$this->load->library('pagination');
			$this->load->model('mpegawai');
			//$data['isi']=$this->mcustomer->list_customer();
			//$data['institusi']=$this->mpegawai->list_institusi();
			$config['base_url']=base_url().'pegawai/listPegawai';
			$idPemilik=$this->session->userdata('id_retail');
			$config['total_rows']=$this->mpegawai->countPegawai($idPemilik);
			$config["per_page"]=$per_page=25;
			$config["uri_segment"] = 3;

			//config for bootstrap pagination class integration
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '&laquo';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '&raquo';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);

			$data['paging']=$this->pagination->create_links();
			$page=($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$idPemilik=$this->session->userdata('id_retail');
			$data['isi']=$this->mpegawai->pageList_pegawai($page,$per_page,$idPemilik);


			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);

			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('pegawai/listPegawai',$data);
			$this->load->view('dasboard/footer');



		}else{

			redirect('home');
		}
	}

	//view data
	public function viewPegawai($id){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);

			$this->load->model('mpegawai');
			//$this->load->model('produk');
			$data['isi']=$this->mpegawai->view_pegawai($id);

			//$data['institusi']=$this->mcustomer->list_institusi();
			//$data['transaksi']=$this->mcustomer->list_transaksi($id);
			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('pegawai/viewPegawai',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}

	//update data
	public function updatePegawai(){
		$cek=$this->session->userdata('username');
		if($cek){
			$data = array(
					
					'idPegawai' => $this->input->post('idPegawai'),
					'nama' => $this->input->post('nama'),
					'jenkel' => $this->input->post('jenkel'),
					'alamat' => $this->input->post('alamat'),
					'hp' => $this->input->post('hp'),
					'email' => $this->input->post('email'),
					'tanggungan' => $this->input->post('tanggungan'),
					'shift' => $this->input->post('shift'),
					'jabatan' => $this->input->post('jabatan')

				);
			$this->load->model('mpegawai');
			$query=$this->mpegawai->update($data);
			if($query==1){
				$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>	<i class="icon fa fa-check"></i> Update data berhasil</p></div>');
				redirect("pegawai/listPegawai");
			}else{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>	<i class="icon fa fa-ban"></i>Update data gagal </p></div>');
				redirect("pegawai/listPegawai");
			}
		}else{

			redirect('home');
		}

	}
	//delete data
	public function deletePegawai($id){
		$cek=$this->session->userdata('username');
		if($cek){
			$this->load->model('mpegawai');
			$query=$this->mpegawai->delete($id);
			if($query==1){
				$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>	<i class="icon fa fa-check"></i> Hapus data berhasil</p></div>');
				redirect("pegawai/listPegawai");
			}else{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>	<i class="icon fa fa-ban"></i>data gagal dihapus,Data dipakai tabel lain</p></div>');
					redirect("pegawai/listPegawai");
			}

		}else{

			redirect('home');
		}
	}

	//searching data
	public function cariPegawai(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);

			$word=$this->input->post('cari');
			$this->load->model('mpegawai');
			$data['isi']=$this->mpegawai->list_cariCustomer($word);
			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('pegawai/listPegawai',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}

	}
	//actian function
	public function addPegawai_act(){
		$this->db->reconnect();

		$cek=$this->session->userdata('username');
		if($cek){
			$data = array(
					
					'idPegawai' => $this->input->post('idPegawai'),
					'nama' => $this->input->post('nama'),
					'jenkel' => $this->input->post('jenkel'),
					'alamat' => $this->input->post('alamat'),
					'hp' => $this->input->post('hp'),
					'email' => $this->input->post('email'),
					'jabatan' => $this->input->post('jabatan'),
					'tanggungan' => $this->input->post('tanggungan'),
					'shift' => $this->input->post('shift'),
					'id_pemilik' => $this->session->userdata('id_retail')


				);
			$this->load->model('mpegawai');
			$query=$this->mpegawai->addPegawai($data);

			if($query==0){
				$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>	<i class="icon fa fa-check"></i> input data berhasil</p></div>');
				redirect("pegawai/addPegawai");
			}else if($query==-1){
				$this->session->set_flashdata('pesan','<div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>	<i class="icon fa fa-warning"></i>No.id sudah terpakai, silahkan gunakan No.id yang lain.</p></div>');
				redirect("pegawai/addPegawai");
			}else{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>	<i class="icon fa fa-ban"></i>data gagal diinputkan</p></div>');
				redirect("pegawai/addPegawai");
			}
		}else{

			redirect('home');
		}
	}

// penggajian

	public function listGaji(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$idPemilik=$this->session->userdata('id_retail');
			$this->load->model('mpegawai');
			$data['tanggal']=$this->input->post('tanggal').'-31';
			$data['tampil']=$this->input->post('tanggal').'-27';
			$data['atur']=$this->mpegawai->list_atur($idPemilik);
			// print_r($data['atur']);
			// $data['tanggal']='2017-01-31';
			$data['gaji']=$this->mpegawai->list_jual($data,$idPemilik);
			$this->load->view('dasboard/head',$user);
			$this->load->view('dasboard/header');
			$this->load->view('dasboard/sidebar');
			// echo $data['cek'];
			$this->load->view('dasboard/listGaji',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}

	public function bayarGaji(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$data['tanggal']=$this->input->post('tanggal');
			$data['total']=$this->input->post('total');
			// print_r($data['total']);
			$idPemilik=$this->session->userdata('id_retail');
			$this->load->model('mpegawai');
			// $this->load->model('mmodal');
			// $this->load->model('mpenjualan');
	    	$haha = $this->mpegawai->cek2();
	    	$idjurnalH = $haha->apa;
			// $jenis2 = $this->mmodal->cekjenis($id);
			//echo $jenis;
			//$jenis= $jenis2->jenis_modal;
			$this->mpegawai->bayarGaji($idPemilik,$data,$idjurnalH);
			$this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Pelunasan berhasil !!</div></div>");
		redirect("Pegawai/listGaji");
		}
	}

	public function updateAtur(){
		$cek=$this->session->userdata('username');
		if($cek){

			$data = array(
					'pokok' => $this->input->post('pokok'),
					'tunjangan' => $this->input->post('tunjangan'),
					'bonus'  => $this->input->post('bonus'),
					'satuan'  => $this->input->post('satuan'),
					'id' => $this->session->userdata('id_retail')
				);

			$this->load->model('mpegawai');
			$this->mpegawai->update_gaji($data);
				$this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Update data berhasil !!</div></div>");
			redirect("Pegawai/listGaji");
		}else{

			redirect('home');
		}
	}


}
?>
