<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_menu');
		$this->load->model('mpetugas');
		$this->load->model('m_role');
	}

	public function index(){
		$cek=$this->session->userdata('username');
		//$menu = $this->m_menu->getId(strtolower($this->uri->uri_string()));
		$flag = $this->m_menu->checkAccess($this->session->userdata('roles'));
		if($flag){
			//data header
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			$this->load->view('dasboard/core/menu/index');
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}


	public function ListMenu()
	{
		$email=$this->session->userdata('username');
		$user_id=$this->mpetugas->getId($email);
		// $user_id=$this->input->post('user');
		$role=$this->session->userdata('roles');

		$result = array();
		// print_r($user_id);
		$list = $this->m_menu->listParent($user_id,1);
		// print_r(json_decode($list,TRUE));
		// print_r($list);
		return $list;
	}

	public function inputMenu()
	{
			$role=$this->session->userdata('roles');
			// $this->load->model('m_menu');
			// $dataMenu['menu'] = $this->m_menu->listParent($email,1);

			$this->load->model('m_menu');
			$dataParent['menu'] = $this->m_menu->listParent($role,0);
			$this->load->model('DefaultMenu');
			$this->DefaultMenu->defaultLayout();
			$this->load->view('dasboard/core/menu/inputMenu',$dataParent);			
			$this->load->view('dasboard/footer');
	}

	public function addMenu_act()
	{
		$email=$this->session->userdata('username');
		$this->load->model('mpetugas');
		$idPet=$this->mpetugas->getId($email);
		$data = array(
            'parent_id' => $this->input->post('parent_id'),
            'name' => $this->input->post('name'),
            'controller' => $this->input->post('controller') ,
            'icon_name' => $this->input->post('icon_name') ,
            'company_id' => $this->session->userdata('company'),
            'creation_user' => $idPet,
            'group_id' => $this->session->userdata('roles'),
            );
		$query=$this->m_menu->addMenu($data);
		// print_r($query);
		if($query){
			echo json_encode(array('success'=>true));
		}else {
    		echo json_encode(array('msg'=>'Gagal memperbarui data.<br>Menu sudah tersedia.'));
  		}
		
	}

	public function updateMenu_act($id)
	{
		// print_r($id);
		$data = array(
            'parent_id' => $this->input->post('parent_id'),
            'name' => $this->input->post('name'),
            'controller' => $this->input->post('controller') ,
            'icon_name' => $this->input->post('icon_name') ,
            );
		$query=$this->m_menu->update($data,$id);
		// // print_r($query);
		if($query){
			echo json_encode(array('success'=>true));
		}else {
    		echo json_encode(array('msg'=>'Gagal memasukkan data.<br>Mungkin menu sudah tersedia.'));
  		}

	}

	public function addPrivilege()
	// public function addPrivilege($group_id)
	{
		$role  = $this->session->userdata('roles');
		$company_id=$this->session->userdata('company');
		$this->load->model('m_company');
		$company = $this->m_company->findCompany($company_id);

		$data['roles'] = $this->m_role->read($role);
		// $data['roles'] = $group_id;
		$data['menu'] = $this->m_menu->listMenu($company,0);


		$this->load->model('DefaultMenu');
		$this->DefaultMenu->defaultLayout();
		$this->load->view('dasboard/core/privilege/inputprivilege',$data);			
		$this->load->view('dasboard/footer');
	}

	public function listAllMenu()
	{
		$company_id=$this->session->userdata('company');
		$this->load->model('m_company');
		$company = $this->m_company->findCompany($company_id);
		$result = $this->m_menu->FullMenu($company,0);
		// $dataawal = array('data'=>$result);
		if($result)
		{
			$data = json_encode($result);
		}
		else
		{
			$data = json_encode(array());
		}
		echo $data;
	}
	public function listAllParent()
	{
		$company_id=$this->session->userdata('company');
		$this->load->model('m_company');
		$company = $this->m_company->findCompany($company_id);
		$result = $this->m_menu->listMenu($company,1);
		// $dataawal = array('data'=>$result);
		if($result)
		{
			$data = json_encode($result);
		}
		else
		{
			$data = json_encode(array());
		}
		echo $data;
	}

	public function addPrivilege_act($group_id)
	{
		// echo $data;
		$data = json_decode($_POST['data'], true);
		// var_dump(array_column($data, 'privilege_list'));
		// $group_id = $data[0]['roles'];
		$menu = array_column($data, 'privilege_list');
		// print_r($this->input->post('privilege_list'));
		// // $count = 0;
		// foreach ($menu as $menus) 
		// {
		// 	echo $menus.' ';
		// }

			// print_r($data);
		// $menu = array('1','2','3','4','5');
			$query=$this->m_menu->addPrivilege($menu,$group_id);
		// 	// $count+=1;
		// // }
		if($query){
			echo json_encode(array('success'=>true));
		}else {
    		echo json_encode(array('msg'=>'Gagal memasukkan data.'));
  		}
	}

	public function edit()
	{
		$query = $this->m_menu->editMenu($id);
		if($query){
			// $data = json_encode($query);
			// echo $data;
			return $query;
  		}	
	}

	public function delete($id){
		$data = array(
				'is_delete' => 1,
				'last_mod_user' => $this->session->userdata('id_petugas'),
			);
		// $idfinal = $this->m_menu->findMenu($id);
		// $query=$this->m_menu->delete($data,$idfinal);
		$query=$this->m_menu->delete($data,$id);

		if($query){
			echo json_encode(array('success'=>true));
		}else {
    		echo json_encode(array('msg'=>'Gagal menghapus data.'));
  		}
	}

}