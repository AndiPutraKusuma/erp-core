<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backup extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_Backup');
	}

	public function exportSQL()
	{
		$this->m_Backup->Export_DatabaseSQL($this->session->userdata('id_retail'));
	}

	public function exportCSV()
	{
		$this->m_Backup->Export_DatabaseCSV();
	}

}
