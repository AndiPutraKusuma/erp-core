<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Contohbarcode extends CI_Controller {
	 
	public function index()
	{
		$this->load->view('dasboard/barcode_view');
	}
	 
	public function bikin_barcode($kode)
	{
		//kita load library nya ini membaca file Zend.php yang berisi loader
		//untuk file yang ada pada folder Zend
		$this->load->library('zend');
		 
		//load yang ada di folder Zend
		$this->zend->load('Zend/Barcode');
		 
		//generate barcodenya
		//$kode = 12345abc;
		$barcodeOptions = array(
		    'text' => $kode, 
		    'barHeight'=> 35, 
		    'factor'=>2,
		);


		$rendererOptions = array();
		$renderer = Zend_Barcode::factory(
		    'code128', 'image', $barcodeOptions, $rendererOptions
		)->render();
		// Zend_Barcode::render('code128', 'image', array('text'=>$kode), array());
		}
		//end of class
}