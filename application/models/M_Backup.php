<?php
class M_Backup extends CI_Model{

	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }

    public function Export_DatabaseSQL($id_pemilik)
    {
        date_default_timezone_set('GMT');
       // Load the file helper in codeigniter
        $this->load->helper('file');


        $con = mysqli_connect("localhost","root","hominatest","retail_core");

        // $tables = array();
        // $query = mysqli_query($con, 'SHOW TABLES');
        // while($row = mysqli_fetch_row($query)){
        //      $tables[] = $row[0];
        // }
		$tables = array('asset','detail_penerimaan','detail_pengeluaran','detail_penjualan','detail_purchasing','detail_suplier','gl_journal_h','gl_journal_l','gudang','item_master','kategori','modal','penerimaan_barang','pengeluaran_barang','penjualan','petugas','purchasing','sub_kategori','suplier');


        $result = 'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";';
        $result .= 'SET time_zone = "+00:00";';

        foreach($tables as $table){
    	if ($table == 'gl_journal_h' || $table == 'gl_journal_l')
        {
            $temp = "SELECT * FROM `".$table."` where uid = '".$id_pemilik."'";
        }
        elseif ($table == 'asset') 
        {
            $temp = "SELECT * FROM `".$table."` where id_retail = '".$id_pemilik."'";
            // $query = "SELECT * FROM `".$table."` where id_retail = '1235557' LIMIT ".($b*$batchSize-$batchSize).",".$batchSize;  
        }
        elseif ($table == 'petugas') 
        {
            $temp = "SELECT * FROM `".$table."` where id_petugas = '".$id_pemilik."'";
            // $query = "SELECT * FROM `".$table."` where id_petugas = '1235557' LIMIT ".($b*$batchSize-$batchSize).",".$batchSize; 
        }       
        else
        {
            $temp = "SELECT * FROM `".$table."` where id_pemilik = '".$id_pemilik."'";
        }
        $query = mysqli_query($con,$temp);
        $num_fields = mysqli_num_fields($query);

        // $result .= 'DROP TABLE IF EXISTS '.$table.';';
        // $row2 = mysqli_fetch_row(mysqli_query($con, 'SHOW CREATE TABLE `'.$table.'`'));
        // $result .= "\n\n".$row2[1].";\n\n";

        for ($i = 0; $i < $num_fields; $i++) {
        while($row = mysqli_fetch_row($query)){
           $result .= 'INSERT INTO `'.$table.'` VALUES(';
             for($j=0; $j<$num_fields; $j++){
               $row[$j] = addslashes($row[$j]);
               $row[$j] = str_replace("\n","\\n",$row[$j]);
            if(isset($row[$j])){
                   $result .= '"'.$row[$j].'"' ; 
                }else{ 
                    $result .= '""';
                }
                if($j<($num_fields-1)){ 
                    $result .= ',';
                }
            }
            $result .= ");\n";
        }
        }
        $result .="\n\n";
        }

        //Create Folder
        $folder = 'database/';
        if (!is_dir($folder))
        mkdir($folder, 0777, true);
        chmod($folder, 0777);

        $date = date('m-d-Y'); 
        $filename = $folder."backup_data-".$date.'-'.$id_pemilik; 

        $handle = fopen($filename.'.sql','w+');
        fwrite($handle,$result);
        fclose($handle);
        // $file_url = 'http://www.myremoteserver.com/file.exe';
        // header('Content-Type: text; name="'.$filename.'.sql"');
		// header('Content-Disposition: attachment; filename="'.$filename.'.sql"');
		header('Content-Type: application/octet-stream');
		header("Content-Transfer-Encoding: Binary"); 
		header("Content-disposition: attachment; filename='" .$filename.".sql'"); 
		readfile($filename.'.sql'); 
        //redirect('admin');           
    }

    public function Export_DatabaseCSV()
	{
		$server = 'localhost';
		$login = 'root';
		$password = 'hominatest';
		$db = 'retail_core';
		$id_pemilik = $this->session->userdata('id_retail');
		$filename = 'backup-'.$id_pemilik.'-'.date('m-d-Y').'.csv'; 
		$FILE = fopen($filename, "w");
		$con  = mysqli_connect($server, $login, $password,$db);
		//$res = mysqli_query($con,"SHOW TABLES FROM $db");
		$tables = array('asset','detail_penerimaan','detail_pengeluaran','detail_penjualan','detail_purchasing','detail_suplier','gl_journal_h','gl_journal_l','gudang','item_master','kategori','modal','penerimaan_barang','pengeluaran_barang','penjualan','petugas','sub_kategori','suplier');
		// while($row = mysqli_fetch_array($res, MYSQL_NUM)) {
		    // $tables[] = "$row[0]";
		// }
		// print_r($tables);
		foreach($tables as $table) {
		    $columns = array();
		    $res = mysqli_query($con,"SHOW COLUMNS FROM $table");
		    while($row = mysqli_fetch_array($res, MYSQLI_NUM)) {
		        $columns[] = "$row[0]";
		        // echo $row[0].' yeay ';
		    }
		    // print_r(implode(",", $columns));
		    fwrite($FILE, implode(",", $columns)); fwrite($FILE,"\n");
	    	 if ($table == 'gl_journal_h' || $table == 'gl_journal_l')
	            {
				    $resTable = mysqli_query($con,"SELECT * FROM $table where uid = '".$id_pemilik."'");
	            }
	            elseif ($table == 'asset') 
	            {
				    $resTable = mysqli_query($con,"SELECT * FROM $table where id_retail = '".$id_pemilik."'");

	            	// $query = "SELECT * FROM `".$table."` where id_retail = '1235557' LIMIT ".($b*$batchSize-$batchSize).",".$batchSize;	
	            }
	            elseif ($table == 'petugas') 
	            {
				    $resTable = mysqli_query($con,"SELECT * FROM $table where id_petugas = '".$id_pemilik."'");

	            	// $query = "SELECT * FROM `".$table."` where id_petugas = '1235557' LIMIT ".($b*$batchSize-$batchSize).",".$batchSize;	
	            }                    
	            else
	            {
				    $resTable = mysqli_query($con,"SELECT * FROM $table where id_pemilik = '".$id_pemilik."'");

	                // $query = "SELECT * FROM `".$table."` where id_pemilik = '1235557'  LIMIT ".($b*$batchSize-$batchSize).",".$batchSize;

	            }
		    //$resTable = mysqli_query("SELECT * FROM $table");
		    while($row = mysqli_fetch_array($resTable, MYSQLI_NUM)) {
		        fwrite($FILE, implode(",", $row)); fwrite($FILE,"\n");
		    }
		}

	    header('Content-Type: text/csv; name="'.$filename.'"');
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		readfile($filename);
	}
}


?>
