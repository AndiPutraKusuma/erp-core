<?php
class Mpenjualan extends CI_Model{

	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }

	 public function cek()
	{
		$query=$this->db->query("SELECT MAX(id)+1 as apa FROM gl_journal_h ");
		return $query->row();
	}

	public function cek2()
	{
		$query=$this->db->query("SELECT MAX(id)+1 as apa FROM gl_journal_h ");
		return $query->row();
	}

	public function max()
	{
		$query=$this->db->query("SELECT MAX(id_so)+1 as dipsy FROM penjualan ");
		return $query->row();	
	}

	public function total($data)
	{
		$query = $this->db->query("SELECT c.harga*c.jumlah as jumlahjual , a.hargaSatuan*c.jumlah as jumlahbeli  FROM detail_purchasing a, detail_penjualan c,purchasing d , penjualan e WHERE d.id_po = id_purchasing and d.id_suplier =  c.id_suplier and d.id_pemilik='$data[id_pemilik]' and e.id_so='$data[idTransaksi]'");
		// if ($query->num_rows() > 0)
		// 	{
		// 	foreach ($query->result() as $row)
		// 	{
		// 			$hasil[] = $row;
		// 	}
		// 	return $hasil;
		// 	}
		// 	else{
		// 		return 0;
		// 	}
			//result return dibenerke (ketoke)

		// $query = "SELECT c.harga*c.jumlah as jumlahjual , a.hargaSatuan*c.jumlah as jumlahbeli  FROM detail_purchasing a, detail_penjualan c,purchasing d WHERE d.id_po = id_purchasing and d.id_suplier =  c.id_suplier and d.id_pemilik=$id";
		return $query->result();
		// print_r($query->result());
	}
	public function addPenjualan($data,$idjurnalL,$idjurnalH,$pemilik,$total){
		$this->db->reconnect();
		$query=$this->db->query("CALL sp_input_penjualan('$data[idTransaksi]','$data[email]','$data[idCustomer]','$data[total]','$data[tgl]','$data[kurir]','$data[ongkir]','$data[id_pemilik]')");
		foreach($this->cart->contents() as $item){
			$idSup=$item['options']['idSuplier'];
			$barcode=$item['options']['barcode'];
			// $this->db->query("CALL sp_input_detailPenjualan('$data[idTransaksi]','$item[id]','$item[qty]','$item[price]','$idSup')");
			$this->db->query("CALL sp_input_detailPenjualan('$data[idTransaksi]','$item[id]','$item[qty]','$item[price]','$idSup','$barcode',$data[id_pemilik])");

			$detail = $this->db->query("SELECT DISTINCT c.harga*c.jumlah AS jumlahjual , a.hargaSatuan*c.jumlah AS jumlahbeli , a. barcode_barang
				FROM gudang a 
				INNER JOIN detail_penjualan AS c ON c.`barcode_barang` = a.`barcode_barang`
				where a.id_pemilik='$data[id_pemilik]' and c.id_so='$data[idTransaksi]'");
            $jual = $detail->row();
			// $query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',4000,0,$jual->jumlahjual,'$data[id_pemilik]')");
			// // $query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',2200,0,(1/11)*'$data[total]','$data[id_pemilik]')");
			// $query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',1000,'$jual->jumlahjual',0,'$data[id_pemilik]')");
			// $query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',1400,0,'$jual->jumlahbeli','$data[id_pemilik]')");
			// $query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',5000,'$jual->jumlahbeli',0,'$data[id_pemilik]')");

		}
            $query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES('$data[tgl]',CONCAT('Pembayaran Invoice ','$data[idTransaksi]'),'$data[id_pemilik]')");


			$this->load->model('mgl');
			$schema = $this->mgl->schema_line(2);
			$searchArray = array("HB", "HJ", "0");
			$replaceArray = array($jual->jumlahbeli,$jual->jumlahjual,0);
			foreach ($schema as $line) {
				$data_line = array(
					'journal_id' => $idjurnalL,
					'acc_id' => $line->acc_id,
					'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
					'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
					'uid' => $data['id_pemilik'],
				);
				$oke = $this->db->insert('gl_journal_l',$data_line);
			}
		$this->cart->destroy();
	 }

	public function list_penjualan(){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_list_penjualan()");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	 public function pageList_penjualan($start,$limit,$id){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_pageList_penjualan2($start,$limit,$id)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	 //deatil pemesanan
	 public function rincianPenjualan($id){
		 $this->db->reconnect();
			$query = $this->db->query("CALL sp_rincianPenjualan('$id')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	 }
	public function list_cariProduk($word){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_cari_suplierProduk('$word')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function list_produk_perSuplier($id){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_listProduk_perSuplier('$id')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}

	}
	//update status
	 public function updateStatus($data){
		 $this->db->reconnect();
		 $query=$this->db->query("CALL sp_updateStatus_penjualan('$data[idTransaksi]','$data[status]')");
	 }
	 public function countPenjualan($id){

		$this->db->reconnect();
			$query = $this->db->query("CALL sp_hitungPenjualan($id)");

				$row=$query->row();
				return $row->jumlah;


	}
	 public function countPenjualanSukses($id){

		$this->db->reconnect();
			$query = $this->db->query("CALL sp_hitungPenjualanSukses($id)");

				$row=$query->row();
				return $row->jumlah;


	}
	public function cekStock($id)
	{
		$stock = 0;
		$this->db->reconnect();
		$this->db->select('sum(jumlah) as stock');
		$this->db->where('id_item',$id);
		// $this->db->where('id_pemilik',$data['id_pemilik']);
		// $this->db->where('jumlah > 0');
		$read = $this->db->get('gudang');
		foreach ($read->result() as $datastock) 
		{
			$stock = $datastock->stock;
		}
		return $stock;
	}

	// public function GetItem($id_barang,$harga,$butuh,$id_so)
	public function Sales_header($data)
	{
		$this->db->reconnect();
		$ok = $this->db->insert('penjualan',$data);
		// $this->load->model('mpenjualan');
		$this->load->model('mgudang');
		$haha = $this->cek2();
		$idjurnalH = $haha->apa;
		$detail = $this->db->query("SELECT DISTINCT c.harga*c.jumlah AS jumlahjual , a.hargaSatuan*c.jumlah AS jumlahbeli , a. barcode_barang
				FROM gudang a 
				INNER JOIN detail_penjualan AS c ON c.`barcode_barang` = a.`barcode_barang`
				where a.id_pemilik='$data[id_pemilik]' and c.id_so='$data[id_so]'");
            $jual = $detail->row();
		$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES('$data[tanggal]',CONCAT('Pembayaran Invoice ','$data[id_so]'),'$data[id_pemilik]')");
		$this->load->model('mgl');
		$schema = $this->mgl->schema_line(2);
		$searchArray = array("HB", "HJ", "0");
		$replaceArray = array($jual->jumlahbeli,$jual->jumlahjual,0);
		foreach ($schema as $line) {
			$data_line = array(
				'journal_id' => $idjurnalH,
				'acc_id' => $line->acc_id,
				'line_debit' => (int)str_replace($searchArray, $replaceArray, $line->line_debit),
				'line_credit' => (int)str_replace($searchArray, $replaceArray, $line->line_credit),
				'uid' => $data['id_pemilik'],
			);
			$ok = $this->db->insert('gl_journal_l',$data_line);
		}
		if($ok)
		{ //auto keluar barang
			$dataupdate = array(
				'idTransaksi' => $data['id_so'],
				'status' => 4
            		);
			$ok3 = $this->updateStatus($dataupdate);
			$data2 = $this->mgudang->rincianViewSO($data['id_so']);
			$dataSO = $data2[0];
            $datagudang = array(
					'idSO' => $data['id_so'],
					'idTransaksi' => uniqid("ISSUE"),
					'total' => $dataSO->total,
					'tgl' => $dataSO->tanggal,
					'kurir' => 1,
					'idCustomer' => $dataSO->id_customer,
					'email' => $this->session->userdata('username'),
					'kode' => 1,
					'id_pemilik' =>$this->session->userdata('id_retail')
				);
            // print_r($datagudang);
			$this->mgudang->keluarGudang($datagudang);
		
			//sampe sini
			if ($ok3)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;	
		}
	}
	public function GetItem($data)
	{
		$result = array();
		$id_pemilik = $this->session->userdata('id_retail');
		//$id_so = uniqid("SO"); iki ndek controller
		$butuh = $data['butuh'];
		$stock = $this->cekStock($data['id_barang']);
		if($stock >= $butuh)
		{	
			$this->db->reconnect();
			$this->db->select('barcode_barang , jumlah as stock, id_supplier');
			$this->db->where('id_item',$data['id_barang']);
			// $this->db->where('id_pemilik',$data['id_pemilik']);
			$this->db->where('jumlah > 0');
			$read = $this->db->get('gudang');
			foreach ($read->result() as $data2) 
			{
				if($butuh >= $data2->stock)
				{
					$jumlah = $data2->stock;
				}
				else
				{
					$jumlah = $butuh;
				}
				echo '<script> console.log('.$butuh.' '.$jumlah.') </script>';
				array_push(
	                    $result,
	                    array(
	                    	'id_so' =>$data['id_so'],
	                        'barcode'=>$data2->barcode_barang,
	                        'jumlah' =>$jumlah,
	                    )
	            ); 
				$insert = array(
					'id_so' => $data['id_so'],
					'id_item' => $data['id_barang'],
					'jumlah' => $jumlah,
					'harga' => $data['harga'],
					'id_pemilik' =>$id_pemilik,
					'id_suplier' => $data2->id_supplier,
					'barcode_barang' =>$data2->barcode_barang
					);
				$ok = $this->db->insert('detail_penjualan',$insert);
				//$butuh = $butuh-$jumlah;

				if($ok)
				{
					$update = array(
						'jumlah' => $data2->stock - $jumlah
						);
					$this->db->where('barcode_barang',$data2->barcode_barang);
					$ok2 = $this->db->update('gudang',$update);
					$butuh = $butuh - $jumlah;
					if($butuh == 0)
					{
						break;
					}
				}
			}
			return $result;
		}
		else
		{
			return 'ga cukup';
		}
	}

}


?>
