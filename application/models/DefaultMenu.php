<?php class DefaultMenu extends CI_Model {

    public function defaultLayout()
        {
            $email=$this->session->userdata('username');
            //data header
            $this->load->model('mpetugas');
            $idPet=$this->mpetugas->getId($email);
            $user['user']=$this->mpetugas->view_petugas($idPet);
            $this->load->model('mgudang');
            $idPemilik=$this->session->userdata('id_retail');
            $user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
            $user['alert']=$this->mgudang->alertStok($idPemilik);

            $this->load->model('m_menu');
            $role=$this->session->userdata('roles');

            $dataMenu['menu'] = $this->m_menu->listParent($role,1);

            $this->load->view('dasboard/head');
            $this->load->view('dasboard/header',$user);
            $this->load->view('dasboard/sidebar',$dataMenu);
            // $this->load->view($link,$data);
            // $this->load->view('dasboard/footer');
        }

}
?>
