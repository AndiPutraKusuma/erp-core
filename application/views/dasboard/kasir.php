<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		Penjualan

		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Penjualan</a></li>
			<li class="active">Add Penjualan</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
			<div id="cc" class="easyui-layout" data-options="fit:true" style="height:500px">
			    <div data-options="region:'north',title:'Kasir',collapsible:false" style="height:100px;">
    	            <div style="padding: 5px">
    	            	<form id="forminput">
	    	            <input id="input_barcode" class="easyui-textbox" label=" Barcode:" labelPosition="left" style="width:50%;"><br>
				    	<!-- <label for="item_name">Item Name:</label> -->
	                	<input class="easyui-combogrid" id="item" name="item_name" label=" Item Name:" labelPosition="left" data-options="
		                    panelHeight: 'auto',
		                    idField: 'id_item',
		                    textField: 'nama_item',
		                    url: '<?php echo base_url() ?>kasir/getItem',
		                    method: 'get',
		                    columns: [[
		                        {field:'nama_item',width:'100%'}
		                    ]],
		                    fitColumns: true,
		                    pagination: true
		                " style="width:50%; padding:5px" />
	                	<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="save($('#item').val())"></a>
	                	</form>
                	</div>
			    </div>
			    <div data-options="region:'south',collapsible:false" style="height:100px;">
			    	<table id="sub-total" style="width:100%;height:auto"
					rownumbers="false" fitColumns="true" singleSelect="false" idField="keterangan">
						<thead>
							<tr>
								<th field="keterangan" width="75%"></th>
								<!-- <th field="stockcode" width="15%">Stock Code</th> -->
								<th field="value" width="25%" formatter="formatPrice"></th>
							</tr>
						</thead>
					</table>
					<div style="padding: 5px;" align="right">
						<a href="#" class="easyui-linkbutton c8" style="width:120px" onclick="bayar()">Submit</a>
					</div>
			    </div>
			    <div data-options="region:'center'" style="padding:5px;">
					<table id="cart" title="List Item" style="width:100%;height:100%"
					rownumbers="true" fitColumns="true" singleSelect="true" idField="barcode">
						<thead>
							<tr>
								<th field="barcode" width="20%">Barcode</th>
								<!-- <th field="stockcode" width="15%">Stock Code</th> -->
								<th field="name" width="25%">Item Name</th>
								<th field="price" width="20%" formatter="formatPrice">Price</th>
								<th field="quantity" width="15%" formatter="formatPrice" editor="numberbox">Quantity</th>
								<th field="total" width="20%" formatter="formatPrice">Total</th>
							  
							</tr>
						</thead>
					</table>
			    </div>
			</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div id="mm" class="easyui-menu">
	<div onclick="javascript:$('#cart').edatagrid('destroyRow')">Delete</div>
</div>
<!-- <div id="tb" style="height:auto">
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#schema_line').edatagrid('addRow')">New</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#schema_line').edatagrid('destroyRow')">Destroy</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#schema_line').edatagrid('saveRow')">Save</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#schema_line').edatagrid('cancelRow')">Cancel</a>
</div> -->
<div id="modalloading" style="display: none">
	<div class="center">
		<img alt="" src="<?php echo base_url() ?>assets/images/loadinglg.gif" />
	</div>
</div>

<!-- iki -->
<div id='response'>
</div>>

<div id="FormBayar" class="easyui-dialog" style="width:400px; height:300px; padding: 10px 20px" closed="true" buttons="#dialog-buttons">
	<form id="formbayar" method="post">
		<div class="form-item">
			<label for="type" style="font-size: 16px; margin-top: 10px">Total Belanja</label><br />
			<input type="text" id="total_bayar" name="total_bayar" class="easyui-textbox" required="true" style="width: 100%" readonly="true" />
		</div>
		<div class="form-item">
			<label for="type" style="font-size: 16px; margin-top: 10px">Pembayaran</label><br />
			<input id="bayar_tunai" name="bayar_tunai" class="easyui-numberbox" data-options="precision:0, groupSeparator:',',decimalSeparator:'.',width:'100%'" required="true">
		</div>
		<div class="form-item">
			<label for="type" style="font-size: 16px; margin-top: 10px">Kembalian</label><br />
			<input type="text" id="kembalian" name="kembalian" class="easyui-textbox" required="true" style="width: 100%" readonly="true" />
		</div>
	</form>
</div>
<div id="dialog-buttons">
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="simpan()">Simpan</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:jQuery('#FormBayar').dialog('close')">Batal</a>
</div>

<script type="text/javascript">

	var arr_cart = [];
	// var arr_carto = new Array();
	var arr_total = [];
	var data_json = {total:0,rows:[]};
	var data_total = {total:0,rows:[]};
	var obj_total = {};
		// var mbuh = str.replace("[","");
	    obj_total.keterangan = "Sub Total";
		obj_total.value = 0;
		arr_total.push(obj_total);
		data_total.rows = arr_total;

	function reset(){
    	arr_cart = [];
		count();
    }

	function bayar(){
		var total = 0;
      	$('#FormBayar').dialog('open').dialog('setTitle','Pembayaran');
		$('#formbayar').form('clear');
		arr_total.forEach( function (totalItem){
			if (totalItem.keterangan == "Sub Total") {
				total = totalItem.value;
			}
		});
      	$('#total_bayar').textbox('setValue', total);
      	$('#kembalian').textbox('setValue', 0);
      	url = '<?php echo base_url() ?>kasir/add';
      	 
      	// console.log (JSON.stringify(arr_carto));
      	// console.log (arr_cart);

    }
		
	function reload(){
		data_json.rows = arr_cart;
		console.log(arr_cart);
		$('#cart').edatagrid('loadData',data_json);
		$('#sub-total').edatagrid('loadData',data_total);
	}

	function formatPrice(val,row){
		var x = parseInt(val);
		return x.toLocaleString('ind');
		// console.log(typeof val);
    }

    function formatNumber(val){
		var x = parseInt(val);
		return x.toLocaleString('ind');
		// console.log(typeof val);
    }

	function count(){
		var total_awal = 0
		arr_cart.forEach( function (arrayItem){
			arrayItem.total = parseInt(arrayItem.quantity)*parseInt(arrayItem.price);
			total_awal = total_awal + arrayItem.total;
		});
		arr_total.forEach( function (totalItem){
			if (totalItem.keterangan == "Sub Total") {
				totalItem.value = total_awal;
			}
		});
		reload();
	}	

	function add(code){
		var lala = 0;
		arr_cart.forEach( function (arrayItem)
		{
		    var x = arrayItem.barcode;
		    // alert(x);
		    if(x == code){
		    	arrayItem.quantity = parseInt(arrayItem.quantity) + 1;
		    	// alert(x);
		    	count();
		    	lala = 1;
		    };
		    
		});
		// arr_carto.forEach( function (arrayItem2)
		// {
		//     var x = arrayItem2.barcode;
		//     // alert(x);
		//     if(x == code){
		//     	arrayItem2.quantity = parseInt(arrayItem2.quantity) + 1;
		//     	// alert(x);
		//     	count();
		//     	lala = 1;
		//     };
		    
		// });
		// alert(lala);
		return lala;
	}

    $(function(){
      // var row = $('#schema_data').datagrid('getSelected');
        $('#cart').edatagrid({
        	// url:"<?php //echo base_url() ?>kasir/data",
            // url: 'get_users.php',
            // saveUrl: '<?php //echo base_url() ?>gl/schema_insert/'+row.id,
            // if (arr_cart) {
            // data: JSON.stringify(arr_cart),
            data: data_json,
            autoSave: true,
            // },
            // updateUrl: $('#cart').edatagrid('saveRow'),
            // destroyUrl: save('BRG48'),
            onRowContextMenu: function(e,index,row){
				$(this).datagrid('selectRow',index);
				e.preventDefault();
				$('#mm').menu('show', {
					left:e.pageX,
					top:e.pageY
				});
			},
			onSave: function(index,row){
				$('#cart').edatagrid('saveRow');
				count();
			},
			onDestroy: function(index,row){
				count();
			}
        });
        $('#input_barcode').textbox({
			inputEvents:$.extend({},$.fn.textbox.defaults.inputEvents,{
				keyup:function(e){
					// console.log(e)
					if (e.keyCode == 13) {
						save($('#input_barcode').val());
						$('#input_barcode').textbox('clear');
						// alert('yeay');
					}
				}
			})
		});
		$('#bayar_tunai').numberbox({
			inputEvents:$.extend({},$.fn.numberbox.defaults.inputEvents,{
				keyup:function(e){
					if (e.keyCode == 13) {
						var x = $('#bayar_tunai').val();
						var y = $('#total_bayar').val();
						var kembalian = x - y;
						$('#kembalian').textbox('reset');
						$('#kembalian').textbox('setValue', kembalian);
						// console.log(x);
						// alert(x);
					}
				}
			})
		});
		$('#sub-total').edatagrid({
        	data: data_total
        });
    });

   	
    function save(code){
    	// console.log(code);
        jQuery('#forminput').form('submit', {
		    url:'<?php echo base_url() ?>kasir/data/'+code,
		    onSubmit: function(){
		    	if (code) {
		    		return true;
		    	}
		    	else{
		    		return false;
		    	}
		    },
		    success:function(data){
		    	var str = eval(data);
		    	var obj_item = {};
		    	var flag = 0;
		    	// var mbuh = str.replace("[","");
		    	// arr_cart.forEach()
		    	// var data2 = {};
		    	// data2.barcode = str[0].kodeBarang;
		    	// data2.quantity = 1;
		    	//  var collection = {
		     //        'barcode' : str[0].kodeBarang,
		     //        'quantity' : 1
		     //    };
		        obj_item.barcode = str[0].kodeBarang;
				obj_item.name = str[0].nama_item;
				obj_item.price = str[0].item_harga;
				obj_item.quantity = 1;
				obj_item.total = obj_item.quantity*str[0].item_harga;
				flag = add(obj_item.barcode);
				if(flag==0){
					arr_cart.push(obj_item);
					// arr_carto.push(collection);
					count();
					// alert("yeay");
				}
				$('#item').combobox('clear');
				// alert(flag);

				// alert(arr_cart);
		    }
		});
    }

    function simpan(){
    	var photos = new Array();
    	// for(var i = 0; i < 3; i++)
	    // {
	    //      var collection = {
	    //         'no' : i ,
	    //         'data' : arr_cart
	    //     };
	    //     photos.push( collection );
	    // }
	    // photos.push(arr_cart);
	    var isValid = $('#formbayar').form('validate');
	    if (isValid) {
	    	$.ajax({ 
		        type: "POST",
		        url: "<?php echo base_url()?>kasir/add",
		        data: {
		        	collection : arr_cart , 
		        	total : arr_total[0]['value']},
		        cache: false,
		        success: function(response)
		        {
		            console.log(response);
		            alert('success');
		            jQuery('#FormBayar').dialog('close');
	                reset();
		            // window.location = '<?php echo base_url()?>create/submit';
		        }
		    });
	    }
	    // console.log(isValid);

     //    jQuery('#form').form('submit',{
     //        url: url,
     //        onSubmit: function(data){
     //        	data.cart = data_json;
     //        	data.total = data_total;
     //            return jQuery(this).form('validate');
     //        },
     //        success: function(result){
     //            var result = eval('('+result+')');
     //            if(result.success){
     //                jQuery('#FormBayar').dialog('close');
     //                arr_cart = [];
					// arr_total = [];
					// obj_total = {};
					// count();
     //                $.messager.alert({
     //                    title: 'Berhasil',
     //                    msg: 'Transaksi Berhasil!',
     //                    icon: 'info'
     //                });
     //            } else {
     //                $.messager.alert({
     //                    title: 'Error',
     //                    msg: result.msg,
     //                    icon: 'error'
     //                });
     //            }
     //        }
     //    });
    }

  </script>