  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Stock Item

          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Gudang</a></li>
            <li class="active">Stock Item</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-xs-12 col-lg-12">
              <div class="box">
                <div class="box-header">
                  <div class="box-tools">
                    <div class="input-group" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <?php if($this->session->flashdata('pesan')){
                    echo $this->session->flashdata('pesan');
                  } ?>
                  <table class="table table-hover">
                    <tr>
                      <th>Id Item</th>
                      <th>Nama Item</th>
                      <!-- <th>Petugas</th> -->
                      <!-- <th>Customer</th> -->
					   <!-- <th>Kurir</th> -->
                      <th>Stock</th>
                    </tr>

					<?php
							if(!empty($isi)){
							foreach($isi as $baris){ ?>
                    <tr>
                      <td><?php echo $baris->id_item?></td>
                      <td><?php echo $baris->nama_item?></td>
                      <td><?php echo $baris->stock?></td>
					  
					

                    </tr>
					<?php }}
								else{
									echo "Stock Kosong";
								}

							?>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			   <div class="row">
					<div class="col-md-12 text-center">
						<?php echo $paging; ?>
					</div>
				</div>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
