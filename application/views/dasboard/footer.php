<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Edited By <a href="http://ahapedia.com">ahapedia</a></b>
        </div>
        <strong>Copyright &copy; 2017 <a href="#">PT.Retail</a></strong>
      </footer>



    </div><!-- ./wrapper -->


<!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url() ?>assets/js/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?php echo base_url() ?>assets/js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery-jvectormap-world-mill-en.js"></script>

    <!-- daterangepicker -->
    <script src="<?php echo base_url() ?>assets/js/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap3-wysihtml5.all.min.js"></script>

    <!-- Slimscroll -->
    <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() ?>assets/js/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() ?>assets/js/app.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() ?>assets/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/datatables/dataTables.bootstrap.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <!--script src="<?php echo base_url() ?>assets/js/demo.js"></script>
      <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--script src="<?php echo base_url() ?>assets/js/dashboard.js"></script>

    <!-- Bootstrap-select -->
    <script src="<?php echo base_url() ?>assets/js/select2.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/easyui/jquery.easyui.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/easyui/src/jquery.edatagrid.js"></script>
    <script>
  /*$(function () {
    $("#dataTabel").DataTable();
    $('#dataTabel2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });*/
</script>
    <script type="text/javascript">
         //Date picker
        $('.datepicker').datepicker({
          autoclose: true

        });

        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
        $(document).ready(function() {
          $(".selecttree").select2();
        });
    </script>
    <script type="text/javascript">
    <?php if($this->session->userdata('game')) { ?>
    // Set the date we're counting down to
    var now2 = new Date("<?php echo $this->session->userdata('tanggal_sekarang'); ?>").getTime();
    // var now3 = new Date("03-01-2018 22:00:11").getTime();
    // alert(now2);
    var newDateObj = new Date(now2 + 5*60000).getTime();
    // alert(now3);
    // var newDateObj = new Date(now2 + 10000).getTime();

    // var distance = newDateObj - now2;
    // alert(distance);

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();
        
        // Find the distance between now an the count down date
        var distance = newDateObj - now;
        // Time calculations for days, hours, minutes and seconds
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        if(seconds < 10)
          seconds = "0"+seconds;
        
        // Output the result in an element with id="demo"
        document.getElementById("demo").innerHTML = "0" +minutes + ":" + seconds ;
        // document.getElementById("demo").innerHTML = seconds ;

        
        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            $.ajax({
                url:'<?php echo base_url() ?>game/gameStart',
                // url : url,
                type: 'GET',
                // data: {data: JSON.stringify(rows)},
                cache: false,
                success: function(result){
                        var result = eval('('+result+')');
                        if(result.success){
                            // location.reload();
                            // jQuery('#formPrivilege').dialog('close');
                            // $.messager.alert({
                            //     title: 'Berhasil',
                            //     msg: 'Berhasil memasukkan data!',
                            //     icon: 'info'
                            // });
                            window.location.replace('<?php echo base_url(); ?>gudang/listPengeluaran');
                        }
                    }
              });
             // location.reload();
        }
    }, 1000);
    <?php } ?>
</script>
  </body>
</html>
