  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Add Privilege
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Privilege</li>
          </ol>
        </section>
      
        <!-- Main content -->
        <section class="content">
         
          <div class="row">
			<div class="col-md-6">
				 <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Form Privilege</h3>
				  
				<?php if($this->session->flashdata('pesan')){
					  echo $this->session->flashdata('pesan');
				  } ?>
               
				
				<!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="<?php echo base_url() ?>Menu/addPrivilege_act" method="post" enctype="multipart/form-data">
                  <div class="box-body">
					           <div class="form-group">
                        <label>Roles</label>
                        <select name="roles" class="form-control select2" style="width: 100%;" required="">
                           <option value="">- Pilih Salah Satu -</option>
                            <?php foreach ($roles as $data) 
                            { ?>
                              <option value="<?php echo $data->id ?>"><?php echo $data->name ?></option>
                              
                            <?php  } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Menu :</label> <br>
                        <?php foreach ($menu as $menus) 
                        { ?>
                          <input type="checkbox" name="privilege_list[]" value="<?php echo $menus->id ?>"><label><?php echo $menus->name ?></label><br/>
                          
                        <?php } ?>
                    </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

			</div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     