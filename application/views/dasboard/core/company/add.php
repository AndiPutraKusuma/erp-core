  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Add Company
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Company</a></li>
            <li class="active">Add Company</li>
          </ol>
        </section>
      
        <!-- Main content -->
        <section class="content">
         
          <div class="row">
			<div class="col-md-6">
				 <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Form Add Company</h3>
				  
				<?php if($this->session->flashdata('pesan')){
					  echo $this->session->flashdata('pesan');
				  } ?>
               
				
				<!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="<?php echo base_url() ?>company/insert" method="post" enctype="multipart/form-data">
                  <div class="box-body">
                    <div class="form-group">
                      <label>Company Code</label>
                      <input name="company_code" type="text" class="form-control" id="company_code" placeholder="Company Code" required>
                    </div>
                    <div class="form-group">
                      <label>Company Name</label>
                      <input name="company_name" type="text" class="form-control" id="company_name" placeholder="Company Name" required>
                    </div>
                    <div class="form-group">
                      <label>Company Address</label>
                      <input name="company_address" type="text" class="form-control" id="company_address" placeholder="Company Address" required>
                    </div>
                    <div class="form-group" hidden>
                      <label>Created By</label>
                      <input name="created_by" class="form-control" id="created_by" value="<?php echo $id_petugas; ?>">
                    </div>
                    <div class="form-group" hidden>
                      <label>Parent Id</label>
                      <input name="parent_id" class="form-control" id="parent_id" value="<?php echo $company_id; ?>">
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

			</div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     