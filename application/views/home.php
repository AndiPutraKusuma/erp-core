    <div class="main">
        <div class="main-inner">
            <div class="content">
                <div class="mt-150">
    <div class="hero-image">
    <div class="hero-image-inner" style="background-image: url('<?php echo base_url() ?>assets/images/tmp/slider-large-3.jpg');">
        <div class="hero-image-content">
            <div class="container">
                <h1>Retail</h1>

                <p>Memuat semua data keluar masuk barang</p>

				?>


            </div><!-- /.container -->
        </div><!-- /.hero-image-content -->

        <div class="hero-image-form-wrapper">
            <div class="container">
                <div class="row">

                    <div class="col-sm-4 col-sm-offset-8 col-lg-4 col-lg-offset-7">
                      <?php
              					if($this->session->flashdata("pesan")){
              						echo $this->session->flashdata("pesan");
              					}

              				?>
                        <form method="post" action="<?php echo base_url() ?>admin/login_act" style="margin-top:-400px;">
                            <h2>Login</h2>

                            <div class="hero-image-keyword form-group">
                                <input name="email" type="email" class="form-control" placeholder="Email">
                            </div><!-- /.form-group -->
							<div class="hero-image-category form-group">
                                <input name="passwd" type="password" class="form-control" placeholder="Password">
                            </div><!-- /.form-group -->

                            <button type="submit" class="btn btn-primary btn-block">Login</button>
                        </form>
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.hero-image-form-wrapper -->
    </div><!-- /.hero-image-inner -->
</div><!-- /.hero-image -->

</div>
</div><!-- /.content -->
</div><!-- /.main-inner -->
</div><!-- /.main -->
